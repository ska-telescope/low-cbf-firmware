----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au) & Norbert Abel
-- 
-- Create Date: 13.03.2020 09:47:34
-- Module Name: CT_VFC_Single - Behavioral  (single station corner turn)
-- Description: 
--  Corner turn for the Perentie VFC (very fine channeliser) 
--  The corner turn takes data for all channels for one station, buffers, and outputs 
--  data in bursts for each channel. Burst length is programmable via MACE.
-- 
-- INPUT DATA [x1]:
-- for time = ... (forever)
--    for coarse_group = 1:384/8-1  (order of the coarse groups may vary)
--       for coarse = 1:8
--          for time = 1:2:2048
--             [[ts0, pol0], [ts0, pol1], [ts1, pol0], [ts1, pol1]]
--
-- OUTPUT DATA:
-- Output <BURST LENGTH> and <PRELOAD LENGTH> is configurable via MACE.
-- for coarse = <order defined by a table, programmed via MACE>
--    for time = 1:(<BURST LENGTH> + <PRELOAD LENGTH>)x4096
--       if station_group == 1: [station0, pol0], [station0, pol1]
--
---------------------------------------------------------------------------
-- Structure
-- ---------
-- The basic structure of the Coarse Corner Turner is as follows:
-- 
--           ----------->-------HBM-------->---------
--           |                                      |
-- => INPUT_BUFFER <----> (ct_vfc_malloc) <----> OUTPUT_BUFFER => to Correlator filterbank
--                                               OUTPUT_BUFFER => to PSS/PST filterbank
--             
--
-- The input buffer implements a helper FIFO that allows for longer bursts on the HBM.
-- ct_vfc_malloc keeps track of the memory allocated in the HBM
-- The output buffer sorts the data into the required output ports. 
--
-- Memory Management
-- -----------------
-- The corner turn takes a software-like approach to managing the memory.
-- Data is tracked using pointers to memory buffers, with all the pointers stored 
-- in block RAM.
-- The basic unit of memory that is allocated is 64Kbytes.
-- Input data is in packets with 8192 bytes, so 8 different times are needed to fill a 64K block.
-- Correlator output data is in units of 16384 (4096 samples * 2 pol * 2 (complex)), so 4 different times fill a 64K block.
-- Since 1 Gbyte = 65536 * 16384,
--  16384 pointers are needed for a full 1 Gbyte of memory.
--  * To address 1 Gbyte, 14 bit pointers are needed.
--  * To address 2 Gbyte, 15 bit pointers are needed. 
-- 
-- The amount of memory needed for the corner turn depends on the length of the corner turn, 
-- and the safety factor - the maximum time allowed for late arriving packets.
--
-- The use of 1 pointer for 4 output times also creates some overhead, since in the worst case
-- memory is allocated but not filled for 384 stations and 3 times.
-- This is a fixed minimum overhead of 384 * 3 * (16384 bytes) = 18 MBytes
-- 
-- Some indicative cases for the correlator :
--
--  Corner turn length  |   Single buffer size  |  Minimum buffer size (+18 overhead, +12 times for preload) 
--     0.9 s                   1224 Mbytes         1314 MBytes
--     0.3 s                   408 Mbytes           498 MBytes   (i.e. could fit in 512 but very little is left over for incoming packet jitter)
--     0.15 s                  204 Mbytes           294 MBytes
--
-- Pointer Management (in ct_vfc_malloc.vhd)
-- ------------------
-- There are two lists, the used list and the free list.
-- There are 16384 pointers per Gbyte of memory, and each pointer is 2 bytes, so the
-- free list needs 32Kbytes of storage = 1 URAM per GByte.
--
-- There is a separate used list for each channel. The used list for each channel needs 
-- space for enough pointers to have at least 2 full frames allocated. For 0.3 s frames,
-- this is 2*((68+12)/4) = 40 pointers.
-- There are 8 LFAA input packets per "used" pointer, so each "used" pointer also 
-- needs to indicate which of the 8 input packets have been written. 
-- So each pointer needs 3 bytes.
-- (384 channels) * (40 pointers) * (3 bytes/pointer) = 46080 bytes
-- URAMs are 72 bits wide, so they fit 3 pointers per address. 
-- Use 2 URAMs for the used pointer list. Allow up to 63 pointers per channel.
-- Then each channel has 21 URAM addresses each, and 21*384 = 8064 of the 8192 addresses are used.
-- (note : 8192 addresses since each URAM is (4096 deep) x (64 bits wide), so 2 URAMs => 8192 addresses)
--
-- Channel Readout Order
-- ---------------------
-- MACE programmable table (Numbers 0 to 383 written to a table 384 deep),
-- defines the order in which coarse channels are read out.
--
-- Data Protection
-- ---------------
-- Reads & writes to the HBM are bursts of 8 transactions.
-- The interface is 256+32 bits wide, i.e. 32+4 bytes. 32 bytes are used for the data,
-- while the remaining 4 bytes are available for other uses.
-- In each burst of writes, we include the packet timestamp in the first word (4 bytes)
-- and a burst checksum in the last word.
----------------------------------------------------------------------------------

library IEEE, ct_vfc_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;
USE ct_vfc_lib.ct_vfc_config_reg_pkg.ALL;
--use work.ct_vfc_pkg.all;

Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;

--library UNISIM;
--use UNISIM.VComponents.all;

entity CT_VFC_Single is
    generic(
        -- How much memory to allow for.
        -- True : 2 Gbytes have been allocated to the corner turn.
        -- False : 1 Gbyte is allocated to the corner turn.
        -- This defines the amount of space allocated for pointers.
        -- The actual amount of memory used and the base address of the memory is defined via MACE. 
        g_USE_2GBYTE : boolean := false
    );
    Port(
        -- Processing & HBM clock
        i_hbm_clk         : in  std_logic;  -- AXI clock: for ES and -1 devices: <=400MHz, for PS of -2 and higher: <=450MHz (HBM core and most of the CTC run in this clock domain)  
        i_hbm_clk_rst     : in  std_logic;        
        --ingress (in input_clk):
        i_input_clk     : in std_logic;                      -- Clock domain for the data input
        i_input_clk_rst : in std_logic;
        i_data          : in std_logic_vector(255 downto 0); -- Incoming data stream, 256 bits wide. 
        i_checksum      : in std_logic_vector(31 downto 0);  -- Checksum every 4 clocks for the packet; used to check for HBM errors.
        i_pktValid      : in std_logic;                      -- pktValid goes high for the duration of the input packet frame, which should be 2048 time samples.
        i_dataValid     : in std_logic;                      -- Current data cycle valid; will go high every second clock when ct_vfc_top has converted from a 128 bit bus to a 256 bit bus.
        -- Readout for the correlator filterbank
        i_cor_clk          : in  std_logic;            -- clock domain for the egress
        o_cor_sof          : out std_logic;            -- single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
        o_cor_header       : out t_ctc_output_header;  -- meta data belonging to the data coming out
        o_cor_header_valid : out std_logic;            -- new meta data (every output packet, aka 4096 cycles) 
        o_cor_data         : out t_ctc_output_payload; -- The actual output data, complex data for 2 polarisations.
        o_cor_data_valid   : out std_logic;            -- High for the duration of the packet (4096 clocks for correlator data)
        o_cor_flagged      : out std_logic;            -- is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
        -- Readout for PSS & PST filterbanks
        i_PSSPST_clk          : in  std_logic;            -- clock domain for the egress
        o_PSSPST_sof          : out std_logic;            -- single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
        o_PSSPST_header       : out t_ctc_output_header;  -- meta data belonging to the data coming out
        o_PSSPST_header_valid : out std_logic;            -- new meta data (every output packet, aka 4096 cycles) 
        o_PSSPST_data         : out t_ctc_output_payload; -- The actual output data, complex data for 2 polarisations. (fields are .hpol.re, .hpol.im, .vpol.re, .vpol.im)
        o_PSSPST_data_valid   : out std_logic;            -- High for the duration of the packet (4096 clocks for correlator data)
        o_PSSPST_flagged      : out std_logic;            -- is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
        --HBM INTERFACE
        o_hbm_mosi    : out t_axi4_full_mosi;              -- Data going to the HBM core
        o_wdata_extra : out std_logic_vector(31 downto 0); -- Extra data to the HBM (ECC bits)
        i_hbm_miso    : in  t_axi4_full_miso;              -- Data coming from the HBM core
        i_rdata_extra : in  std_logic_vector(31 downto 0); -- Extra data from the HBM (ECC bits)
        i_hbm_ready   : in  std_logic;                     -- HBM reset finished? (=apb_complete)
        -------------------------------------------
        -- registers
        --  General config : ??
        i_rst_status : in std_logic;
        i_switch_packet_count : in std_logic_vector(31 downto 0); -- Which packet count to switch delay tables at.
        i_table_select : in std_logic; -- table to use after i_switch_packet_count table.
        i_correlator_totalChannels : in std_logic_vector(8 downto 0); -- Total number of correlator channels to output.
        i_PSSPST_totalChannels     : in std_logic_vector(8 downto 0); -- Total number of PSS/PST channels to output.
        o_table_used :  out std_logic; -- Current delay table in use.
        -- General configuration from (or derived from) global config registers
        i_freeMax              : in std_logic_vector(15 downto 0); -- maximum number of 64K blocks in the memory pool.
        i_baseAddress          : in std_logic_vector(23 downto 0); -- Base address in memory of the memory pool.
        i_maxFutureCount       : in std_logic_vector(15 downto 0); -- Maximum time in the future the packet count can be without dropping the packet.
        i_scanStartPacketCount : in std_logic_vector(31 downto 0); -- first packet count in the scan
        i_blocksPerFrame0      : in std_logic_vector(10 downto 0); -- Number of LFAA blocks (block = 2048 samples) per frame for the correlator output.
        i_preload0             : in std_logic_vector(15 downto 0); -- Number samples for the preload for the correlator.
        i_blocksPerFrame1      : in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame for the PSS/PST output.
        i_preload1             : in std_logic_vector(15 downto 0); -- Number samples for the preload for the PSS/PST output.
        i_CorOutputCycles      : in std_logic_vector(15 downto 0); -- Number of clock cycles per packet output for the correlator output.
        i_PSSPSTOutputCycles   : in std_logic_vector(15 downto 0); -- Number of clock cycles per packet output for the PSS/PST output.
        -- delay table
        -- Addressed by the virtual channel, with two words per virtual channel.
        --  First word: bits(15:0) = Coarse delay in samples, bits(31:16) = DeltaPhase (Hpol)
        --  Second word : bits(15:0) = DeltaPhase (Vpol), bits(31:16) = DeltaDeltaPhase.
        o_delay_table_0_addr : out std_logic_vector(9 downto 0);
        i_delay_table_0_data : in std_logic_vector(31 downto 0);
        o_delay_table_1_addr : out std_logic_vector(9 downto 0);
        i_delay_table_1_data : in std_logic_vector(31 downto 0);
        -- correlator virtual channel output order (512x16 bit memory)
        o_correlator_order_addr : out std_logic_vector(8 downto 0);
        i_correlator_order_data : in  std_logic_vector(15 downto 0);
        -- PSS and PST virtual channel output order (512x16 bit memory
        o_PSSPST_order_addr : out std_logic_Vector(8 downto 0);
        i_PSSPST_order_data : in std_logic_vector(15 downto 0);
        -- memory to record counts of valid output packets for the correlator
        o_correlator_count_addr : out std_logic_vector(8 downto 0);
        o_correlator_count_wrEn : out std_logic;
        o_correlator_count_WrData : out std_logic_vector(31 downto 0);
        i_correlator_count_RdData : in std_logic_vector(31 downto 0);
        -- memory to record counts of valid output packets for PSS/PST
        o_PSSPST_count_addr : out std_logic_vector(8 downto 0);
        o_PSSPST_count_WrEn : out std_logic;
        o_PSSPST_count_WrData : out std_logic_vector(31 downto 0);
        i_PSSPST_count_RdData : in std_logic_vector(31 downto 0);
        -- status registers
        o_free_size                 : out std_logic_Vector(15 downto 0);
        o_free_min                  : out std_logic_vector(15 downto 0);
        o_input_late_count          : out std_logic_vector(15 downto 0);
        o_input_too_soon_count      : out std_logic_vector(15 downto 0);
        o_duplicates                : out std_logic_vector(7 downto 0);
        o_correlator_missing_blocks	: out std_logic_vector(15 downto 0);
        o_psspst_missing_blocks     : out std_logic_vector(15 downto 0);
        o_error_preload_config      : out std_logic;
        o_error_free_bad            : out std_logic;
        o_error_input_overflow      : out std_logic;
        o_error_ctc_underflow       : out std_logic
    );
end CT_VFC_single;

architecture Behavioral of CT_VFC_single is
    
    -- Bus to communicate HBM addresses to the input buffer (ct_vfc_input_buffer) from the memory allocation module (ct_vfc_malloc)
    signal writePacketCount : std_logic_vector(31 downto 0);  -- Packet count from the packet header
    signal writeChannel : std_logic_vector(15 downto 0);  -- virtual channel from the packet header
    signal writePacketCountValid : std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
    signal writeAddress : std_logic_vector(23 downto 0); -- Address to write this packet to, in units of 8192 bytes.
    signal writeOK : std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
    signal writeAddressValid : std_logic;  
    
    -- Bus to communicate the HBM addresses to the output buffer (ct_vfc_output_buffer) from the memory allocation module (ct_vfc_malloc) 
    signal readAddress     : std_logic_vector(23 downto 0);   -- HBM address to read from, in units of 8192 bytes. Points to the start of a 2048 sample packet.
    signal readPacketCount : std_logic_vector(31 downto 0);   -- packet count for this read address
    signal readChannel     : std_logic_vector(15 downto 0);   -- Virtual channel at this address.
    signal readStart, readOK, readFree, readValid0, readReady0, readValid1, readReady1, free0, free1 : std_logic;
    
    signal mallocBusy : std_logic;

    -- configuration from registers
    signal freeMax : std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
    signal baseAddr : std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
    signal maxFutureCount : std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
    signal scanStartPacketCount : std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
    signal blocksPerFrame0 : std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
    signal preload0 : std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
    signal totalChannels0 : std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
    signal blocksPerFrame1 : std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
    signal preload1 : std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
    signal totalChannels1 : std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
    signal readChannelOrderAddress0 : std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 0
    signal readChannelOrderData0 : std_logic_vector(8 downto 0); -- virtual channel to read; assumes 3 clock latency from o_readChannelOrderAddress
    signal readChannelOrderAddress1 : std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 1
    signal readChannelOrderData1 : std_logic_vector(8 downto 0); -- virtual channel to read, assumes 3 clock latency for o_readChannelOrderAddress1

    signal hbm_mosi_aw_w : t_axi4_full_mosi; -- The aw and w buses for the axi full HBM interface come from the input buffer.
    
begin

    
    ------------------------------------------------------------------------------------
    -- INPUT BUFFER
    ------------------------------------------------------------------------------------
    -- + extract header from payload
    -- + CDC to hbm_clk
    ------------------------------------------------------------------------------------
    E_INPUT_BUFFER: entity ct_vfc_lib.ct_vfc_input_buffer
    port map (
        i_hbm_clk       => i_hbm_clk,
        i_hbm_clk_rst   => i_hbm_clk_rst,
        i_input_clk     => i_input_clk,
        i_input_clk_rst => i_input_clk_rst,
        -- data in from the LFAA ingest
        i_data          => i_data,      -- in(255:0);  -- incoming data stream (header, data)
        i_dataChecksum  => i_checkSum,  -- in(31:0);   -- Checksum is the sum of all the 32-bit words in every 8 clocks (for 128 bit interface) or every 4 clocks (for 256 bit interface); 
        i_pktValid      => i_pktValid,  -- in std_logic; -- High for the duration of the packet.
        i_dataValid     => i_dataValid, -- in std_logic; -- Each packet should have 257 valid cycles. (This will go high every second clock if the top level input was only 128 bits wide)
        -- Talk to the pointer management unit, to get the HBM address to write each block to.
        o_packetCount      => writePacketCount,      -- out std_logic_vector(31 downto 0); -- Packet count from the packet header
        o_channel          => writeChannel,          -- out std_logic_vector(15 downto 0); -- virtual channel from the packet header
        o_packetCountValid => writePacketCountValid, -- out std_logic;                     -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
        i_packetAddress    => writeAddress,          -- in std_logic_vector(31 downto 0);  -- Address to write this packet to.
        i_packetDrop       => writeOK,               -- in std_logic;                      -- Packet should be dropped as it is either too early or too late.
        i_addrValid        => writeAddressValid,     -- in std_logic;                      -- i_packet_addr, i_packet_drop are valid. This concludes the transaction with the pointer management module.
        -- Writes to the HBM :
        --  AXI signals to the HBM
        -- This module only uses the "AW" (write address) bus and the "W" (write data) bus. 
        -- Combine at this level with the read buses to send to the HBM.
        o_hbm_mosi      => hbm_mosi_aw_w, -- out t_axi4_full_mosi;
        o_wdata_extra   => o_wdata_extra, -- out(31:0)
        i_hbm_miso      => i_hbm_miso,    -- in  t_axi4_full_miso;
        i_hbm_ready     => i_hbm_ready,   -- in  std_logic
        -- error out (in input_clk):
        o_error_input_buffer_overflow => inputBufferOverflow -- out std_logic
    );
    
    o_error_input_overflow <= inputBufferOverflow ?? clock domain? hold ?
    
    ------------------------------------------------------------------------------------
    -- Address management - Allocate and free addresses in the HBM
    ------------------------------------------------------------------------------------
    -- + Generates write addresses for data coming in
    -- + Generates read addresses for data to be read out of the HBM
    ------------------------------------------------------------------------------------
    E_MALLOC :  entity ct_vfc_lib.ct_vfc_malloc
    generic map (
        -- How much memory to allow for.
        -- True : 2 Gbytes have been allocated to the corner turn.
        -- False : 1 Gbyte is allocated to the corner turn.
        -- This defines the amount of space allocated for pointers.
        -- The actual amount of memory used and the base address of the memory is defined via MACE. 
        g_USE_2GBYTE => g_USE_2GBYTE --: boolean := false
    ) port map (
        -- This module only uses the HBM clock
        i_clk => i_hbm_clk,
        -- Interface to the data input side - writes to the HBM
        i_writePacketCount => writePacketCount,      -- in(31:0);  -- Packet count from the packet header
        i_writeChannel     => writeChannel,          -- in(15:0);  -- virtual channel from the packet header
        i_writeValid       => writePacketCountValid, -- in std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
        o_writeAddress     => writeAddress,          -- out(23:0); -- Address to write this packet to, in units of 8192 bytes.
        o_writeOK          => writeOK,               -- out std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
        o_writeValid       => writeAddressValid,     -- out std_logic;                     -- i_packet_addr, i_packet_drop are valid. This concludes the transaction with the pointer management module.
        -- Interface to the data output side - Reads from the HBM
        -- If it is the last read of an address, then the address of block to be freed is put into a FIFO in this module, and read from the FIFO on i_free0/1
        -- This means no more than 3 of these addresses should be buffered by the module which reads the HBM.
        o_readAddress     => readAddress, --  out std_logic_vector(23 downto 0);   -- HBM address to read from, in units of 8192 bytes. Points to the start of a 2048 sample packet.
        o_readPacketCount => readPacketCount, --out std_logic_vector(31 downto 0);   -- packet count for this read address
        o_readChannel     => readChannel,     --out std_logic_vector(15 downto 0);   -- Virtual channel at this address.
        o_readStart       => readStart,       --out std_logic;                       -- Indicates this is the first read for this channel
        o_readOK          => readOK,          --out std_logic;                       -- o_readAddress points to valid data
        o_readFree        => readFree,        --out std_logic;                       -- Indicates readAddress can be freed once it has been read
        o_readValid0      => readValid0,      --out std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady0      => readReady0,      -- in std_logic;                        -- port 0 read side can accept a new address to read from. Data is transfered when valid and ready are high.
        o_readValid1      => readValid1,      -- out std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady1      => readReady1,      -- in std_logic;                        -- port 1 read side can accept a new address to read from. Data is transfered when valid and ready are high.        
        i_free0           => free0,           -- in std_logic;                        -- pulse to indicate that the free operation can go ahead.
        i_free1           => free1,           -- in std_logic;
        -- Configuration from MACE
        -- Configuration parameters must be set prior to i_rst.
        -- Typical values for 0.3 second correlator corner turn :
        --  i_freeMax = 16384  = 1 Gbyte of memory
        --  i_baseAddr = 0x0   = Start at the start of the memory block.
        --  i_maxFutureCount = 400 (Units of LFAA blocks of 2048 samples = 2.2ms) (Minimum is 2 frames worth + preload, maximum is 63(pointers)*8(LFAA blocks/pointer) = 504, since that is the maximum length of the used list for a single channel (for the 1Gbyte case))
        --  i_scanStartPacketCount = 0 (Just depends on the input data)
        --  i_blocksPerFrame0 = 136 (=8x17, note 136*2.2ms = 300 ms)
        --  i_preload0 = 22 * 2048 = 45056; 
        --  i_totalChannels0 = 384
        --  i_blocksPerFrame1 = 16  (16*2.2ms = 35.2 ms)
        --  i_preload1 = 11*256 = 2816  (PST preload is 11*256 samples = 2816 samples)
        --  i_totalChannels1 = 384
        i_rst         => i_hbm_clk_rst, -- in std_logic;                         -- Reset, frees all memory. Should be common to all instances of the corner turn. 
        o_busy        => mallocBusy,    -- out std_logic;                        -- Indicates the fsm is busy, e.g. writing default data to the free list after a reset.
        i_freeMax     => freeMax,       -- in std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
        i_baseAddr    => baseAddr,      -- in std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
        i_maxFutureCount => maxFutureCount, -- : in std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
        i_scanStartPacketCount => scanStartPacketCount, -- : in std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
        i_blocksPerFrame0 => blocksPerFrame0, -- : in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
        i_preload0        => preload0, -- : in std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
        i_totalChannels0  => totalChannels0, -- : in std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
        i_blocksPerFrame1 => blocksPerFrame1, -- : in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
        i_preload1        => preload1, -- : in std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
        i_totalChannels1  => totalChannels1, -- : in std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
        o_readChannelOrderAddress0 => readChannelOrderAddress0, -- : out std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 0
        i_readChannelOrderData0    => readChannelOrderData0, -- : in  std_logic_vector(8 downto 0); -- virtual channel to read; assumes 3 clock latency from o_readChannelOrderAddress
        o_readChannelOrderAddress1 => readChannelOrderAddress1, -- : out std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 1
        i_readChannelOrderData1    => readChannelOrderData1, -- : in  std_logic_vector(8 downto 0); -- virtual channel to read, assumes 3 clock latency for o_readChannelOrderAddress1
        -- Timing information 
        i_packetCountNotify => i_packetCountNotify, -- : in std_logic_vector(31 downto 0); -- Set o_packetCountSeen when an incoming packet count (i_packetCountWrite) is this value or higher.
        o_packetCountSeen   => o_packetCountSeen,   -- : out std_logic;                    -- Pulse to indicate reception of a packet with packet count >= i_packetCountNotify
        -- Monitoring
        o_freeSize    => o_free_size, -- : out std_logic_vector(15 downto 0);      -- Number of entries in the free list;
        o_freeMin     => o_free_min,  -- : out std_logic_vector(15 downto 0);      -- low watermark for o_freeSize
        o_inputLateCount => o_input_late_count, -- : out std_logic_vector(15 downto 0);   -- Count of input LFAA blocks which have been dropped as they are too late. Top bit is sticky.
        o_inputTooSoonCount => o_input_Too_Soon_Count, -- : out std_logic_vector(15 downto 0); -- Count of input LFAA blocks which have been dropped as they are too far into the future. Top bit is sticky.
        o_duplicates     => o_duplicates, -- : out std_logic_vector(7 downto 0);    -- count of duplicate input packets (i.e. input packets with the same timestamp and channel as a previous packet). Top bit is sticky.
        o_missingBlocks0 => o_correlator_missing_blocks, -- : out std_logic_vector(15 downto 0);   -- Count of blocks (1 block = 2048 time samples) on output 0 read with no data; top bit is sticky; low 15 bits wrap.
        o_missingBlocks1 => o_PSSPST_missing_blocks, -- : out std_logic_vector(15 downto 0);   -- Count of blocks on output 1 read with no data; top bit is sticky; low 15 bits wrap.
        o_preload1_gt_preload0 => o_error_preload_config, -- : out std_logic;                 -- Error flag; i_preload0 must be greater than or equal to i_preload1. Detected when new values are loaded after a module reset (via i_rst).
        o_badFreeFIFOState  => o_error_free_bad -- : out std_logic                     -- Error flag; Tried to read the toBeFree fifo, but it was empty; should never happen.
    );
    
    
    -- still unassigned :
              : out std_logic;
        o_error_ctc_underflow       : out std_logic
    
--    o_debug_hbm_out       <= hbm_data_out;
--    o_debug_hbm_out_vld   <= hbm_data_out_vld;
--    o_debug_hbm_block_vld <= hbm_block_vld;

    ------------------------------------------------------------------------------------
    -- OUTPUT BUFFER
    ------------------------------------------------------------------------------------
    -- + Timed Output
    -- + Coarse Delay
    -- + Synchronous output of whole uninterrupted packets
    ------------------------------------------------------------------------------------
    E_OUTPUT_BUFFER: entity ct_vfc_lib.ct_vfc_output_buffer
    generic map (
               
    ) port map(
        i_hbm_clk        => i_hbm_clk,
        i_hbm_clk_rst    => hbm_clk_rst,
        --config:
        -- to be updated....
        i_hbm_config_start_ts       => hbm_config_start_ts,
        i_out_config_start_ts       => output_config_start_ts,
        o_coarse_delay_addr         => coarse_delay_addr,
        o_coarse_delay_addr_vld     => coarse_delay_addr_vld,
        i_coarse_delay_packet_count => coarse_delay_packet_count,
        i_coarse_delay_value        => coarse_delay_value,
        i_coarse_delay_delta_hpol   => coarse_delay_delta_hpol,   
        i_coarse_delay_delta_vpol   => coarse_delay_delta_vpol,   
        i_coarse_delay_delta_delta  => coarse_delay_delta_delta,  
        o_end_of_integration_period => end_of_integration_period,
        o_current_packet_count      => current_packet_count,
        i_hbm_config_update         => hbm_config_update,
        i_out_config_update         => output_config_update,
        i_output_clk_wall_time      => i_output_clk_wall_time,
        i_config_start_wt           => output_config_start_wt,
        i_config_output_cycles      => output_config_output_cycles,
        i_enable_timed_output       => enable_timed_output, 
        
        i_correlator_totalChannels => i_correlator_totalChannels, -- in std_logic_vector(8 downto 0); -- Total number of correlator channels to output.
        i_PSSPST_totalChannels     => i_PSSPST_totalChannels,     -- in std_logic_vector(8 downto 0); -- Total number of PSS/PST channels to output.        
        
        -- Talk to the ct_vfc_malloc module to get the address to read from in the HBM
        o_readAddress     => readAddress, -- : in std_logic_vector(23 downto 0);   -- HBM address to read from, in units of 8192 bytes. Points to the start of a 2048 sample packet.
        o_readPacketCount => readPacketCount, -- in std_logic_vector(31 downto 0);   -- packet count for this read address
        o_readChannel     => readChannel,     -- in std_logic_vector(15 downto 0);   -- Virtual channel at this address.
        o_readStart       => readStart,       -- in std_logic;                       -- Indicates this is the first read for this channel
        o_readOK          => readOK,          -- in std_logic;                       -- o_readAddress points to valid data
        o_readFree        => readFree,        -- in std_logic;                       -- Indicates readAddress can be freed once it has been read
        o_readValid0      => readValid0,      -- in std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady0      => readReady0,      -- out std_logic;                        -- port 0 read side can accept a new address to read from. Data is transfered when valid and ready are high.
        o_readValid1      => readvalid1,      -- in std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady1      => readReady1,      -- out std_logic;                        -- port 1 read side can accept a new address to read from. Data is transfered when valid and ready are high.        
        i_free0           => free0,           -- out std_logic;                        -- pulse to indicate that the free operation can go ahead.
        i_free1           => free1,           -- out std_logic;
        
        -- Read address to the HBM and read data from the HBM
        o_hbm_mosi        => hbm_mosi_ar_r,
        i_hbm_miso        => i_hbm_miso,
        -- Readout for the correlator filterbank
        i_cor_clk          => i_cor_clk, -- : in  std_logic;            -- clock domain for the egress
        o_cor_sof          => o_cor_sof, -- : out std_logic;            -- single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
        o_cor_header       => o_cor_header,       -- : out t_ctc_output_header;  -- meta data belonging to the data coming out
        o_cor_header_valid => o_cor_header_valid, -- : out std_logic;            -- new meta data (every output packet, aka 4096 cycles) 
        o_cor_data         => o_cor_data,         -- : out t_ctc_output_payload; -- The actual output data, complex data for 2 polarisations.
        o_cor_data_valid   => o_cor_data_valid,   -- : out std_logic;            -- High for the duration of the packet (4096 clocks for correlator data)
        o_cor_flagged      => o_cor_flagged,      -- : out std_logic;            -- is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
        -- Readout for PSS & PST filterbanks
        i_PSSPST_clk          => i_PSSPST_clk,          -- in  std_logic;            -- clock domain for the egress
        o_PSSPST_sof          => o_PSSPST_sof,          -- out std_logic;            -- single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
        o_PSSPST_header       => o_PSSPST_header,       -- out t_ctc_output_header;  -- meta data belonging to the data coming out
        o_PSSPST_header_valid => o_PSSPST_header_valid, -- out std_logic;            -- new meta data (every output packet, aka 4096 cycles) 
        o_PSSPST_data         => o_PSSPST_data,         -- out t_ctc_output_payload; -- The actual output data, complex data for 2 polarisations. (fields are .hpol.re, .hpol.im, .vpol.re, .vpol.im)
        o_PSSPST_data_valid   => o_PSSPST_data_valid,   -- out std_logic;            -- High for the duration of the packet (4096 clocks for correlator data)
        o_PSSPST_flagged      => o_PSSPST_flagged       -- out std_logic;            -- is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
        
        --error:
        o_error_dsp_overflow               => error_dsp_overflow, 
        o_error_ctc_underflow              => error_ctc_underflow,
        o_error_output_aligment_loss       => error_output_aligment_loss,
        o_error_coarse_delay_fifo_overflow => error_coarse_delay_fifo_overflow
    );


    -- assign the AXI bus to the HBM
    -- The write side (AW and W busses) comes from the input buffer, the read side (AR, R busses) come from the output buffer
    -- AW : write address
    -- Notes regarding the addresses 
    --  - Address is a byte address into the HBM
    --  - bits (4:0) are unused, and should be 0, since transactions are always 32 byte aligned.
    --  - Address bus is 33 bits wide (for 8Gbytes of HBM) or 34 bits wide (for 1 GBytes of HBM)
    --  - There are 32 AXI channels into the HBM;
    --     + For 8Gbyte parts, each channel has native access to 256Mbytes. The 256 MByte piece is selected by addr(32:28)
    --     + For 16Gbyte parts, each channel has native access to 512 MBytes. The 512 Mbyte piece is selected by addr(33:29)
    --     + With the global crossbar turned on in the HBM, each AXI port can access the entire memory. But for efficient
    --       use of the HBM, the AXI port should use its native memory as much as possible.
    o_hbm_mosi.awaddr  <= hbm_mosi_aw_w.awaddr;
    o_hbm_mosi.awid    <= hbm_mosi_aw_w.awid;
    o_hbm_mosi.awlen   <= hbm_mosi_aw_w.awlen;
    o_hbm_mosi.awvalid <= hbm_mosi_aw_w.awvalid;
    -- Default values for the rest of the AW signals.
    o_hbm_mosi.awburst <= (0=>'1', others => '0');
    o_hbm_mosi.awsize  <= "101"; -- 256 bit wide bus;
    o_hbm_mosi.awregion <= (others => '0');
    o_hbm_mosi.awlock <= (others => '0');
    o_hbm_mosi.awcache <= (others => '0');
    o_hbm_mosi.prot <= (others => '0');  -- "000" indicates unprivileged access, secure access, data. Probably just ignored by the HBM.
    o_hbm_mosi.qos <= (others => '0'); -- "0000" indicates no particular quality of service scheme is in use. QOS is not supported by the HBM
    
    -- write data
    o_hbm_mosi.wdata  <= o_hbm_mosi_aw_w.wdata;
    o_hbm_mosi.wlast  <= o_hbm_mosi_aw_w.wlast;
    o_hbm_mosi.wstrb  <= o_hbm_mosi_aw_w.wstrb;
    o_hbm_mosi.wvalid <= o_hbm_mosi_aw_w.wvalid;
    
    -- write response
    -- (signals from the HBM are i_hbm_miso.bid, .bresp, .buser, and .bvalid)
    o_hbm_mosi.bready <= '1';

    -- Read side, from the output buffer
    -- AR : Read address
    o_hbm_mosi.arid   <= hbm_mosi_ar_r.arid;
    o_hbm_mosi.araddr <= hbm_mosi_ar_r.araddr;
    o_hbm_mosi.arlen  <= hbm_mosi_ar_r.arlen;
    o_hbm_mosi.arsize <= "101"; -- 256 bit wide data bus. 
    o_hbm_mosi.arburst <= hbm_mosi_ar_r.arburst;
    o_hbm_mosi.arvalid <= hbm_mosi_ar_r.arvalid;
    o_hbm_mosi.arlock <= (others => '0');
    o_hbm_mosi.arcache <= (others => '0');
    o_hbm_mosi.arprot <= (others => '0');
    o_hbm_mosi.arqos <= (others => '0');
    o_hbm_mosi.arregion <= (others => '0');
    o_hbm_mosi.aruser <= (others => '0');
    
    o_hbm_mosi.rready <= hbm_mosi_ar_r.rready;
    
    
end Behavioral;
