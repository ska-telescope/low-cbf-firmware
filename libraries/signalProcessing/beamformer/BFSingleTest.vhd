----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.05.2020 19:35:04
-- Design Name: 
-- Module Name: BFSingleTest - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BFSingleTest is
    Port (
        clkIn : in std_logic;
        din : in std_logic_vector(1 downto 0);
        dout : out std_logic_vector(5 downto 0)
    );
end BFSingleTest;

architecture Behavioral of BFSingleTest is

    component clk_wiz_0
    port(-- Clock in ports
        -- Clock out ports
        clk_out1          : out    std_logic;
        clk_in1           : in     std_logic);
    end component;

    signal clk : std_logic;
    signal v1 : std_logic_vector(15 downto 0);
    signal v2 : std_logic_vector(15 downto 0);
    signal v3 : std_logic_vector(15 downto 0);

    signal dinDel1 : std_logic_vector(1 downto 0);
    signal dinDel2 : std_logic_vector(1 downto 0);
    signal doutadv1 : std_logic_vector(15 downto 0);
    
    signal portData       : std_logic_vector(95 downto 0);
    signal portDataValid  : std_logic;
    signal portStart      : std_logic; -- start of a new frame; This signals both that new data is arriving for this frame and that the beams computed for the previous frame should start being sent.
    signal dataBuffer     : std_logic := '0'; -- which data buffer to put the data into; Other buffer is read from.
    signal weightSelect   : std_logic_vector(11 downto 0) := "010101010101"; -- Indicates which set of beamform coefficients should be used (coefficients are double-buffered). This module uses the bottom bit and shifts all the bits right before passing to the next module.
    signal calStart       : std_logic;
    signal frameCount     :  std_logic_vector(9 downto 0);
    
    signal beamData0, beamData1 : std_logic_vector(31 downto 0);
    signal beamValid0, beamValid1 : std_logic;
    
    signal regWriteData : std_logic_vector(38 downto 0);
    signal regWriteValid : std_logic;
    
    type t_slv_12_arr is array (integer range <>) of std_logic_vector(11 downto 0);
    type t_slv_32_arr is array (integer range <>) of std_logic_vector(31 downto 0);
    type t_slv_36_arr is array (integer range <>) of std_logic_vector(35 downto 0);
    type t_slv_39_arr is array (integer range <>) of std_logic_vector(38 downto 0);
    type t_slv_96_arr is array (integer range <>) of std_logic_vector(95 downto 0);
    
    signal portData_v  : t_slv_96_arr(12 downto 0);
    signal portDataValid_v : std_logic_vector(12 downto 0);
    signal portStart_v : std_logic_vector(12 downto 0); -- start of a new frame; This signals both that new data is arriving for this frame and that the beams computed for the previous frame should start being sent.
    signal dataBuffer_v: std_logic_vector(12 downto 0); -- which data buffer to put the data into; Other buffer is read from.
    signal weightSelect_v : t_slv_12_arr(12 downto 0); -- Indicates which set of beamform coefficients should be used (coefficients are double-buffered). This module uses the bottom bit and shifts all the bits right before passing to the next module.
    signal calStart_v : std_logic_vector(12 downto 0);
    
    signal beamData0_v, beamData1_v : t_slv_32_arr(12 downto 0); -- std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
    signal beamValid0_v, beamValid1_v : std_logic_vector(12 downto 0);
    
    signal regWriteData_v : t_slv_39_arr(12 downto 0);
    signal regWriteValid_v : std_logic_vector(12 downto 0);
    
    signal regReadData_v : t_slv_36_arr(12 downto 0);
    signal regReadValid_v : std_logic_vector(12 downto 0);
    
begin
    
    clkbasic : clk_wiz_0
    port map (
        clk_out1 => clk,
        clk_in1 => clkIn
    );
    
    process(clk)
    begin
        if rising_edge(clk) then
            
            portData(31 downto 0) <= std_logic_vector(unsigned(portData(31 downto 0)) + 1);
            portData(63 downto 32) <= std_logic_vector(unsigned(portData(63 downto 32)) + 2);
            portData(95 downto 64) <= std_logic_vector(unsigned(portData(95 downto 64)) + 3);
            
            if unsigned(frameCount) < 1000 then
                frameCount <= std_logic_Vector(unsigned(frameCount) + 1);
            else
                frameCount <= (others => '0');
                dataBuffer <= not dataBuffer;
                weightSelect(11 downto 1) <= weightSelect(10 downto 0);
                weightSelect(0) <= weightSelect(11); 
            end if;
            if (unsigned(frameCount) < 672) then
                portDataValid <= '1';
            else
                portDataValid <= '0';
            end if;
            if (unsigned(frameCount) = 0) then
                portStart <= '1';
            else
                portStart <= '0';
            end if;
            calStart <= '0';
            
            regWriteData(31 downto 0) <= std_logic_vector(unsigned(regWriteData(31 downto 0)) + 7);
            regWriteData(38 downto 32) <= std_logic_vector(unsigned(regWriteData(38 downto 32)) + 5);
            if (unsigned(frameCount) = 27) then
                regWriteValid <= '1';
            else
                regWriteValid <= '0';
            end if;
            
            -- Dummy output assignments to ensure it doesn't get optimised away.
            dout(0) <= beamData0_v(0)(0) xor beamData0_v(0)(4) xor beamData0_v(0)(8) xor beamData0_v(0)(12) xor beamData0_v(0)(16) xor beamData0_v(0)(20) xor beamData0_v(0)(24) xor beamData0_v(0)(28);
            dout(1) <= beamValid0;
            dout(2) <= beamData1_v(0)(0) xor beamData1_v(0)(4) xor beamData1_v(0)(8) xor beamData1_v(0)(12) xor beamData1_v(0)(16) xor beamData1_v(0)(20) xor beamData1_v(0)(24) xor beamData1_v(0)(28);
            dout(3) <= beamValid1;
            dout(4) <= regReadData_v(0)(0) xor regReadData_v(0)(5) xor regReadData_v(0)(10) xor regReadData_v(0)(15)  xor regReadData_v(0)(20)  xor regReadData_v(0)(25) xor regReadData_v(0)(30)  xor regReadData_v(0)(35);
            dout(5) <= regReadValid_v(0);
            
        end if;
    end process;

    portData_v(0) <= portData; -- : in std_logic_vector(95 downto 0);
    portDataValid_v(0) <= portDataValid; -- : in std_logic;
    portStart_v(0) <= portStart;     --: in std_logic; -- start of a new frame; This signals both that new data is arriving for this frame and that the beams computed for the previous frame should start being sent.
    dataBuffer_v(0) <= dataBuffer;    --: in std_logic; -- which data buffer to put the data into; Other buffer is read from.
    weightSelect_v(0) <= weightSelect;  -- : in std_logic_vector(11 downto 0); -- Indicates which set of beamform coefficients should be used (coefficients are double-buffered). This module uses the bottom bit and shifts all the bits right before passing to the next module.
    calStart_v(0) <= calStart;

    regWriteData_v(0) <= regWriteData; -- : in std_logic_vector(38 downto 0);
    regWriteValid_v(0) <= regWriteValid;
    
    BFGen : for i in 0 to 11 generate
        BF_i : entity work.BFsingle
        Generic map(
            g_N_CHANNELS => 12, -- : integer := 10;   -- Number of channels packed together in the input data stream
            g_THIS_CHANNEL => i, -- : integer := 0;  -- Which channel to extract in this module (0 to (g_N_CHANNELS-1)). 
            g_BEAM_ORDER => i, -- : integer := 0;    -- When is it this modules turn to put data on the beam bus ? 0 = immediately after i_update, otherwise wait for g_BEAM_ORDER packets to go past.
            g_BEAM_OUTPUT => (i mod 2) -- : integer := 0    -- Which of the two beam output buses to use ?
        ) Port map (
            i_clk => clk, -- : in std_logic; -- only one clock domain
            ----------------------------------------------------------------------------
            -- PORT DATA
            ----------------------------------------------------------------------------
            -- input Data Bus; all the data for all the ports.
            -- input data is 12+12 bits complex, packed into 4 frequency samples in each clock.
            -- up to 12 frequency channels are carried, with a new port every 3 clocks.
            -- 
            --                    port 0          port 1        port 2         port 3      ...
            --                /------------\  /-----------\  /-----------\  /-----------\  ...
            --    bits(95:72)  f9   f10  f11  f9   f10  f11  f9   f10  f11  f9   f10  f11  ...
            --    bits(71:48)  f6   f7   f8   f6   f7   f8   f6   f7   f8   f6   f7   f8   ...
            --    bits(47:24)  f3   f4   f5   f3   f4   f5   f3   f4   f5   f3   f4   f5   ...
            --    bits(23:0)   f0   f1   f2   f0   f1   f2   f0   f1   f2   f0   f1   f2   ...
            --    clock         0    1    2    3    4    5    6    7    8    9   10   11   ...
            -- So the module picks up a sample every 3rd clock.
            -- e.g. if 
            --   - g_THIS_CHANNEL = 0 => use bits(23:0), starting at clock 0
            --   - g_THIS_CHANNEL = 1 => use bits(23:0), starting at clock 1
            --   - g_THIS_CHANNEL = 2 => use bits(23:0), starting at clock 2, and set output bus bits(23:0) = 0, so it will be optimised away. 
            --   - g_THIS_CHANNEL = 3 => use bits(47:24), starting at clock 0
            --     etc.
            -- Total number of data words should be 3 * 224 = 672 (there are 224 ports; a port gets delivered every 3 clocks)
            i_portData      => portData_v(i), -- : in std_logic_vector(95 downto 0);
            i_portDataValid => portDataValid_v(i), -- : in std_logic;
            i_portStart     => portStart_v(i),     --: in std_logic; -- start of a new frame; This signals both that new data is arriving for this frame and that the beams computed for the previous frame should start being sent.
            i_dataBuffer    => dataBuffer_v(i),    --: in std_logic; -- which data buffer to put the data into; Other buffer is read from.
            i_weightSelect  => weightSelect_v(i),  -- : in std_logic_vector(11 downto 0); -- Indicates which set of beamform coefficients should be used (coefficients are double-buffered). This module uses the bottom bit and shifts all the bits right before passing to the next module.
            i_calStart      => calStart_v(i),      -- : in std_logic; -- check on i_portStart; Indicates that the calibration correlator should restart, and dump current values.
            -- output Data bus; pipelined version of the input data bus, so BFSingle units can be chained together.
            o_portData      => portData_v(i+1),  -- : out std_logic_Vector(95 downto 0);
            o_portDataValid => portDataValid_v(i+1), -- : out std_logic;
            o_portStart     => portStart_v(i+1), -- : out std_logic;
            o_dataBuffer    => dataBuffer_v(i+1), -- : out std_logic;
            o_weightSelect  => weightSelect_v(i+1), -- : out std_logic_vector(11 downto 0);
            o_calStart      => calStart_v(i+1), -- : out std_logic;
            -----------------------------------------------------------------------------
            -- BEAM DATA
            -----------------------------------------------------------------------------
            -- One beam output per clock.
            -- So e.g. for 72 dual-pol beams, there are 144 outputs cycles.
            -- Data for all beams is generated either
            --  - When i_portStart occurs, if g_BEAM_ORDER = 0
            --  - After g_BEAM_ORDER packets have gone past otherwise.
            -- Output packets are always at least one clock long; to facilitate this, the last clock of the packet is always invalid data.
            -- So, e.g. if there were 17 beams calculated, then an output packet 18 clocks long would be generated, with the last cycle of the
            -- output packet holding invalid data.
            i_beamData0     => beamData0_v(12 - i), -- : in std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
            i_beamValid0    => beamValid0_v(12 - i), -- : in std_logic;
            o_beamData0     => beamData0_v(12 - i - 1), -- : out std_logic_vector(31 downto 0);
            o_beamValid0    => beamValid0_v(12 - i - 1), -- : out std_logic;
            -- Data is put on this bus if g_BEAM_OUTPUT = 1.
            i_beamData1     => beamData1_v(12 - i), -- : in std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
            i_beamValid1    => beamValid1_v(12-i),   -- : in std_logic;
            o_beamData1     => beamData1_v(12 - i-1), -- : out std_logic_vector(31 downto 0);
            o_beamValid1    => beamValid1_v(12 - i - 1), -- : out std_logic;
            -----------------------------------------------------------------------------
            -- Register reads + writes
            -----------------------------------------------------------------------------
            -- Register data could be
            --  - Beamformer weights. These packets must align with i_portStart, since that is the only time there is bandwidth to write them to the weights ultraRAMs.
            --      - Header word on the packet is 
            --        - bits(3:0) = 0x1  - indicates that these are beamformer weights
            --        - bits(7:4) = destination; ignore this packet unless this matches g_BEAM_ORDER
            --        - bits(15:8) = number of weights in the packet, maximum per packet is 256.
            --        - bits(18:16) = Memory address to write them to, mod 6
            --        - bits(30:19) = Memory address to write them to, divided by 6
            --            - The address is sent this way to map to the weights ultraRAMs.
            --              There are 3 ultraRAMs used, with 2 weights per 72 bit word at an ultraRAM address
            --              A total of 24576 weights can be specified, double buffered with 12288 in each buffer.
            --              address/6 => 0 to 4095, with 2048 in each buffer 
            --
            --      - Data words in the packet are
            --        - 14+14 bit complex, 8 bit port to use, 3 bit instruction. Instruction is 
            --          - bit(0) = start of a new beam
            --          - bit(1) = end of a beam
            --          - bit(2) = end of all beams      
            --             -- NOTE - Weights are dealt with in groups of 12. Even though these bits come into the module for all weights, they are condensed down to 1 value for every 12 weights. 
            --                       If the number of weights desired is not a multiple of 12, then unused weights must be set to zero to make up a multiple of 12 weights for each beam.
            --                       There is no checking in this module that these values are consistent across groups of 12 weights; the last weight written in a group of 12 sets these bits for all 12 weights.
            --        - Beamformer weights come in packets of up to 256 weights.
            --  - Calibration setup
            --      - Header word on the packet is
            --        - bits(3:0) = 0x2  - indicates calibration setup command
            --        - bits(7:4) = destination; ignore this packet unless this matches g_BEAM_ORDER
            --        - bits(15:8) = port 1 to calibrate against
            --        - bits(23:16) = port 2 to calibrate against
            --      - No data.
            --  - Read back of beamformer weights
            --      - Header word on the packet is
            --        - bits(3:0) = 0x3  - indicates readback command
            --        - bits(7:4) = destination; ignore this packet unless this matches g_BEAM_ORDER
            --        - bits(15:8) = Number of weights to read
            --        - bits(31:16) = start read address of weights.
            i_regWriteData => regWriteData_v(i), -- : in std_logic_vector(38 downto 0);
            i_regWriteValid => regWriteValid_v(i), -- : in std_logic;
            -- Pipelined input bus to chain to the next instance of BFSingle
            o_regWriteData => regWriteData_v(i+1),   -- out std_logic_vector(38 downto 0);
            o_regWriteValid => regWriteValid_v(i+1), -- out std_logic
            -- Register output bus, used to read back weights and also to dump calibration correlator data.
            i_regReadData => regReadData_v(12-i),   -- in(35:0);
            i_regReadValid => regReadValid_v(12-i), -- in std_logic;
            o_regReadData => regReadData_v(12-i-1), -- out(35:0);
            o_regReadValid => regReadValid_v(12-i-1) -- out std_logic
        );
    end generate;
    
    beamData0_v(12) <= (others => '0'); -- 16+16 bit complex beam data.
    beamValid0_v(12) <= '0';

    beamData1_v(12) <= (others => '0'); -- 16+16 bit complex beam data.
    beamValid1_v(12) <= '0';

    regReadData_v(12) <= (others => '0');
    regReadValid_v(12) <= '0';

end Behavioral;
