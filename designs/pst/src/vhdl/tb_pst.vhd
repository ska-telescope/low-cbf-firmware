----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.08.2020 21:59:42
-- Design Name: 
-- Module Name: tb_pst - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE, PSR_Packetiser_lib, signal_processing_common, ethernet_lib;
library common_lib, pst_lib;
library axi4_lib;
library xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
use axi4_lib.axi4_stream_pkg.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
--use dsp_top_lib.run2_tb_pkg.ALL;
use std.textio.all;
use IEEE.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
use common_lib.common_pkg.all;
use std.env.finish;
use ethernet_lib.ethernet_pkg.ALL;

library technology_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;

entity tb_pst is
    generic (
        S_AXI_100G                          : boolean := TRUE;
        LFAA_BLOCKS_PER_FRAME_DIV3_generic  : integer := 2;
        default_bigsim                      : boolean := FALSE;
        g_folder_location                   : string := "../../../../../../../"
    ); 
      
end tb_pst;

architecture Behavioral of tb_pst is

    function choose_sim_input(a,b : string ; compare : boolean) return string is
        begin
            if compare = TRUE then
                return a;
            else
                return b;
            end if;
        end;

    constant cmd_file_name          : string(1 to 23) := "full__s_axi_tb_data.txt";
    constant RegCmd_file_name       : string(1 to 18) := "registerUpload.txt";
    constant cmd_file_name_lesspkt  : string(1 to 23) := "short_s_axi_tb_data.txt";

    constant tb_output_filename     : string(1 to 13) := "tb_result.txt";
    
    constant cmd_file_name_in_use   : string(1 to 23) := choose_sim_input(cmd_file_name, cmd_file_name_lesspkt, default_bigsim);

    signal ap_clk       : std_logic := '0';
    signal clk100       : std_logic := '0';
    signal ap_rst_n     : std_logic := '0';
    signal ap_rst       : std_logic;

    signal mc_lite_mosi : t_axi4_lite_mosi;
    signal mc_lite_miso : t_axi4_lite_miso;

    signal eth100_rx_sosi_pkt_num : integer :=0;
    signal eth100_tx_sosi_pkt_num : integer;

    signal LFAADone : std_logic := '0';
    -- The shared memory in the shell is 128Kbytes;
    -- i.e. 32k x 4 byte words. 
    type memType is array(32767 downto 0) of integer;
    shared variable sharedMem : memType;
    
    function strcmp(a, b : string) return boolean is
        alias a_val : string(1 to a'length) is a;
        alias b_val : string(1 to b'length) is b;
        variable a_char, b_char : character;
    begin
        if a'length /= b'length then
            return false;
        elsif a = b then
            return true;
        else
            return false;
        end if;
    end;
    

    COMPONENT axi_bram_RegisterSharedMem
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(16 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    END COMPONENT;
    
    --signal s_axi_aclk :  STD_LOGIC;
    --signal s_axi_aresetn :  STD_LOGIC;
    signal m00_awaddr :  STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m00_awlen :  STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal m00_awsize :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_awburst :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_awlock :  STD_LOGIC;
    signal m00_awcache :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_awprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_awvalid :  STD_LOGIC;
    signal m00_awready :  STD_LOGIC;
    signal m00_wdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_wstrb :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_wlast :  STD_LOGIC;
    signal m00_wvalid :  STD_LOGIC;
    signal m00_wready :  STD_LOGIC;
    signal m00_bresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_bvalid :  STD_LOGIC;
    signal m00_bready :  STD_LOGIC;
    signal m00_araddr :  STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m00_arlen :  STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal m00_arsize : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_arburst : STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_arlock :  STD_LOGIC;
    signal m00_arcache :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_arprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_arvalid :  STD_LOGIC;
    signal m00_arready :  STD_LOGIC;
    signal m00_rdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_rresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_rlast :  STD_LOGIC;
    signal m00_rvalid :  STD_LOGIC;
    signal m00_rready :  STD_LOGIC;
    
    signal m00_clk : std_logic;
    signal m00_we : std_logic_vector(3 downto 0);
    signal m00_addr : std_logic_vector(16 downto 0);
    signal m00_wrData : std_logic_vector(31 downto 0);
    signal m00_rdData : std_logic_vector(31 downto 0);
    

    signal m01_awvalid : std_logic;
    signal m01_awready : std_logic;
    signal m01_awaddr : std_logic_vector(63 downto 0);
    signal m01_awid : std_logic_vector(0 downto 0);
    signal m01_awlen : std_logic_vector(7 downto 0);
    signal m01_awsize : std_logic_vector(2 downto 0);
    signal m01_awburst : std_logic_vector(1 downto 0);
    signal m01_awlock :  std_logic_vector(1 downto 0);
    signal m01_awcache :  std_logic_vector(3 downto 0);
    signal m01_awprot :  std_logic_vector(2 downto 0);
    signal m01_awqos :  std_logic_vector(3 downto 0);
    signal m01_awregion :  std_logic_vector(3 downto 0);
    signal m01_wvalid :  std_logic;
    signal m01_wready :  std_logic;
    signal m01_wdata :  std_logic_vector(511 downto 0);
    signal m01_wstrb :  std_logic_vector(63 downto 0);
    signal m01_wlast :  std_logic;
    signal m01_bvalid : std_logic;
    signal m01_bready :  std_logic;
    signal m01_bresp :  std_logic_vector(1 downto 0);
    signal m01_bid :  std_logic_vector(0 downto 0);
    signal m01_arvalid :  std_logic;
    signal m01_arready :  std_logic;
    signal m01_araddr :  std_logic_vector(63 downto 0);
    signal m01_arid :  std_logic_vector(0 downto 0);
    signal m01_arlen :  std_logic_vector(7 downto 0);
    signal m01_arsize :  std_logic_vector(2 downto 0);
    signal m01_arburst : std_logic_vector(1 downto 0);
    signal m01_arlock :  std_logic_vector(1 downto 0);
    signal m01_arcache :  std_logic_vector(3 downto 0);
    signal m01_arprot :  std_logic_Vector(2 downto 0);
    signal m01_arqos :  std_logic_vector(3 downto 0);
    signal m01_arregion :  std_logic_vector(3 downto 0);
    signal m01_rvalid :  std_logic;
    signal m01_rready :  std_logic;
    signal m01_rdata :  std_logic_vector(511 downto 0);
    signal m01_rlast :  std_logic;
    signal m01_rid :  std_logic_vector(0 downto 0);
    signal m01_rresp :  std_logic_vector(1 downto 0);
    
    signal m02_awvalid : std_logic;
    signal m02_awready : std_logic;
    signal m02_awaddr : std_logic_vector(63 downto 0);
    signal m02_awid : std_logic_vector(0 downto 0);
    signal m02_awlen : std_logic_vector(7 downto 0);
    signal m02_awsize : std_logic_vector(2 downto 0);
    signal m02_awburst : std_logic_vector(1 downto 0);
    signal m02_awlock :  std_logic_vector(1 downto 0);
    signal m02_awcache :  std_logic_vector(3 downto 0);
    signal m02_awprot :  std_logic_vector(2 downto 0);
    signal m02_awqos :  std_logic_vector(3 downto 0);
    signal m02_awregion :  std_logic_vector(3 downto 0);
    signal m02_wvalid :  std_logic;
    signal m02_wready :  std_logic;
    signal m02_wdata :  std_logic_vector(255 downto 0);
    signal m02_wstrb :  std_logic_vector(31 downto 0);
    signal m02_wlast :  std_logic;
    signal m02_bvalid : std_logic;
    signal m02_bready :  std_logic;
    signal m02_bresp :  std_logic_vector(1 downto 0);
    signal m02_bid :  std_logic_vector(0 downto 0);
    signal m02_arvalid :  std_logic;
    signal m02_arready :  std_logic;
    signal m02_araddr :  std_logic_vector(63 downto 0);
    signal m02_arid :  std_logic_vector(0 downto 0);
    signal m02_arlen :  std_logic_vector(7 downto 0);
    signal m02_arsize :  std_logic_vector(2 downto 0);
    signal m02_arburst : std_logic_vector(1 downto 0);
    signal m02_arlock :  std_logic_vector(1 downto 0);
    signal m02_arcache :  std_logic_vector(3 downto 0);
    signal m02_arprot :  std_logic_Vector(2 downto 0);
    signal m02_arqos :  std_logic_vector(3 downto 0);
    signal m02_arregion :  std_logic_vector(3 downto 0);
    signal m02_rvalid :  std_logic;
    signal m02_rready :  std_logic;
    signal m02_rdata :  std_logic_vector(255 downto 0);
    signal m02_rlast :  std_logic;
    signal m02_rid :  std_logic_vector(0 downto 0);
    signal m02_rresp :  std_logic_vector(1 downto 0);

    signal m03_awvalid : std_logic;
    signal m03_awready : std_logic;
    signal m03_awaddr : std_logic_vector(63 downto 0);
    signal m03_awid : std_logic_vector(0 downto 0);
    signal m03_awlen : std_logic_vector(7 downto 0);
    signal m03_awsize : std_logic_vector(2 downto 0);
    signal m03_awburst : std_logic_vector(1 downto 0);
    signal m03_awlock :  std_logic_vector(1 downto 0);
    signal m03_awcache :  std_logic_vector(3 downto 0);
    signal m03_awprot :  std_logic_vector(2 downto 0);
    signal m03_awqos :  std_logic_vector(3 downto 0);
    signal m03_awregion :  std_logic_vector(3 downto 0);
    signal m03_wvalid :  std_logic;
    signal m03_wready :  std_logic;
    signal m03_wdata :  std_logic_vector(255 downto 0);
    signal m03_wstrb :  std_logic_vector(31 downto 0);
    signal m03_wlast :  std_logic;
    signal m03_bvalid : std_logic;
    signal m03_bready :  std_logic;
    signal m03_bresp :  std_logic_vector(1 downto 0);
    signal m03_bid :  std_logic_vector(0 downto 0);
    signal m03_arvalid :  std_logic;
    signal m03_arready :  std_logic;
    signal m03_araddr :  std_logic_vector(63 downto 0);
    signal m03_arid :  std_logic_vector(0 downto 0);
    signal m03_arlen :  std_logic_vector(7 downto 0);
    signal m03_arsize :  std_logic_vector(2 downto 0);
    signal m03_arburst : std_logic_vector(1 downto 0);
    signal m03_arlock :  std_logic_vector(1 downto 0);
    signal m03_arcache :  std_logic_vector(3 downto 0);
    signal m03_arprot :  std_logic_Vector(2 downto 0);
    signal m03_arqos :  std_logic_vector(3 downto 0);
    signal m03_arregion :  std_logic_vector(3 downto 0);
    signal m03_rvalid :  std_logic;
    signal m03_rready :  std_logic;
    signal m03_rdata :  std_logic_vector(255 downto 0);
    signal m03_rlast :  std_logic;
    signal m03_rid :  std_logic_vector(0 downto 0);
    signal m03_rresp :  std_logic_vector(1 downto 0);

    signal m04_awvalid : std_logic;
    signal m04_awready : std_logic;
    signal m04_awaddr : std_logic_vector(63 downto 0);
    signal m04_awid : std_logic_vector(0 downto 0);
    signal m04_awlen : std_logic_vector(7 downto 0);
    signal m04_awsize : std_logic_vector(2 downto 0);
    signal m04_awburst : std_logic_vector(1 downto 0);
    signal m04_awlock :  std_logic_vector(1 downto 0);
    signal m04_awcache :  std_logic_vector(3 downto 0);
    signal m04_awprot :  std_logic_vector(2 downto 0);
    signal m04_awqos :  std_logic_vector(3 downto 0);
    signal m04_awregion :  std_logic_vector(3 downto 0);
    signal m04_wvalid :  std_logic;
    signal m04_wready :  std_logic;
    signal m04_wdata :  std_logic_vector(255 downto 0);
    signal m04_wstrb :  std_logic_vector(31 downto 0);
    signal m04_wlast :  std_logic;
    signal m04_bvalid : std_logic;
    signal m04_bready :  std_logic;
    signal m04_bresp :  std_logic_vector(1 downto 0);
    signal m04_bid :  std_logic_vector(0 downto 0);
    signal m04_arvalid :  std_logic;
    signal m04_arready :  std_logic;
    signal m04_araddr :  std_logic_vector(63 downto 0);
    signal m04_arid :  std_logic_vector(0 downto 0);
    signal m04_arlen :  std_logic_vector(7 downto 0);
    signal m04_arsize :  std_logic_vector(2 downto 0);
    signal m04_arburst : std_logic_vector(1 downto 0);
    signal m04_arlock :  std_logic_vector(1 downto 0);
    signal m04_arcache :  std_logic_vector(3 downto 0);
    signal m04_arprot :  std_logic_Vector(2 downto 0);
    signal m04_arqos :  std_logic_vector(3 downto 0);
    signal m04_arregion :  std_logic_vector(3 downto 0);
    signal m04_rvalid :  std_logic;
    signal m04_rready :  std_logic;
    signal m04_rdata :  std_logic_vector(255 downto 0);
    signal m04_rlast :  std_logic;
    signal m04_rid :  std_logic_vector(0 downto 0);
    signal m04_rresp :  std_logic_vector(1 downto 0);    
    
    signal m05_awvalid : std_logic;
    signal m05_awready : std_logic;
    signal m05_awaddr : std_logic_vector(63 downto 0);
    signal m05_awid : std_logic_vector(0 downto 0);
    signal m05_awlen : std_logic_vector(7 downto 0);
    signal m05_awsize : std_logic_vector(2 downto 0);
    signal m05_awburst : std_logic_vector(1 downto 0);
    signal m05_awlock :  std_logic_vector(1 downto 0);
    signal m05_awcache :  std_logic_vector(3 downto 0);
    signal m05_awprot :  std_logic_vector(2 downto 0);
    signal m05_awqos :  std_logic_vector(3 downto 0);
    signal m05_awregion :  std_logic_vector(3 downto 0);
    signal m05_wvalid :  std_logic;
    signal m05_wready :  std_logic;
    signal m05_wdata :  std_logic_vector(255 downto 0);
    signal m05_wstrb :  std_logic_vector(31 downto 0);
    signal m05_wlast :  std_logic;
    signal m05_bvalid : std_logic;
    signal m05_bready :  std_logic;
    signal m05_bresp :  std_logic_vector(1 downto 0);
    signal m05_bid :  std_logic_vector(0 downto 0);
    signal m05_arvalid :  std_logic;
    signal m05_arready :  std_logic;
    signal m05_araddr :  std_logic_vector(63 downto 0);
    signal m05_arid :  std_logic_vector(0 downto 0);
    signal m05_arlen :  std_logic_vector(7 downto 0);
    signal m05_arsize :  std_logic_vector(2 downto 0);
    signal m05_arburst : std_logic_vector(1 downto 0);
    signal m05_arlock :  std_logic_vector(1 downto 0);
    signal m05_arcache :  std_logic_vector(3 downto 0);
    signal m05_arprot :  std_logic_Vector(2 downto 0);
    signal m05_arqos :  std_logic_vector(3 downto 0);
    signal m05_arregion :  std_logic_vector(3 downto 0);
    signal m05_rvalid :  std_logic;
    signal m05_rready :  std_logic;
    signal m05_rdata :  std_logic_vector(255 downto 0);
    signal m05_rlast :  std_logic;
    signal m05_rid :  std_logic_vector(0 downto 0);
    signal m05_rresp :  std_logic_vector(1 downto 0);    
   
    signal m06_awvalid  : std_logic;
    signal m06_awready  : std_logic;
    signal m06_awaddr   : std_logic_vector(63 downto 0);
    signal m06_awid     : std_logic_vector(0 downto 0);
    signal m06_awlen    : std_logic_vector(7 downto 0);
    signal m06_awsize   : std_logic_vector(2 downto 0);
    signal m06_awburst  : std_logic_vector(1 downto 0);
    signal m06_awlock   :  std_logic_vector(1 downto 0);
    signal m06_awcache  :  std_logic_vector(3 downto 0);
    signal m06_awprot   :  std_logic_vector(2 downto 0);
    signal m06_awqos    : std_logic_vector(3 downto 0);
    signal m06_awregion : std_logic_vector(3 downto 0);
    signal m06_wvalid   : std_logic;
    signal m06_wready   : std_logic;
    signal m06_wdata    : std_logic_vector(511 downto 0);
    signal m06_wstrb    : std_logic_vector(63 downto 0);
    signal m06_wlast    : std_logic;
    signal m06_bvalid   : std_logic;
    signal m06_bready   : std_logic;
    signal m06_bresp    : std_logic_vector(1 downto 0);
    signal m06_bid      : std_logic_vector(0 downto 0);
    signal m06_arvalid  : std_logic;
    signal m06_arready  : std_logic;
    signal m06_araddr   : std_logic_vector(63 downto 0);
    signal m06_arid     : std_logic_vector(0 downto 0);
    signal m06_arlen    : std_logic_vector(7 downto 0);
    signal m06_arsize   : std_logic_vector(2 downto 0);
    signal m06_arburst  : std_logic_vector(1 downto 0);
    signal m06_arlock   : std_logic_vector(1 downto 0);
    signal m06_arcache  : std_logic_vector(3 downto 0);
    signal m06_arprot   : std_logic_Vector(2 downto 0);
    signal m06_arqos    : std_logic_vector(3 downto 0);
    signal m06_arregion : std_logic_vector(3 downto 0);
    signal m06_rvalid   : std_logic;
    signal m06_rready   : std_logic;
    signal m06_rdata    : std_logic_vector(511 downto 0);
    signal m06_rlast    : std_logic;
    signal m06_rid      : std_logic_vector(0 downto 0);
    signal m06_rresp    : std_logic_vector(1 downto 0);

    constant g_HBM_INTERFACES       : integer := 6;
    constant g_HBM_AXI_ADDR_WIDTH   : integer := 64;
    constant g_HBM_AXI_DATA_WIDTH   : integer := 512;
    constant g_HBM_AXI_ID_WIDTH     : integer := 1;
    
    ---------------------------------------------------------------------------------------
    -- AXI4 interfaces for accessing HBM
    -- 2 per pipeline.
    -- 1 = 2 Gbytes, CT1
    --     
    -- 2 = 1 Gbytes, CT2
    --   
    signal HBM_axi_awvalid  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awready  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awaddr   : t_slv_64_arr(g_HBM_INTERFACES-1 downto 0); -- out std_logic_vector(M01_AXI_ADDR_WIDTH-1 downto 0);
    signal HBM_axi_awid     : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(M01_AXI_ID_WIDTH - 1 downto 0);
    signal HBM_axi_awlen    : t_slv_8_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(7 downto 0);
    signal HBM_axi_awsize   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(2 downto 0);
    signal HBM_axi_awburst  : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(1 downto 0);
    signal HBM_axi_awlock   : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(1 downto 0);
    signal HBM_axi_awcache  : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(3 downto 0);
    signal HBM_axi_awprot   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(2 downto 0);
    signal HBM_axi_awqos    : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);  -- out std_logic_vector(3 downto 0);
    signal HBM_axi_awregion : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(3 downto 0);
    
    signal HBM_axi_wvalid   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wready   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wdata    : t_slv_512_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_DATA_WIDTH-1 downto 0);
    signal HBM_axi_wstrb    : t_slv_64_arr(g_HBM_INTERFACES-1 downto 0);  -- std_logic_vector(M01_AXI_DATA_WIDTH/8-1 downto 0);
    signal HBM_axi_wlast    : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bvalid   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bready   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bresp    : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(1 downto 0);
    signal HBM_axi_bid      : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_ID_WIDTH - 1 downto 0);
    signal HBM_axi_arvalid  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arready  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_araddr   : t_slv_64_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_ADDR_WIDTH-1 downto 0);
    signal HBM_axi_arid     : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_ID_WIDTH-1 downto 0);
    signal HBM_axi_arlen    : t_slv_8_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(7 downto 0);
    signal HBM_axi_arsize   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(2 downto 0);
    signal HBM_axi_arburst  : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(1 downto 0);
    signal HBM_axi_arlock   : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(1 downto 0);
    signal HBM_axi_arcache  : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(3 downto 0);
    signal HBM_axi_arprot   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_Vector(2 downto 0);
    signal HBM_axi_arqos    : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(3 downto 0);
    signal HBM_axi_arregion : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(3 downto 0);
    signal HBM_axi_rvalid   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rready   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rdata    : t_slv_512_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_DATA_WIDTH-1 downto 0);
    signal HBM_axi_rlast    : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rid      : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_ID_WIDTH - 1 downto 0);
    signal HBM_axi_rresp    : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(1 downto 0);


    signal eth100_rx_sosi   : t_lbus_sosi;
    signal eth100_tx_sosi   : t_lbus_sosi;
    signal eth100_tx_siso   : t_lbus_siso;
    signal setupDone        : std_logic;
    signal eth100G_clk      : std_logic := '0';
    signal eth100G_rst      : std_logic := '0';
    signal eth100G_rstn     : std_logic := '0';
    
    signal m00_bram_we : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_bram_en : STD_LOGIC;
    signal m00_bram_addr : STD_LOGIC_VECTOR(16 DOWNTO 0);
    signal m00_bram_addr_word : std_logic_vector(14 downto 0);
    signal m00_bram_wrData : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_bram_rdData : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_bram_clk : std_logic;
    signal validMemRstActive : std_logic; 

    
    signal m02_arFIFO_dout, m02_arFIFO_din : std_logic_vector(63 downto 0);
    signal m02_arFIFO_empty, m02_arFIFO_rdEn, m02_arFIFO_wrEn : std_logic;
    signal m02_arFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal M02_READ_QUEUE_SIZE, MIN_LAG : integer;
    signal m02_arlen_delayed : std_logic_vector(7 downto 0);
    signal m02_arsize_delayed : std_logic_vector(2 downto 0);
    signal m02_arburst_delayed : std_logic_vector(1 downto 0);
    signal m02_arcache_delayed : std_logic_vector(3 downto 0);
    signal m02_arprot_delayed : std_logic_vector(2 downto 0);
    signal m02_arqos_delayed : std_logic_vector(3 downto 0);
    signal m02_arregion_delayed : std_logic_vector(3 downto 0);
    
    signal m02_araddr_delayed : std_logic_vector(19 downto 0);
    signal m02_reqTime : std_logic_vector(31 downto 0);
    signal m02_arvalid_delayed, m02_arready_delayed : std_logic;
    
    
    signal wr_addr_x410E0, rd_addr_x410E0 : std_logic := '0'; 
    signal wrdata_x410E0, rddata_x410E0 : std_logic := '0';

    signal rx_axis_tdata : std_logic_vector(511 downto 0);
    signal rx_axis_tkeep : std_logic_vector(63 downto 0);
    signal rx_axis_tlast : std_logic;
    signal rx_axis_tready : std_logic;
    signal rx_axis_tuser : std_logic_vector(79 downto 0);
    signal rx_axis_tvalid : std_logic;
    signal PTP_time_ARGs_clk : std_logic_vector(79 downto 0);

    signal tx_axis_tdata : std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
    signal tx_axis_tkeep : std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
    signal tx_axis_tlast : std_logic;
    signal tx_axis_tuser : std_logic;
    signal tx_axis_tvalid : std_logic;
    signal tx_axis_tready : std_logic;

    signal cmac_mc_lite_mosi : t_axi4_lite_mosi; 
    signal cmac_mc_lite_miso : t_axi4_lite_miso;

    signal timeslave_mc_full_mosi : t_axi4_full_mosi;
    signal timeslave_mc_full_miso : t_axi4_full_miso;

    signal testCount_322            : integer   := 0;

    signal clock_322_rst            : std_logic := '1';

    signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;
    
    signal input_HBM_reset          : std_logic_vector(5 downto 0);
   

    -- Do an axi-lite read of a single 32-bit register.
    PROCEDURE axi_lite_rd(SIGNAL mm_clk   : IN STD_LOGIC;
                          SIGNAL axi_miso : IN t_axi4_lite_miso;
                          SIGNAL axi_mosi : OUT t_axi4_lite_mosi;
                          register_addr   : NATURAL;  -- 4-byte word address
                          variable rd_data  : out std_logic_vector(31 downto 0)) is

        VARIABLE stdio             : line;
        VARIABLE result            : STD_LOGIC_VECTOR(31 DOWNTO 0);
        variable wvalidInt         : std_logic;
        variable awvalidInt        : std_logic;
        
    BEGIN

        -- Start transaction
        WAIT UNTIL rising_edge(mm_clk);
            -- Setup read address
            axi_mosi.arvalid <= '1';
            axi_mosi.araddr <= std_logic_vector(to_unsigned(register_addr*4, 32));
            axi_mosi.rready <= '1';

        read_address_wait: LOOP
            WAIT UNTIL rising_edge(mm_clk);
            IF axi_miso.arready = '1' THEN
               axi_mosi.arvalid <= '0';
               axi_mosi.araddr <= (OTHERS => '0');
            END IF;

            IF axi_miso.rvalid = '1' THEN
               EXIT;
            END IF;
        END LOOP;

        
        rd_data := axi_miso.rdata(31 downto 0);
        -- Read response
        IF axi_miso.rresp = "01" THEN
            write(stdio, string'("exclusive access error "));
            writeline(output, stdio);
        ELSIF axi_miso.rresp = "10" THEN
            write(stdio, string'("slave error "));
            writeline(output, stdio);
        ELSIF axi_miso.rresp = "11" THEN
           write(stdio, string'("address decode error "));
           writeline(output, stdio);
        END IF;


        
        WAIT UNTIL rising_edge(mm_clk);
        axi_mosi.rready <= '0';
    end procedure;
        
begin

    ap_clk <= not ap_clk after 2.5 ns; -- 200 MHz clock.
    clk100 <= not clk100 after 5 ns; -- 100 MHz clock
    eth100G_clk <= not eth100G_clk after 1.553 ns; -- 322 MHz

-----------------------------------------------------------------------------
-- Reset for 100G clock
    eth100G_reset_proc : process(eth100G_clk)
    begin
        if rising_edge(eth100G_clk) then
            -- power up reset logic
            if power_up_rst_clock_322(31) = '1' then
                power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
                clock_322_rst   <= '1';
                testCount_322   <= 0;
            else
                clock_322_rst   <= '0';
                testCount_322   <= testCount_322 + 1;
            end if;
        end if;
    end process;

    eth100G_rst     <= clock_322_rst;
    eth100G_rstn    <= NOT clock_322_rst;
-----------------------------------------------------------------------------

PRIMARY_LOOP : process
        file RegCmdfile: TEXT;
        variable RegLine_in : Line;
        variable RegGood : boolean;
        variable cmd_str : string(1 to 2);
        variable regAddr : std_logic_vector(31 downto 0);
        variable regSize : std_logic_vector(31 downto 0);
        variable regData : std_logic_vector(31 downto 0);
        variable readResult : std_logic_vector(31 downto 0);
    begin        
        SetupDone <= '0';
        ap_rst_n <= '1';
        
        FILE_OPEN(RegCmdfile,g_folder_location & RegCmd_file_name,READ_MODE);
        
        for i in 1 to 10 loop
            WAIT UNTIL RISING_EDGE(ap_clk);
        end loop;
        ap_rst_n <= '0';
        for i in 1 to 10 loop
             WAIT UNTIL RISING_EDGE(ap_clk);
        end loop;
        ap_rst_n <= '1';
        
        for i in 1 to 100 loop
             WAIT UNTIL RISING_EDGE(ap_clk);
        end loop;
        
        input_HBM_reset     <= (others => '0');
        
        -- For some reason the first transaction doesn't work; this is just a dummy transaction
        -- Arguments are       clk,    miso      ,    mosi     , 4-byte word Addr, write ?, data)
        axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 0,    true, x"00000000");
        
        -- Addresses in the axi lite control module
        -- ADDR_AP_CTRL         = 6'h00,
        -- ADDR_DMA_SRC_0       = 6'h10,
        -- ADDR_DMA_DEST_0      = 6'h14,
        -- ADDR_DMA_SHARED_0    = 6'h18,
        -- ADDR_DMA_SHARED_1    = 6'h1C,
        -- ADDR_DMA_SIZE        = 6'h20,
        --
        axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 6, true, x"DEF20000");  -- Full address of the shared memory; arbitrary so long as it is 128K aligned.
        axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 7, true, x"56789ABC");  -- High 32 bits of the  address of the shared memory; arbitrary.
        
        
        -- Pseudo code :
        --
        --  Repeat while there are commands in the command file:
        --    - Read command from the file (either read or write, together with the ARGs register address)
        --        - Possible commands : [read address length]   <-- Does a register read.
        --                              [write address length]  <-- Does a register write
        --    - If this is a write, then read the write data from the file, and copy into a shared variable used by the memory.
        --    - trigger the kernel to do the register read/write.
        --  Trigger sending of the 100G test data.
        --
        
        while (not endfile(RegCmdfile)) loop 
            readline(RegCmdfile, RegLine_in);
            
            assert False report "Processing register command " & RegLine_in.all severity note;
            
            -- line_in should have a command; process it.
            read(RegLine_in,cmd_str,RegGood);   -- cmd should be either "rd" or "wr"
            hread(RegLine_in,regAddr,regGood);  -- then the register address
            hread(RegLine_in,regSize,regGood);  -- then the number of words
            
            if strcmp(cmd_str,"wr") then
            
                -- The command is a write, so get the write data into the shared memory variable "sharedMem"
                -- Divide by 4 to get the number of words, since the size is in bytes.
                for i in 0 to (to_integer(unsigned(regSize(31 downto 2)))-1) loop
                    readline(regCmdFile, regLine_in);
                    hread(regLine_in,regData,regGood);
                    sharedMem(i) := to_integer(signed(regData));
                end loop;
                
                -- put in the source address. For writes, the source address is the shared memory, which is mapped to word address 0x8000 (=byte address 0x20000) in the ARGS address space.
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 4, true, x"00020000"); 
                -- Put in the destination address in the ARGS memory space.
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 5, true, regAddr);
                -- Size = number of bytes to transfer. 
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 8, true, regSize);

            elsif strcmp(cmd_str,"rd") then
                -- Put in the source address
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 4, true, regAddr);      -- source Address
                -- Destination Address - the shared memory. 
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 5, true, x"00020000");  
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 8, true, regSize);      -- size of the transaction in bytes
                
            else
                assert False report "Bad data in register command file" severity error;
            end if;
   
            -- trigger the command
            axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 0, true, x"00000001");
        
            -- Poll until the command is done
            readResult := (others => '0');
            while (readResult(1) = '0') loop
                axi_lite_rd(ap_clk, mc_lite_miso, mc_lite_mosi, 0, readResult);
                for i in 1 to 10 loop
                    WAIT UNTIL RISING_EDGE(ap_clk);
                end loop;
            end loop;
            
        end loop;

        if validMemRstActive = '1' then
            wait until validMemRstActive = '0';
        end if;
        
        SetupDone <= '1';
        
        wait until LFAADone = '1';

        if (default_bigsim = TRUE) then
            wait until eth100_tx_sosi_pkt_num = 486;
        else
            wait until eth100_tx_sosi_pkt_num = 108; 
        end if;

        report "number of tx packets all received";

        wait for 10 us;
        report "simulation successfully finished";
        finish;

    end process;
    
    
    m00_bram_addr_word <= m00_bram_addr(16 downto 2);
    
    process(m00_bram_clk)
    begin
        if rising_edge(m00_bram_clk) then
            m00_bram_rdData <= std_logic_vector(to_signed(sharedMem(to_integer(unsigned(m00_bram_addr_word))),32)); 
            
            if m00_bram_we(0) = '1' and m00_bram_en = '1' then
                sharedMem(to_integer(unsigned(m00_bram_addr_word))) := to_integer(signed(m00_bram_wrData));
            end if;
            
            assert (m00_bram_we(3 downto 0) /= "0000" or m00_bram_we(3 downto 0) /= "1111") report "Byte wide write enables should never occur to shared memory" severity error;
            
        end if;
    end process;
    
    -----------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------
    -- axi BRAM controller to interface to the shared memory for register reads and writes.
    
    registerSharedMem : axi_bram_RegisterSharedMem
    PORT MAP (
        s_axi_aclk => ap_clk,
        s_axi_aresetn => ap_rst_n,
        s_axi_awaddr => m00_awaddr(16 downto 0),
        s_axi_awlen => m00_awlen,
        s_axi_awsize => m00_awsize,
        s_axi_awburst => m00_awburst,
        s_axi_awlock => '0',
        s_axi_awcache => m00_awcache,
        s_axi_awprot => m00_awprot,
        s_axi_awvalid => m00_awvalid,
        s_axi_awready => m00_awready,
        s_axi_wdata => m00_wdata,
        s_axi_wstrb => m00_wstrb,
        s_axi_wlast => m00_wlast,
        s_axi_wvalid => m00_wvalid,
        s_axi_wready => m00_wready,
        s_axi_bresp => m00_bresp,
        s_axi_bvalid => m00_bvalid,
        s_axi_bready => m00_bready,
        s_axi_araddr => m00_araddr(16 downto 0),
        s_axi_arlen => m00_arlen,
        s_axi_arsize => m00_arsize,
        s_axi_arburst => m00_arburst,
        s_axi_arlock => '0',
        s_axi_arcache => m00_arcache,
        s_axi_arprot => m00_arprot,
        s_axi_arvalid => m00_arvalid,
        s_axi_arready => m00_arready,
        s_axi_rdata => m00_rdata,
        s_axi_rresp => m00_rresp,
        s_axi_rlast => m00_rlast,
        s_axi_rvalid => m00_rvalid,
        s_axi_rready => m00_rready,
        bram_rst_a => open,   -- OUT STD_LOGIC;
        bram_clk_a => m00_bram_clk, -- OUT STD_LOGIC;
        bram_en_a  => m00_bram_en, -- OUT STD_LOGIC;
        bram_we_a  => m00_bram_we, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a => m00_bram_addr, -- OUT STD_LOGIC_VECTOR(16 DOWNTO 0);
        bram_wrdata_a => m00_bram_wrData, -- OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a => m00_bram_rdData  -- IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
    
    -------------------------------------------------------------------------------------------
    -- 100 GE data input, LBUS
LBUS_gen : if (NOT S_AXI_100G) GENERATE

process
        file cmdfile: TEXT;
        variable line_in : Line;
        variable good : boolean;
        variable LFAArepeats : std_logic_vector(15 downto 0);
        variable LFAAData  : std_logic_vector(511 downto 0);
        variable LFAAvalid : std_logic_vector(3 downto 0);
        variable LFAAeop   : std_logic_vector(3 downto 0);
        variable LFAAerror : std_logic_vector(3 downto 0);
        variable LFAAempty0 : std_logic_vector(3 downto 0);
        variable LFAAempty1 : std_logic_vector(3 downto 0);
        variable LFAAempty2 : std_logic_vector(3 downto 0);
        variable LFAAempty3 : std_logic_vector(3 downto 0);
        variable LFAAsop    : std_logic_vector(3 downto 0);
    begin
        
        eth100_rx_sosi.data <= (others => '0');  -- 512 bits
        eth100_rx_sosi.valid <= "0000";          -- 4 bits
        eth100_rx_sosi.eop <= "0000";  
        eth100_rx_sosi.sop <= "0000";
        eth100_rx_sosi.error <= "0000";
        eth100_rx_sosi.empty(0) <= "0000";
        eth100_rx_sosi.empty(1) <= "0000";
        eth100_rx_sosi.empty(2) <= "0000";
        eth100_rx_sosi.empty(3) <= "0000";
       
        if (default_bigsim = TRUE) then
	   report "simulation is using long time";	
           FILE_OPEN(cmdfile,cmd_file_name,READ_MODE);
	else
	   report "simulation is using short time";
	   FILE_OPEN(cmdfile,cmd_file_name_lesspkt,READ_MODE);
        end if; 

        wait until SetupDone = '1';
        
        wait until rising_edge(eth100G_clk);
        
        while (not endfile(cmdfile)) loop 
            readline(cmdfile, line_in);
            hread(line_in,LFAArepeats,good);
            hread(line_in,LFAAData,good);
            hread(line_in,LFAAvalid,good);
            hread(line_in,LFAAeop,good);
            hread(line_in,LFAAerror,good);
            hread(line_in,LFAAempty0,good);
            hread(line_in,LFAAempty1,good);
            hread(line_in,LFAAempty2,good);
            hread(line_in,LFAAempty3,good);
            hread(line_in,LFAAsop,good);
           
	    if (LFAAeop /= "0000") then
	       eth100_rx_sosi_pkt_num <= eth100_rx_sosi_pkt_num + 1;
	    end if;
	     
            eth100_rx_sosi.data <= LFAAData;  -- 512 bits
            eth100_rx_sosi.valid <= LFAAValid;          -- 4 bits
            eth100_rx_sosi.eop <= LFAAeop;
            eth100_rx_sosi.sop <= LFAAsop;
            eth100_rx_sosi.error <= LFAAerror;
            eth100_rx_sosi.empty(0) <= LFAAempty0;
            eth100_rx_sosi.empty(1) <= LFAAempty1;
            eth100_rx_sosi.empty(2) <= LFAAempty2;
            eth100_rx_sosi.empty(3) <= LFAAempty3;
            
            wait until rising_edge(eth100G_clk);
            while LFAArepeats /= "0000000000000000" loop
                LFAArepeats := std_logic_vector(unsigned(LFAArepeats) - 1);
                wait until rising_edge(eth100G_clk);
            end loop;
        end loop;
         
        LFAADone <= '1';
	if (default_bigsim = TRUE) then
           wait until eth100_tx_sosi_pkt_num = 486;
	else
           wait until eth100_tx_sosi_pkt_num = 108; 
	end if;
        report "number of tx packets all received";

	    wait for 100 us;
	    report "simulation successfully finished";
	    finish;
    end process;
    
    eth100_tx_siso.ready <= '1';
    eth100_tx_siso.underflow <= '0';
    eth100_tx_siso.overflow <= '0';
    
    -- Capture data packets in eth100_tx_sosi to a file.
    -- fields in eth100_tx_sosi are
    --   .data(511:0)
    --   .valid(3:0)
    --   .eop(3:0)
    --   .sop(3:0)
    --   .empty(3:0)(3:0)
    
    lbusRX : entity pst_lib.lbus_packet_receive
    Generic map (
        log_file_name => "lbus_out.txt"
    )
    Port map ( 
        clk      => eth100G_clk, -- in  std_logic;     -- clock
        i_rst    => '0', -- in  std_logic;     -- reset input
        i_din    => eth100_tx_sosi.data, -- in  std_logic_vector(511 downto 0);  -- actual data out.
        i_valid  => eth100_tx_sosi.valid, -- in  std_logic_vector(3 downto 0);     -- data out valid (high for duration of the packet)
        i_eop    => eth100_tx_sosi.eop,   -- in  std_logic_vector(3 downto 0);
        i_sop    => eth100_tx_sosi.sop,   -- in  std_logic_vector(3 downto 0);
        i_empty0 => eth100_tx_sosi.empty(0), --  in std_logic_vector(3 downto 0);
        i_empty1 => eth100_tx_sosi.empty(1), -- in std_logic_vector(3 downto 0);
        i_empty2 => eth100_tx_sosi.empty(2), -- in std_logic_vector(3 downto 0);
        i_empty3 => eth100_tx_sosi.empty(3),  -- in std_logic_vector(3 downto 0)
	tx_pkt_num => eth100_tx_sosi_pkt_num
    );

END GENERATE;

S_AXI_gen : if (S_AXI_100G) GENERATE

ap_rst  <= NOT ap_rst_n;

tx_saxi_stub : entity pst_lib.s_axi_to_txt 
    generic map (
        g_output_folder_location    => g_folder_location,
        
        g_output_file_name          => tb_output_filename
    )
    Port map (
        streaming_axi_clock     => eth100G_clk,
        streaming_axi_reset     => eth100G_rst,

        streaming_axi_tdata     => tx_axis_tdata,
        streaming_axi_tkeep     => tx_axis_tkeep,
        streaming_axi_tlast     => tx_axis_tlast,
        streaming_axi_tuser     => tx_axis_tuser,
        streaming_axi_tvalid    => tx_axis_tvalid,
        
        tx_pkt_num              => eth100_tx_sosi_pkt_num
    );


rx_saxi_stub : entity pst_lib.txt_to_s_axi 
    generic map (
        g_input_folder_location     => g_folder_location,
        
        g_input_file_name           => cmd_file_name_in_use
    )
    Port map (
        streaming_axi_clock     => eth100G_clk,
        streaming_axi_reset     => eth100G_rst,

        streaming_axi_tdata     => rx_axis_tdata,
        streaming_axi_tkeep     => rx_axis_tkeep,
        streaming_axi_tlast     => rx_axis_tlast,
        streaming_axi_tuser     => open,
        streaming_axi_tvalid    => rx_axis_tvalid,

        start_reading           => SetupDone,
        end_of_file             => LFAADone
    );


END GENERATE;
    
    dut : entity pst_lib.pstBeamformCore
    generic map (
        g_SIMULATION => TRUE, -- BOOLEAN;  -- when true, the 100GE core is disabled and instead the lbus comes from the top level pins
        g_USE_META => FALSE,   -- BOOLEAN;  -- puts meta data in place of the filterbank data in the corner turn, to help debug the corner turn.
        -- GLOBAL GENERICS for PERENTIE LOGIC
        g_DEBUG_ILA                  => FALSE, --  BOOLEAN
        --g_LFAA_BLOCKS_PER_FRAME_DIV3 => LFAA_BLOCKS_PER_FRAME_DIV3_generic, -- integer := 9;  -- Number of LFAA blocks per frame divided by 3; minimum value is 1, i.e. 3 LFAA blocks per frame.
        g_PST_BEAMS                  => 2, -- integer := 16;
        C_S_AXI_CONTROL_ADDR_WIDTH   => 8,  -- integer := 7;
        C_S_AXI_CONTROL_DATA_WIDTH   => 32, -- integer := 32;
        C_M_AXI_ADDR_WIDTH => 64,         -- integer := 64;
        C_M_AXI_DATA_WIDTH => 32,         -- integer := 32;
        C_M_AXI_ID_WIDTH => 1,             -- integer := 1
        
--        g_HBM_INTERFACES                : integer := 6;
--        g_HBM_AXI_ADDR_WIDTH            : integer := 64;
--        g_HBM_AXI_DATA_WIDTH            : integer := 512;
--        g_HBM_AXI_ID_WIDTH              : integer := 1;
        
        M01_AXI_ADDR_WIDTH => 64,
        M01_AXI_DATA_WIDTH => 512,
        M01_AXI_ID_WIDTH   => 1,
        M02_AXI_ADDR_WIDTH => 64,  -- m02 interface to HBM is used for the corner turn between the filterbanks and the beamformer. 
        M02_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec. 
        M02_AXI_ID_WIDTH   => 1,
        M03_AXI_ADDR_WIDTH => 64,  -- m02 interface to HBM is used for the corner turn between the filterbanks and the beamformer. 
        M03_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec. 
        M03_AXI_ID_WIDTH   => 1,
        M04_AXI_ADDR_WIDTH => 64,  -- m02 interface to HBM is used for the corner turn between the filterbanks and the beamformer. 
        M04_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec. 
        M04_AXI_ID_WIDTH   => 1,
        M05_AXI_ADDR_WIDTH => 64,  -- m02 interface to HBM is used for the corner turn between the filterbanks and the beamformer. 
        M05_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec. 
        M05_AXI_ID_WIDTH   => 1,

	    M06_AXI_ADDR_WIDTH => 64,  -- m01 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
        M06_AXI_DATA_WIDTH => 512,
        M06_AXI_ID_WIDTH   => 1
--        M07_AXI_ADDR_WIDTH => 64,  -- m02 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M07_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M07_AXI_ID_WIDTH   => 1,
--        M08_AXI_ADDR_WIDTH => 64,  -- m03 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M08_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M08_AXI_ID_WIDTH   => 1,
--        M09_AXI_ADDR_WIDTH => 64,  -- m04 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M09_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M09_AXI_ID_WIDTH   => 1,
--        M10_AXI_ADDR_WIDTH => 64,  -- m05 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M10_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M10_AXI_ID_WIDTH   => 1,

--        M11_AXI_ADDR_WIDTH => 64,  -- m01 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M11_AXI_DATA_WIDTH => 512,
--        M11_AXI_ID_WIDTH   => 1,
--        M12_AXI_ADDR_WIDTH => 64,  -- m02 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M12_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M12_AXI_ID_WIDTH   => 1,
--        M13_AXI_ADDR_WIDTH => 64,  -- m03 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M13_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M13_AXI_ID_WIDTH   => 1,
--        M14_AXI_ADDR_WIDTH => 64,  -- m04 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M14_AXI_DATA_WIDTH => 256, -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
--        M14_AXI_ID_WIDTH   => 1,
--        M15_AXI_ADDR_WIDTH => 64,  -- m05 interface to HBM is used for the corner turn between the filterbanks and the beamformer.
--        M15_AXI_DATA_WIDTH => 256  -- 256 bits @ 300MHz is good for 76 Gb/sec. Corner turn output is only 34Gb/sec.
    ) port map (
        ap_clk   => ap_clk, --  in std_logic;
        ap_rst_n => ap_rst_n, -- in std_logic;
        
        -----------------------------------------------------------------------
        -- Ports used for simulation only.
        --
        -- Received data from 100GE
        i_axis_tdata                => rx_axis_tdata, -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep                => rx_axis_tkeep,  -- one bit per byte in i_axi_tdata
        i_axis_tlast                => rx_axis_tlast,
        i_axis_tuser                => rx_axis_tuser, -- Timestamp for the packet.
        i_axis_tvalid               => rx_axis_tvalid,
        -- Data to be transmitted on 100GE
        o_axis_tdata                => tx_axis_tdata,  -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        o_axis_tkeep                => tx_axis_tkeep,  -- one bit per byte in i_axi_tdata
        o_axis_tlast                => tx_axis_tlast,  -- out std_logic;                      
        o_axis_tuser                => tx_axis_tuser,  -- out std_logic;  
        o_axis_tvalid               => tx_axis_tvalid, -- out std_logic;
        i_axis_tready               => eth100G_rstn,  -- in std_logic;

        i_eth100g_clk               => eth100g_clk,     -- in std_logic;
        i_eth100g_locked            => eth100G_rstn, 

        -- Other signals to/from the timeslave 
        i_PTP_time_ARGs_clk         => (others => '0'),
        o_eth100_reset_final        => open,
        o_fec_enable_322m           => open,
        
        i_eth100G_rx_total_packets  => (others => '0'),
        i_eth100G_rx_bad_fcs        => (others => '0'),
        i_eth100G_rx_bad_code       => (others => '0'),
        i_eth100G_tx_total_packets  => (others => '0'),
        
        -- registers for the CMAC in Timeslave BD 
        o_cmac_mc_lite_mosi         => open,
        i_cmac_mc_lite_miso         => cmac_mc_lite_miso,
        -- registers in the timeslave core
        o_timeslave_mc_full_mosi    => open,
        i_timeslave_mc_full_miso    => timeslave_mc_full_miso, 
        
        
             
        -- reset of the valid memory is in progress.
        o_validMemRstActive => validMemRstActive, -- out std_logic;  
        --  Note: A minimum subset of AXI4 memory mapped signals are declared.  AXI
        --  signals omitted from these interfaces are automatically inferred with the
        -- optimal values for Xilinx SDx systems.  This allows Xilinx AXI4 Interconnects
        -- within the system to be optimized by removing logic for AXI4 protocol
        -- features that are not necessary. When adapting AXI4 masters within the RTL
        -- kernel that have signals not declared below, it is suitable to add the
        -- signals to the declarations below to connect them to the AXI4 Master.
        --
        -- List of omitted signals - effect
        -- -------------------------------
        --  ID - Transaction ID are used for multithreading and out of order transactions.  This increases complexity. This saves logic and increases Fmax in the system when ommited.
        -- SIZE - Default value is log2(data width in bytes). Needed for subsize bursts. This saves logic and increases Fmax in the system when ommited.
        -- BURST - Default value (0b01) is incremental.  Wrap and fixed bursts are not recommended. This saves logic and increases Fmax in the system when ommited.
        -- LOCK - Not supported in AXI4
        -- CACHE - Default value (0b0011) allows modifiable transactions. No benefit to changing this.
        -- PROT - Has no effect in SDx systems.
        -- QOS - Has no effect in SDx systems.
        -- REGION - Has no effect in SDx systems.
        -- USER - Has no effect in SDx systems.
        --  RESP - Not useful in most SDx systems.
        --------------------------------------------------------------------------------------
        --  AXI4-Lite slave interface
        s_axi_control_awvalid => mc_lite_mosi.awvalid, --  in std_logic;
        s_axi_control_awready => mc_lite_miso.awready, --  out std_logic;
        s_axi_control_awaddr  => mc_lite_mosi.awaddr(7 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_ADDR_WIDTH-1 downto 0);
        s_axi_control_wvalid  => mc_lite_mosi.wvalid, -- in std_logic;
        s_axi_control_wready  => mc_lite_miso.wready, -- out std_logic;
        s_axi_control_wdata   => mc_lite_mosi.wdata(31 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_DATA_WIDTH-1 downto 0);
        s_axi_control_wstrb   => mc_lite_mosi.wstrb(3 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_DATA_WIDTH/8-1 downto 0);
        s_axi_control_arvalid => mc_lite_mosi.arvalid, -- in std_logic;
        s_axi_control_arready => mc_lite_miso.arready, -- out std_logic;
        s_axi_control_araddr  => mc_lite_mosi.araddr(7 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_ADDR_WIDTH-1 downto 0);
        s_axi_control_rvalid  => mc_lite_miso.rvalid,  -- out std_logic;
        s_axi_control_rready  => mc_lite_mosi.rready, -- in std_logic;
        s_axi_control_rdata   => mc_lite_miso.rdata(31 downto 0), -- out std_logic_vector(C_S_AXI_CONTROL_DATA_WIDTH-1 downto 0);
        s_axi_control_rresp   => mc_lite_miso.rresp(1 downto 0), -- out std_logic_vector(1 downto 0);
        s_axi_control_bvalid  => mc_lite_miso.bvalid, -- out std_logic;
        s_axi_control_bready  => mc_lite_mosi.bready, -- in std_logic;
        s_axi_control_bresp   => mc_lite_miso.bresp(1 downto 0), -- out std_logic_vector(1 downto 0);
  
        -- AXI4 master interface for accessing registers : m00_axi
        m00_axi_awvalid => m00_awvalid, -- out std_logic;
        m00_axi_awready => m00_awready, -- in std_logic;
        m00_axi_awaddr  => m00_awaddr,  -- out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        m00_axi_awid    => open, --s_axi_awid,    -- out std_logic_vector(C_M_AXI_ID_WIDTH - 1 downto 0);
        m00_axi_awlen   => m00_awlen,   -- out std_logic_vector(7 downto 0);
        m00_axi_awsize  => m00_awsize,  -- out std_logic_vector(2 downto 0);
        m00_axi_awburst => m00_awburst, -- out std_logic_vector(1 downto 0);
        m00_axi_awlock  => open, -- s_axi_awlock,  -- out std_logic_vector(1 downto 0);
        m00_axi_awcache => m00_awcache, -- out std_logic_vector(3 downto 0);
        m00_axi_awprot  => m00_awprot,  -- out std_logic_vector(2 downto 0);
        m00_axi_awqos   => open, -- s_axi_awqos,   -- out std_logic_vector(3 downto 0);
        m00_axi_awregion => open, -- s_axi_awregion, -- out std_logic_vector(3 downto 0);
        m00_axi_wvalid   => m00_wvalid,   -- out std_logic;
        m00_axi_wready   => m00_wready,   -- in std_logic;
        m00_axi_wdata    => m00_wdata,    -- out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        m00_axi_wstrb    => m00_wstrb,    -- out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        m00_axi_wlast    => m00_wlast,    -- out std_logic;
        m00_axi_bvalid   => m00_bvalid,   -- in std_logic;
        m00_axi_bready   => m00_bready,   -- out std_logic;
        m00_axi_bresp    => m00_bresp,    -- in std_logic_vector(1 downto 0);
        m00_axi_bid      => "0", -- s_axi_bid,      -- in std_logic_vector(C_M_AXI_ID_WIDTH - 1 downto 0);
        m00_axi_arvalid  => m00_arvalid,  -- out std_logic;
        m00_axi_arready  => m00_arready,  -- in std_logic;
        m00_axi_araddr   => m00_araddr,   -- out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        m00_axi_arid     => open, -- s_axi_arid,     -- out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        m00_axi_arlen    => m00_arlen,    -- out std_logic_vector(7 downto 0);
        m00_axi_arsize   => m00_arsize,   -- out std_logic_vector(2 downto 0);
        m00_axi_arburst  => m00_arburst,  -- out std_logic_vector(1 downto 0);
        m00_axi_arlock   => open, -- s_axi_arlock,   -- out std_logic_vector(1 downto 0);
        m00_axi_arcache  => m00_arcache,  -- out std_logic_vector(3 downto 0);
        m00_axi_arprot   => m00_arprot,   -- out std_logic_Vector(2 downto 0);
        m00_axi_arqos    => open, -- s_axi_arqos,    -- out std_logic_vector(3 downto 0);
        m00_axi_arregion => open, -- s_axi_arregion, -- out std_logic_vector(3 downto 0);
        m00_axi_rvalid   => m00_rvalid,   -- in std_logic;
        m00_axi_rready   => m00_rready,   -- out std_logic;
        m00_axi_rdata    => m00_rdata,    -- in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        m00_axi_rlast    => m00_rlast,    -- in std_logic;
        m00_axi_rid      => "0", -- s_axi_rid,      -- in std_logic_vector(C_M_AXI_ID_WIDTH - 1 downto 0);
        m00_axi_rresp    => m00_rresp,    -- in std_logic_vector(1 downto 0);


        ---------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        -- AXI4 interfaces for accessing HBM
        -- 2 per pipeline.
        -- 1 = 2 Gbytes, CT1
        --     
        -- 2 = 1 Gbytes, CT2
        --     

        HBM_axi_awvalid  =>  HBM_axi_awvalid,
        HBM_axi_awready  =>  HBM_axi_awready,
        HBM_axi_awaddr   =>  HBM_axi_awaddr,
        HBM_axi_awid     =>  HBM_axi_awid,
        HBM_axi_awlen    =>  HBM_axi_awlen,
        HBM_axi_awsize   =>  HBM_axi_awsize,
        HBM_axi_awburst  =>  HBM_axi_awburst,
        HBM_axi_awlock   =>  HBM_axi_awlock,
        HBM_axi_awcache  =>  HBM_axi_awcache,
        HBM_axi_awprot   =>  HBM_axi_awprot,
        HBM_axi_awqos    =>  HBM_axi_awqos,
        HBM_axi_awregion =>  HBM_axi_awregion,

        HBM_axi_wvalid   =>  HBM_axi_wvalid,
        HBM_axi_wready   =>  HBM_axi_wready,
        HBM_axi_wdata    =>  HBM_axi_wdata,
        HBM_axi_wstrb    =>  HBM_axi_wstrb,
        HBM_axi_wlast    =>  HBM_axi_wlast,
        HBM_axi_bvalid   =>  HBM_axi_bvalid,
        HBM_axi_bready   =>  HBM_axi_bready,
        HBM_axi_bresp    =>  HBM_axi_bresp,
        HBM_axi_bid      =>  HBM_axi_bid,
        HBM_axi_arvalid  =>  HBM_axi_arvalid,
        HBM_axi_arready  =>  HBM_axi_arready,
        HBM_axi_araddr   =>  HBM_axi_araddr,
        HBM_axi_arid     =>  HBM_axi_arid,
        HBM_axi_arlen    =>  HBM_axi_arlen,
        HBM_axi_arsize   =>  HBM_axi_arsize,
        HBM_axi_arburst  =>  HBM_axi_arburst,
        HBM_axi_arlock   =>  HBM_axi_arlock,
        HBM_axi_arcache  =>  HBM_axi_arcache,
        HBM_axi_arprot   =>  HBM_axi_arprot,
        HBM_axi_arqos    =>  HBM_axi_arqos,
        HBM_axi_arregion =>  HBM_axi_arregion,
        HBM_axi_rvalid   =>  HBM_axi_rvalid,
        HBM_axi_rready   =>  HBM_axi_rready,
        HBM_axi_rdata    =>  HBM_axi_rdata,
        HBM_axi_rlast    =>  HBM_axi_rlast,
        HBM_axi_rid      =>  HBM_axi_rid,
        HBM_axi_rresp    =>  HBM_axi_rresp,
        -- HBM debug
        --i_input_HBM_reset   => input_HBM_reset,
        --
        -- GT pins
        -- clk_gt_freerun is a 50MHz free running clock, according to the GT kernel Example Design user guide.
        -- But it looks like it is configured to be 100MHz in the example designs for all parts except the U280. 
        clk_freerun    => clk100, --  in std_logic; 
        gt_rxp_in      => "0000", --  in std_logic_vector(3 downto 0);
        gt_rxn_in      => "1111", -- in std_logic_vector(3 downto 0);
        gt_txp_out     => open, -- out std_logic_vector(3 downto 0);
        gt_txn_out     => open, -- out std_logic_vector(3 downto 0);
        gt_refclk_p    => '0', -- in std_logic;
        gt_refclk_n    => '1'  -- in std_logic
    );


    -- Emulate HBM
    -- 1 Gbyte of memory for the first corner turn.
    -- Translate from 1Gbyte address space to a 1 MByte address space to make the amount of memory manageable for simulation.
    -- 4 buffers of 256 MBytes each                       = 2 bits, (29:28)   => keep all 4 buffers       = 2 bits,  (19:18)
    -- Each buffer is 1024 virtual channels               = 10 bits, (27:18)  => Space for 8 channels     = 3 bits,  (17:15)
    -- Each virtual channel has space for 32 LFAA packets = 5 bits, (17:13)   => space for 4 LFAA packets = 2 bits,  (14:13)
    -- Each LFAA packet is 8192 bytes                     = 13 bits, (12:0)   => keep the same            = 13 bits, (12:0)

    HBM1G_1 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 30, -- : integer := 32;   -- Byte address width. This also defines the amount of data. Use the correct width for the HBM memory block, e.g. 28 bits for 256 MBytes.
        AXI_ID_WIDTH => 1, -- integer := 1;
        AXI_DATA_WIDTH => 512, -- integer := 256;  -- Must be a multiple of 32 bits.
        READ_QUEUE_SIZE => 16, --  integer := 16;
        MIN_LAG => 60,  -- integer := 80   
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 95, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 80 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk => ap_clk,
        i_rst_n => ap_rst_n,
        axi_awaddr => m01_awaddr(29 downto 0),
        axi_awid   =>  m01_awid, -- in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen => m01_awlen,
        axi_awsize => m01_awsize,
        axi_awburst => m01_awburst,
        axi_awlock => m01_awlock,
        axi_awcache => m01_awcache,
        axi_awprot => m01_awprot,
        axi_awqos  => m01_awqos, -- in(3:0)
        axi_awregion => m01_awregion, -- in(3:0)
        axi_awvalid => m01_awvalid,
        axi_awready => m01_awready,
        axi_wdata => m01_wdata,
        axi_wstrb => m01_wstrb,
        axi_wlast => m01_wlast,
        axi_wvalid => m01_wvalid,
        axi_wready => m01_wready,
        axi_bresp => m01_bresp,
        axi_bvalid => m01_bvalid,
        axi_bready => m01_bready,
        axi_bid => m01_bid, -- out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_araddr => m01_araddr(29 downto 0),
        axi_arlen => m01_arlen,
        axi_arsize => m01_arsize,
        axi_arburst => m01_arburst,
        axi_arlock => m01_arlock,
        axi_arcache => m01_arcache,
        axi_arprot => m01_arprot,
        axi_arvalid => m01_arvalid,
        axi_arready => m01_arready,
        axi_arqos => m01_arqos,
        axi_arid  => m01_arid,
        axi_arregion => m01_arregion,
        axi_rdata => m01_rdata,
        axi_rresp => m01_rresp,
        axi_rlast => m01_rlast,
        axi_rvalid => m01_rvalid,
        axi_rready => m01_rready,
        -- control dump to disk.
        i_write_to_disk         => '0', --: in std_logic;
        i_write_to_disk_addr    => 0, --: in integer; -- address to start the memory dump at.
        i_write_to_disk_size    => 0, --: in integer; -- size in bytes
        i_fname                 => "", --: in string;
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => '0',
        i_init_fname            => "" --: in string
    );


    HBM1G_2 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 30, -- : integer := 32;   -- Byte address width. This also defines the amount of data. Use the correct width for the HBM memory block, e.g. 28 bits for 256 MBytes.
        AXI_ID_WIDTH => 1, -- integer := 1;
        AXI_DATA_WIDTH => 512, -- integer := 256;  -- Must be a multiple of 32 bits.
        READ_QUEUE_SIZE => 16, --  integer := 16;
        MIN_LAG => 60,  -- integer := 80   
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 95, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 80 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk => ap_clk,
        i_rst_n => ap_rst_n,
        axi_awaddr  => m06_awaddr(29 downto 0),
        axi_awid    => m06_awid, -- in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen   => m06_awlen,
        axi_awsize  => m06_awsize,
        axi_awburst => m06_awburst,
        axi_awlock  => m06_awlock,
        axi_awcache => m06_awcache,
        axi_awprot  => m06_awprot,
        axi_awqos   => m06_awqos, -- in(3:0)
        axi_awregion => m06_awregion, -- in(3:0)
        axi_awvalid  => m06_awvalid,
        axi_awready  => m06_awready,
        axi_wdata    => m06_wdata,
        axi_wstrb    => m06_wstrb,
        axi_wlast    => m06_wlast,
        axi_wvalid   => m06_wvalid,
        axi_wready   => m06_wready,
        axi_bresp    => m06_bresp,
        axi_bvalid   => m06_bvalid,
        axi_bready   => m06_bready,
        axi_bid      => m06_bid, -- out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_araddr   => m06_araddr(29 downto 0),
        axi_arlen    => m06_arlen,
        axi_arsize   => m06_arsize,
        axi_arburst  => m06_arburst,
        axi_arlock   => m06_arlock,
        axi_arcache  => m06_arcache,
        axi_arprot   => m06_arprot,
        axi_arvalid  => m06_arvalid,
        axi_arready  => m06_arready,
        axi_arqos    => m06_arqos,
        axi_arid     => m06_arid,
        axi_arregion => m06_arregion,
        axi_rdata    => m06_rdata,
        axi_rresp    => m06_rresp,
        axi_rlast    => m06_rlast,
        axi_rvalid   => m06_rvalid,
        axi_rready   => m06_rready,
                -- control dump to disk.
        i_write_to_disk         => '0', --: in std_logic;
        i_write_to_disk_addr    => 0, --: in integer; -- address to start the memory dump at.
        i_write_to_disk_size    => 0, --: in integer; -- size in bytes
        i_fname                 => "", --: in string;
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => '0',
        i_init_fname            => "" --: in string
    );
 

    ------------------------------------------------------------------------------------------------------------
    -- 1Gbyte of memory for the second corner turn
    -- Need to check this address mapping makes sense after second corner turn is functioning in the firmware.
    -- This is in 4 different blocks, with 256 Mbyte in each
    -- Addresses outside of the first MByte in each 256MBytes will cause a simulation error.

    ram256_1 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 28,
        AXI_ID_WIDTH => 1,
        AXI_DATA_WIDTH => 256,
        READ_QUEUE_SIZE => 16,
        MIN_LAG => 80,
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 11342, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 90, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 70 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk => ap_clk,     -- in std_logic;
        i_rst_n => ap_rst_n, -- in std_logic;
        --
        axi_awvalid => m02_awvalid, --: in std_logic;
        axi_awready => m02_awready, --: out std_logic;
        axi_awaddr  => m02_awaddr(27 downto 0), --: in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_awid    => m02_awid, --: in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen   => m02_awlen, --: in std_logic_vector(7 downto 0);
        axi_awsize  => m02_awsize, --: in std_logic_vector(2 downto 0);
        axi_awburst => m02_awburst, --: in std_logic_vector(1 downto 0);
        axi_awlock  => m02_awlock, --: in std_logic_vector(1 downto 0);
        axi_awcache => m02_awcache, --: in std_logic_vector(3 downto 0);
        axi_awprot  => m02_awprot, --: in std_logic_vector(2 downto 0);
        axi_awqos   => m02_awqos, --: in std_logic_vector(3 downto 0);
        axi_awregion=> m02_awregion, -- : in std_logic_vector(3 downto 0);
        axi_wvalid  => m02_wvalid, -- : in std_logic;
        axi_wready  => m02_wready, -- : out std_logic;
        axi_wdata   => m02_wdata, -- : in std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_wstrb   => m02_wstrb, -- : in std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
        axi_wlast   => m02_wlast, -- : in std_logic;
        axi_bvalid  => m02_bvalid, -- : out std_logic;
        axi_bready  => m02_bready, -- : in std_logic;
        axi_bresp   => m02_bresp, -- : out std_logic_vector(1 downto 0);
        axi_bid     => m02_bid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_arvalid => m02_arvalid, -- : in std_logic;
        axi_arready => m02_arready, -- : out std_logic;
        axi_araddr  => m02_araddr(27 downto 0), -- : in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_arid    => m02_arid, -- : in std_logic_vector(AXI_ID_WIDTH-1 downto 0);
        axi_arlen   => m02_arlen, -- : in std_logic_vector(7 downto 0);
        axi_arsize  => m02_arsize, -- : in std_logic_vector(2 downto 0);
        axi_arburst => m02_arburst, -- : in std_logic_vector(1 downto 0);
        axi_arlock  => m02_arlock, -- : in std_logic_vector(1 downto 0);
        axi_arcache => m02_arcache, -- : in std_logic_vector(3 downto 0);
        axi_arprot  => m02_arprot, -- : in std_logic_Vector(2 downto 0);
        axi_arqos   => m02_arqos, -- : in std_logic_vector(3 downto 0);
        axi_arregion=> m02_arregion, -- : in std_logic_vector(3 downto 0);
        axi_rvalid  => m02_rvalid, -- : out std_logic;
        axi_rready  => m02_rready, -- : in std_logic;
        axi_rdata   => m02_rdata, -- : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_rlast   => m02_rlast, -- : out std_logic;
        axi_rid     => m02_rid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_rresp   => m02_rresp,
                -- control dump to disk.
        i_write_to_disk         => '0', --: in std_logic;
        i_write_to_disk_addr    => 0, --: in integer; -- address to start the memory dump at.
        i_write_to_disk_size    => 0, --: in integer; -- size in bytes
        i_fname                 => "", --: in string;
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => '0',
        i_init_fname            => "" --: in string
    );

    ram256_2 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 28,
        AXI_ID_WIDTH => 1,
        AXI_DATA_WIDTH => 256,
        READ_QUEUE_SIZE => 16,
        MIN_LAG => 80,
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 34212, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 95, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 90 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    
    ) Port map (
        i_clk => ap_clk,     -- in std_logic;
        i_rst_n => ap_rst_n, -- in std_logic;
        --
        axi_awvalid => m03_awvalid, --: in std_logic;
        axi_awready => m03_awready, --: out std_logic;
        axi_awaddr  => m03_awaddr(27 downto 0), --: in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_awid    => m03_awid, --: in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen   => m03_awlen, --: in std_logic_vector(7 downto 0);
        axi_awsize  => m03_awsize, --: in std_logic_vector(2 downto 0);
        axi_awburst => m03_awburst, --: in std_logic_vector(1 downto 0);
        axi_awlock  => m03_awlock, --: in std_logic_vector(1 downto 0);
        axi_awcache => m03_awcache, --: in std_logic_vector(3 downto 0);
        axi_awprot  => m03_awprot, --: in std_logic_vector(2 downto 0);
        axi_awqos   => m03_awqos, --: in std_logic_vector(3 downto 0);
        axi_awregion=> m03_awregion, -- : in std_logic_vector(3 downto 0);
        axi_wvalid  => m03_wvalid, -- : in std_logic;
        axi_wready  => m03_wready, -- : out std_logic;
        axi_wdata   => m03_wdata, -- : in std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_wstrb   => m03_wstrb, -- : in std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
        axi_wlast   => m03_wlast, -- : in std_logic;
        axi_bvalid  => m03_bvalid, -- : out std_logic;
        axi_bready  => m03_bready, -- : in std_logic;
        axi_bresp   => m03_bresp, -- : out std_logic_vector(1 downto 0);
        axi_bid     => m03_bid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_arvalid => m03_arvalid, -- : in std_logic;
        axi_arready => m03_arready, -- : out std_logic;
        axi_araddr  => m03_araddr(27 downto 0), -- : in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_arid    => m03_arid, -- : in std_logic_vector(AXI_ID_WIDTH-1 downto 0);
        axi_arlen   => m03_arlen, -- : in std_logic_vector(7 downto 0);
        axi_arsize  => m03_arsize, -- : in std_logic_vector(2 downto 0);
        axi_arburst => m03_arburst, -- : in std_logic_vector(1 downto 0);
        axi_arlock  => m03_arlock, -- : in std_logic_vector(1 downto 0);
        axi_arcache => m03_arcache, -- : in std_logic_vector(3 downto 0);
        axi_arprot  => m03_arprot, -- : in std_logic_Vector(2 downto 0);
        axi_arqos   => m03_arqos, -- : in std_logic_vector(3 downto 0);
        axi_arregion=> m03_arregion, -- : in std_logic_vector(3 downto 0);
        axi_rvalid  => m03_rvalid, -- : out std_logic;
        axi_rready  => m03_rready, -- : in std_logic;
        axi_rdata   => m03_rdata, -- : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_rlast   => m03_rlast, -- : out std_logic;
        axi_rid     => m03_rid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_rresp   => m03_rresp,
                -- control dump to disk.
        i_write_to_disk         => '0', --: in std_logic;
        i_write_to_disk_addr    => 0, --: in integer; -- address to start the memory dump at.
        i_write_to_disk_size    => 0, --: in integer; -- size in bytes
        i_fname                 => "", --: in string;
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => '0',
        i_init_fname            => "" --: in string
    );
    
    ram256_3 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 28,
        AXI_ID_WIDTH => 1,
        AXI_DATA_WIDTH => 256,
        READ_QUEUE_SIZE => 16,
        MIN_LAG => 80,
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 3463221, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 60, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 60 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk => ap_clk,     -- in std_logic;
        i_rst_n => ap_rst_n, -- in std_logic;
        --
        axi_awvalid => m04_awvalid, --: in std_logic;
        axi_awready => m04_awready, --: out std_logic;
        axi_awaddr  => m04_awaddr(27 downto 0), --: in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_awid    => m04_awid, --: in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen   => m04_awlen, --: in std_logic_vector(7 downto 0);
        axi_awsize  => m04_awsize, --: in std_logic_vector(2 downto 0);
        axi_awburst => m04_awburst, --: in std_logic_vector(1 downto 0);
        axi_awlock  => m04_awlock, --: in std_logic_vector(1 downto 0);
        axi_awcache => m04_awcache, --: in std_logic_vector(3 downto 0);
        axi_awprot  => m04_awprot, --: in std_logic_vector(2 downto 0);
        axi_awqos   => m04_awqos, --: in std_logic_vector(3 downto 0);
        axi_awregion=> m04_awregion, -- : in std_logic_vector(3 downto 0);
        axi_wvalid  => m04_wvalid, -- : in std_logic;
        axi_wready  => m04_wready, -- : out std_logic;
        axi_wdata   => m04_wdata, -- : in std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_wstrb   => m04_wstrb, -- : in std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
        axi_wlast   => m04_wlast, -- : in std_logic;
        axi_bvalid  => m04_bvalid, -- : out std_logic;
        axi_bready  => m04_bready, -- : in std_logic;
        axi_bresp   => m04_bresp, -- : out std_logic_vector(1 downto 0);
        axi_bid     => m04_bid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_arvalid => m04_arvalid, -- : in std_logic;
        axi_arready => m04_arready, -- : out std_logic;
        axi_araddr  => m04_araddr(27 downto 0), -- : in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_arid    => m04_arid, -- : in std_logic_vector(AXI_ID_WIDTH-1 downto 0);
        axi_arlen   => m04_arlen, -- : in std_logic_vector(7 downto 0);
        axi_arsize  => m04_arsize, -- : in std_logic_vector(2 downto 0);
        axi_arburst => m04_arburst, -- : in std_logic_vector(1 downto 0);
        axi_arlock  => m04_arlock, -- : in std_logic_vector(1 downto 0);
        axi_arcache => m04_arcache, -- : in std_logic_vector(3 downto 0);
        axi_arprot  => m04_arprot, -- : in std_logic_Vector(2 downto 0);
        axi_arqos   => m04_arqos, -- : in std_logic_vector(3 downto 0);
        axi_arregion=> m04_arregion, -- : in std_logic_vector(3 downto 0);
        axi_rvalid  => m04_rvalid, -- : out std_logic;
        axi_rready  => m04_rready, -- : in std_logic;
        axi_rdata   => m04_rdata, -- : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_rlast   => m04_rlast, -- : out std_logic;
        axi_rid     => m04_rid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_rresp   => m04_rresp,
                -- control dump to disk.
        i_write_to_disk         => '0', --: in std_logic;
        i_write_to_disk_addr    => 0, --: in integer; -- address to start the memory dump at.
        i_write_to_disk_size    => 0, --: in integer; -- size in bytes
        i_fname                 => "", --: in string;
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => '0',
        i_init_fname            => "" --: in string
    );
    
    ram256_4 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 28,  -- ! Warning - only the low 20 bits are used.
        AXI_ID_WIDTH => 1,
        AXI_DATA_WIDTH => 256,
        READ_QUEUE_SIZE => 16,
        MIN_LAG => 80,
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 9872312, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 69, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 94 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk => ap_clk,     -- in std_logic;
        i_rst_n => ap_rst_n, -- in std_logic;
        --
        axi_awvalid => m05_awvalid, --: in std_logic;
        axi_awready => m05_awready, --: out std_logic;
        axi_awaddr  => m05_awaddr(27 downto 0), --: in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_awid    => m05_awid, --: in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen   => m05_awlen, --: in std_logic_vector(7 downto 0);
        axi_awsize  => m05_awsize, --: in std_logic_vector(2 downto 0);
        axi_awburst => m05_awburst, --: in std_logic_vector(1 downto 0);
        axi_awlock  => m05_awlock, --: in std_logic_vector(1 downto 0);
        axi_awcache => m05_awcache, --: in std_logic_vector(3 downto 0);
        axi_awprot  => m05_awprot, --: in std_logic_vector(2 downto 0);
        axi_awqos   => m05_awqos, --: in std_logic_vector(3 downto 0);
        axi_awregion=> m05_awregion, -- : in std_logic_vector(3 downto 0);
        axi_wvalid  => m05_wvalid, -- : in std_logic;
        axi_wready  => m05_wready, -- : out std_logic;
        axi_wdata   => m05_wdata, -- : in std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_wstrb   => m05_wstrb, -- : in std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
        axi_wlast   => m05_wlast, -- : in std_logic;
        axi_bvalid  => m05_bvalid, -- : out std_logic;
        axi_bready  => m05_bready, -- : in std_logic;
        axi_bresp   => m05_bresp, -- : out std_logic_vector(1 downto 0);
        axi_bid     => m05_bid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_arvalid => m05_arvalid, -- : in std_logic;
        axi_arready => m05_arready, -- : out std_logic;
        axi_araddr  => m05_araddr(27 downto 0), -- : in std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
        axi_arid    => m05_arid, -- : in std_logic_vector(AXI_ID_WIDTH-1 downto 0);
        axi_arlen   => m05_arlen, -- : in std_logic_vector(7 downto 0);
        axi_arsize  => m05_arsize, -- : in std_logic_vector(2 downto 0);
        axi_arburst => m05_arburst, -- : in std_logic_vector(1 downto 0);
        axi_arlock  => m05_arlock, -- : in std_logic_vector(1 downto 0);
        axi_arcache => m05_arcache, -- : in std_logic_vector(3 downto 0);
        axi_arprot  => m05_arprot, -- : in std_logic_Vector(2 downto 0);
        axi_arqos   => m05_arqos, -- : in std_logic_vector(3 downto 0);
        axi_arregion=> m05_arregion, -- : in std_logic_vector(3 downto 0);
        axi_rvalid  => m05_rvalid, -- : out std_logic;
        axi_rready  => m05_rready, -- : in std_logic;
        axi_rdata   => m05_rdata, -- : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        axi_rlast   => m05_rlast, -- : out std_logic;
        axi_rid     => m05_rid, -- : out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_rresp   => m05_rresp,
                -- control dump to disk.
        i_write_to_disk         => '0', --: in std_logic;
        i_write_to_disk_addr    => 0, --: in integer; -- address to start the memory dump at.
        i_write_to_disk_size    => 0, --: in integer; -- size in bytes
        i_fname                 => "", --: in string;
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => '0',
        i_init_fname            => "" --: in string
    );    
   


end Behavioral;
