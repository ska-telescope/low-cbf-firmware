#!/bin/bash

# Use the matlab model repo - sub module = low-cbf-model
#
# Generate the following for each run: 
#  - the ARGs register files
#  - S_AXI LFAA stimulus packets
# 
# copy the above to the build dir for

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
#shellcheck source=util.sh
#source "$MY_DIR/util.sh"

if [[ -z "$EXTRA_MATLAB_MODEL_DIR" ]]; then
    2>&1 echo "Error: EXTRA_MATLAB_MODEL_DIR variable is required for matlab job"
    exit 2
fi

cd "$(envsubst <<< "${EXTRA_MATLAB_MODEL_DIR}")" || exit 2

# PST currently validated against run13 in the model
# create the model.
matlab -batch "create_config('run13',0,3)"

# generate the short LFAA packet burst
matlab -batch "write_tb_data('run13',1,144,256)"

# copy short file to build for artifacting and update name
echo "copy short file to build for artifacting"
cp run13/tb/LFAA100GE_s_axi_tb_data.txt ../../build/short_s_axi_tb_data.txt

# generate the Full LFAA packet burst
matlab -batch "write_tb_data('run13',1,512,256)"

# copy full file to build for artifacting and update name
echo "copy full file to build for artifacting"
cp run13/tb/LFAA100GE_s_axi_tb_data.txt ../../build/full__s_axi_tb_data.txt

# create ARGs registry file, this requires the CCFG from ARGs, the HWData_pst.txt in the tb folder of the run.
# store the output in build dir as registerUpload.txt
# this is using a python script in the tools folder of the repo.
echo "Generating registerUpload.txt for the testbench from HWData_pst.txt"
python3 ../../tools/args/gen_tb_config.py -c ../../build/ARGS/pst/pst.ccfg -i run13/tb/HWData_pst.txt -o ../../build/registerUpload.txt

echo "ARGs register set now in build"
echo "------------------------ SIM GENERATION COMPLETE ------------------------"