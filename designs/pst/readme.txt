# commands to generate a PST project.

export SVN=/home/hum089/projects/perentie/ska-low-cbf-fw-pst/
source $SVN/tools/bin/setup_radiohdl.sh

python3 $SVN/tools/radiohdl/base/vivado_config.py -l pst -a

export PERSONALITY=pst
export TARGET_ALVEO=u55
export VITIS_VERSION=2022.2
export COMMON_PATH=~/projects/perentie/ska-low-cbf-fw-pst/common

vivado -mode batch -source create_project.tcl

