set time_raw [clock seconds];
set date_string [clock format $time_raw -format "%y%m%d_%H%M%S"]

set proj_dir "$env(RADIOHDL)/build/$env(PERSONALITY)/ct1_tb_build_$date_string"
set ARGS_PATH "$env(RADIOHDL)/build/ARGS/pst"
set BOARD_PATH "$env(RADIOHDL)/designs/libraries/board"
set DESIGN_PATH "$env(RADIOHDL)/designs/pst"
set RLIBRARIES_PATH "$env(RADIOHDL)/libraries"
set COMMON_PATH "$env(RADIOHDL)/common/libraries"
set BUILD_PATH "$env(RADIOHDL)/build"
set DEVICE "xcu55c-fsvh2892-2L-e"
set BOARD "xilinx.com:au55c:part0:1.0"

puts "RADIOHDL directory:"
puts $env(RADIOHDL)

#puts "Timeslave IP in submodule"
# RADIOHDL is ENV_VAR for current project REPO. 
#set timeslave_repo "$env(RADIOHDL)/pub-timeslave/hw/cores"

# Create the new build directory
puts "Creating build_directory $proj_dir"
file mkdir $proj_dir

# This script sets the project variables
puts "Creating new project: pst"
cd $proj_dir

set workingDir [pwd]
puts "Working directory:"
puts $workingDir

# WARNING - proj_dir must be relative to workingDir.
# But cannot be empty because args generates tcl with the directory specified as "$proj_dir/"
set proj_dir "../ct1_tb_build_$date_string"

create_project $env(PERSONALITY) -part $DEVICE -force
set_property board_part $BOARD [current_project]
set_property target_language VHDL [current_project]
set_property target_simulator XSim [current_project]

############################################################
# Board specific files
############################################################


############################################################
# Design specific files
############################################################


add_files -fileset sim_1 [glob \
$DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd \
]
#set_property file_type {VHDL 2008} [get_files  $DESIGN_PATH/src/vhdl/HBM_axi_tbModel.vhd]
set_property file_type {VHDL 2008} [get_files  /home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/vhdl/HBM_axi_tbModel.vhd]
set_property library pst_lib [get_files  /home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/vhdl/HBM_axi_tbModel.vhd]



#add_files -fileset constrs_1 [ glob $DESIGN_PATH/vivado/vcu128_gemini_dsp.xdc ]

# vivado_xci_files: Importing IP to the project
# tcl scripts for ip generation

#source $DESIGN_PATH/src/ip/pst.tcl
############################################################
# AXI4

add_files -fileset sources_1 [glob \
$COMMON_PATH/base/axi4/src/vhdl/axi4_lite_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/axi4_full_pkg.vhd \
$COMMON_PATH/base/axi4/src/vhdl/mem_to_axi4_lite.vhd \
]
set_property library axi4_lib [get_files {\
*libraries/base/axi4/src/vhdl/axi4_lite_pkg.vhd \
*libraries/base/axi4/src/vhdl/axi4_full_pkg.vhd \
*libraries/base/axi4/src/vhdl/mem_to_axi4_lite.vhd \
}]

# Technology select package
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/technology/technology_pkg.vhd \
 $RLIBRARIES_PATH/technology/technology_select_pkg.vhd \
]
set_property library technology_lib [get_files {\
 *libraries/technology/technology_pkg.vhd \
 *libraries/technology/technology_select_pkg.vhd \
}]

add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/signalProcessing/DSP_top/src/vhdl/DSP_top_pkg.vhd \
]
set_property library DSP_top_lib [get_files  {\
 *libraries/signalProcessing/DSP_top/src/vhdl/DSP_top_pkg.vhd \
}]

#############################################################
# Common

add_files -fileset sources_1 [glob \
 $COMMON_PATH/base/common/src/vhdl/common_reg_r_w.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_ram_crw_crw.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_str_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_mem_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_field_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_lfsr_sequences_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_interface_layers_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_network_layers_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_network_total_header_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_components_pkg.vhd \
 $COMMON_PATH/base/common/src/vhdl/common_pipeline.vhd \
]
set_property library common_lib [get_files {\
 *libraries/base/common/src/vhdl/common_reg_r_w.vhd \
 *libraries/base/common/src/vhdl/common_pkg.vhd \
 *libraries/base/common/src/vhdl/common_ram_crw_crw.vhd \
 *libraries/base/common/src/vhdl/common_str_pkg.vhd \
 *libraries/base/common/src/vhdl/common_mem_pkg.vhd \
 *libraries/base/common/src/vhdl/common_field_pkg.vhd \
 *libraries/base/common/src/vhdl/common_lfsr_sequences_pkg.vhd \
 *libraries/base/common/src/vhdl/common_interface_layers_pkg.vhd \
 *libraries/base/common/src/vhdl/common_network_layers_pkg.vhd \
 *libraries/base/common/src/vhdl/common_network_total_header_pkg.vhd \
 *libraries/base/common/src/vhdl/common_components_pkg.vhd \
 *libraries/base/common/src/vhdl/common_pipeline.vhd \
}]

#############################################################
# tech memory
# (Used by ARGs)
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/technology/memory/tech_memory_component_pkg.vhd \
 $RLIBRARIES_PATH/technology/memory/tech_memory_ram_cr_cw.vhd \
 $RLIBRARIES_PATH/technology/memory/tech_memory_ram_crw_crw.vhd \
]
set_property library tech_memory_lib [get_files {\
 *libraries/technology/memory/tech_memory_component_pkg.vhd \
 *libraries/technology/memory/tech_memory_ram_cr_cw.vhd \
 *libraries/technology/memory/tech_memory_ram_crw_crw.vhd \
}]

#############################################################
# input Corner Turn (ct1)

add_files -fileset sources_1 [glob \
  $ARGS_PATH/pst_ct1/pst_ct1/pst_ct1_reg_pkg.vhd \
  $ARGS_PATH/pst_ct1/pst_ct1/pst_ct1_reg.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1_top.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1_readout_32bit.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1_valid.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1_readout.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_div96.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/poly_axi_bram_wrapper.vhd \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/poly_eval.vhd \
]

set_property library ct_lib [get_files {\
  *build/ARGS/pst/pst_ct1/pst_ct1/pst_ct1_reg_pkg.vhd \
  *build/ARGS/pst/pst_ct1/pst_ct1/pst_ct1_reg.vhd \
  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_top.vhd \
  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_readout_32bit.vhd \
  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_valid.vhd \
  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_readout.vhd \
  *libraries/signalProcessing/corner_turner/ct1/pst_div96.vhd \
  *libraries/signalProcessing/corner_turner/ct1/poly_axi_bram_wrapper.vhd \
  *libraries/signalProcessing/corner_turner/ct1/poly_eval.vhd \
 }]

add_files -fileset sim_1 [glob \
  $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1_tb.vhd \
]

#  *libraries/signalProcessing/corner_turner/ct1/pst_ct1_tb.vhd \ 

source $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1.tcl
source $RLIBRARIES_PATH/signalProcessing/corner_turner/ct1/pst_ct1_ct2_common.tcl


#############################################################
# Filterbanks
add_files -fileset sources_1 [glob \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/fb_DSP.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/PSTFFTwrapper.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/PSTFBmem.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/PSTFBTop.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/FB_top_PST.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/ShiftandRound_16bit.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/fineDelay.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/BRAM_512x192.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/BRAM_512x96.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/vhdl/BROMWrapper.vhd \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps1.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps2.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps3.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps4.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps5.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps6.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps7.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps8.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps9.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps10.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps11.mem \
 $RLIBRARIES_PATH/signalProcessing/filterbanks/src/coe/PSTFIRTaps12.mem \ 
]

set_property library filterbanks_lib [get_files {\
 *libraries/signalProcessing/filterbanks/src/vhdl/fb_DSP.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/PSTFFTwrapper.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/PSTFBmem.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/PSTFBTop.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/FB_top_PST.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/ShiftandRound_16bit.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/fineDelay.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/BRAM_512x192.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/BRAM_512x96.vhd \
 *libraries/signalProcessing/filterbanks/src/vhdl/BROMWrapper.vhd \
}] 

source $RLIBRARIES_PATH/signalProcessing/filterbanks/src/ip/dspAxB.tcl
source $RLIBRARIES_PATH/signalProcessing/filterbanks/src/ip/fineDelay.tcl
source $RLIBRARIES_PATH/signalProcessing/filterbanks/src/ip/PSTFB_FFT.tcl


##############################################################
# Set top
set_property top_lib xil_defaultlib [get_filesets sim_1]
# top level testbench
set_property top pst_ct1_tb [get_filesets sim_1]

