# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Standalone code to create configuration for the SKA low correlator corner turn 1 module
---------------------------------------------
Introduction:
Corner turn 1 (CT1) buffers packets and then plays them back in a known order.
Main functionality being tested here is the configuration and calculation of the delay polynomials.
This code reads in a yaml file and writes out configuration data for the yaml test case.
The configurationd data includes the polynomial coefficients.
----------------------------------------------
Configuration memory:
  The memory is organised into blocks of 80 bytes.
  Within that memory :
    words 0 to 9 : Config for virtual channel 0, buffer 0 (see below for specification of contents)
    words 10 to 19 : Config for virtual channel 1, buffer 0
    ...
    words 10230 to 10239 : Config for virtual channel 1023, first buffer
    words 10240 to 20479 : Config for all 1024 virtual channels, second buffer

  Polynomial data is stored in the memory as a block of 9 x 64bit words for each virtual channel:   
      word 0 = c0,
       ...  
      word 5 = c5,
               c0 to c5 are double precision floating point values for the delay polynomial :
               c0 + c1*t + c2 * t^2 + c3 * t^3 + c4 * t^4 + c5 * t^5
               Units for c0,.. c5 are ns/s^k for k=0,1,..5
      word 6 = Sky frequency in GHz
               Used to convert the delay (in ns) to a phase rotation.
               (delay in ns) * (sky frequency in GHz) = # of rotations
               From the Matlab code:
                % Phase Rotation
                %  The sampling point is (DelayOffset_all * Ts)*1e-9 ns
                %  then multiply by the center frequency (CF) to get the number of rotations.
                %
                %  The number of cycles of the center frequency per output sample is not an integer due to the oversampling of the LFAA data.
                %  For example, for coarse channel 65, the center frequency is 65 * 781250 Hz = 50781250 Hz.
                %  50781250 Hz = a period of 1/50781250 = 19.692 ns. The sampling period for the LFAA data is 1080 ns, so 
                %  a signal at the center of channel 65 goes through 1080/19.692 = 54.8438 cycles. 
                %  So a delay which is an integer number of LFAA samples still requires a phase shift to be correct.
                resampled = resampled .* exp(1i * 2*pi*DelayOffset_all * Ts * 1e-9 * CF);
                # Note : DelayOffset_all = delay in number of samples (of period Ts)
                #        Ts = sample period in ns (i.e. 1080 for SPS data)
                #        CF = channel center frequency in Hz, e.g. 65 * 781250 = 50781250 for the first SPS channel
                #        - The value [Ts * 1e-9 * CF] is the value stored here.
      word 7 = buf_offset_seconds : seconds from the polynomial epoch to the start of the corner turn frame, as a double precision value 
      word 8 = double precision offset in ns for the second polarisation (relative to the first polarisation).   
      word 9 = Validity time
               - bits 39:0 = buf_frame : corner turn frame at which the polynomial becomes valid.
                             In units of 0.053084160 seconds, i.e. units of (24 SPS packets) 
               - bit 40 = Entry is valid.

"""

# import matplotlib.pyplot as plt
import argparse
from pprint import pprint

import numpy as np
import yaml
import typing
import filterbank as fb

# number of distinct random samples to generate per virtual channel. 
RNG_SAMPLES = 8192

def command_line_args():
    parser = argparse.ArgumentParser(description="Correlator CT1 polynomial configuration generator")
    parser.add_argument(
        "-d",
        "--data",
        type=argparse.FileType(mode="wt"),
        help="File to write configuration data to, only writes non-zero data",
        required=False,
    )
    parser.add_argument(
        "-c",
        "--cfgfull",
        type=argparse.FileType(mode="wt"),
        help="File to write full configuration data to, i.e. all 160 kBytes of data",
        required=False,
    )
    parser.add_argument(
        "-t",
        "--tbdata",
        type=argparse.FileType(mode="rt"),
        help="File to read CT1 output from",
        required=False,
    )
    
    parser.add_argument(
        "-fb",
        "--tb_fb_data",
        type=argparse.FileType(mode="rt"),
        help="File to read filterbank output from",
        required=False,
    )

    parser.add_argument(
        "-f",
        "--filtertaps",
        type=argparse.FileType(mode="rt"),
        help="File to read filterbank filter taps from",
        required=False,
    )
    
    #parser.add_argument("-H0", "--HBM0", help="HBM buffer 0 data from firmware to check",
    #                    type=argparse.FileType(mode="r"))
    #parser.add_argument("-f", "--filter", help="Interpolation filter taps", type=argparse.FileType(mode="r"))
    parser.add_argument(
        "configuration",
        help="Test Configuration (YAML)",
        type=argparse.FileType(mode="r"),
    )
    return parser.parse_args()

def parse_config(file: typing.IO) -> typing.Dict:
    """
    Reads configuration YAML file, checks if required values are present.

    :param file: The YAML file object
    :return: Settings dict, guaranteed to contain the keys specified in required_keys
    :raises AssertionError: if required key(s) are missing
    """
    config = yaml.safe_load(file)

    required_keys = {"polynomials"}
    assert config.keys() >= required_keys, "Configuration YAML missing required key(s)"
    print("\n??\tSettings:")
    for parameter, value in config.items():
        if parameter == "polynomials":
            print("\tpolynomials:")
            for n, src_cfg in config["polynomials"].items():
                print(f"\t  {n}:")
                for src_param, src_val in src_cfg.items():
                    print(f"\t    {src_param}: {src_val}")
        else:
            print(f"\t{parameter}: {value}")
    return config


def ct1_config(config):
    """
    :param config: configuration data as read from the yaml file,
    config["polynomials"][source number]["virtual_channel","poly0","sky_freq0","buf_offset0","Ypol_offset0","ct_frame0","valid0",
        "poly1","sky_freq1","buf_offset1","Ypol_offset1","ct_frame1","valid1",]
    :return: numpy array of uint32 to be loaded into the CT1 polynomial configuration memory.
    """
    # keys start from 0, so add 1 to get total sources
    total_sources = np.max(list(config["polynomials"].keys())) + 1
    print(f"total_sources = {total_sources}")
    # each source has 2*80 bytes of configuration data
    # store as 4-byte integers so 40 entries per source
    config_array_buf0 = np.zeros(1024*20, np.uint32)
    config_array_buf1 = np.zeros(1024*20, np.uint32)
    poly_coefficients0 = np.zeros(6, np.float64)
    poly_coefficients1 = np.zeros(6, np.float64)
    vc_max = 0
    for n, src_cfg in config["polynomials"].items():
        vc = src_cfg["virtual_channel"]
        if vc > vc_max :
            vc_max = vc
        for c_index in range(6):
            poly_coefficients0[c_index] = np.float64(src_cfg["poly0"][c_index])
            poly_coefficients1[c_index] = np.float64(src_cfg["poly1"][c_index])
            
        config_array_buf0[(vc*20):(vc*20+12)] = np.frombuffer(poly_coefficients0.tobytes(), dtype=np.uint32)
        config_array_buf0[(vc*20+12):(vc*20+14)] = np.frombuffer(np.float64(src_cfg["sky_freq0"]).tobytes(), dtype=np.uint32)

        config_array_buf1[(vc*20):(vc*20+12)] = np.frombuffer(poly_coefficients1.tobytes(), dtype=np.uint32)
        config_array_buf1[(vc*20+12):(vc*20+14)] = np.frombuffer(np.float64(src_cfg["sky_freq1"]).tobytes(), dtype=np.uint32)

        # buf_offset_seconds : seconds from the polynomial epoch to the start of the corner turn frame, as a double precision value 
        config_array_buf0[(vc*20+14):(vc*20+16)] = np.frombuffer(np.float64(src_cfg["buf_offset0"]).tobytes(), dtype=np.uint32)
        config_array_buf1[(vc*20+14):(vc*20+16)] = np.frombuffer(np.float64(src_cfg["buf_offset1"]).tobytes(), dtype=np.uint32)
        
        # word 8 = double precision offset in ns for the second polarisation (relative to the first polarisation).   
        config_array_buf0[(vc*20+16):(vc*20+18)] = np.frombuffer(np.float64(src_cfg["Ypol_offset0"]).tobytes(), dtype=np.uint32)
        config_array_buf1[(vc*20+16):(vc*20+18)] = np.frombuffer(np.float64(src_cfg["Ypol_offset1"]).tobytes(), dtype=np.uint32)
        
        # word 9 = Validity time
        # bits 39:0 = buf_frame : corner turn frame at which the polynomial becomes valid,
        #                         in units of 0.053084160 seconds, i.e. units of (24 SPS packets) 
        # bit 40 = Entry is valid.
        int64value = np.frombuffer(np.int64(src_cfg["ct_frame0"]).tobytes(), dtype=np.uint32)
        config_array_buf0[vc*20+18] = int64value[0]
        config_array_buf0[vc*20+19] = int64value[1]
        int64value = np.frombuffer(np.int64(src_cfg["ct_frame1"]).tobytes(), dtype=np.uint32)
        config_array_buf1[vc*20+18] = int64value[0]
        config_array_buf1[vc*20+19] = int64value[1]
        
        if np.int32(src_cfg["valid0"]) > 0 :
            config_array_buf0[vc*20+19] = config_array_buf0[vc*20+19] | 256
        if np.int32(src_cfg["valid1"]) > 0 :
            config_array_buf1[vc*20+19] = config_array_buf1[vc*20+19] | 256
        
    return (config_array_buf0, config_array_buf1, vc_max)

def get_tb_fb_data(tb_file, virtual_channels):
    # Load the filterbank output data saved by the VHDL testbench
    # Example data :
    # 7 0000009B 0000 0001 0002 F7 F8 0A 0C FE F9 02 FD FC FB 0B FB
    # 0 0000009B 0000 0001 0002 FF 01 05 FD 01 01 01 02 06 02 0A FD
    # ... etc. Note 216 lines between the 7 in the first column which signifies the start of the packet
    # (one packet = 216 fine channels for a single time sample and 3 virtual channels)
    
    vc_mult3 = np.int32(3 * np.ceil(virtual_channels / 3))
    # The filterbank output data, size [frames, time samples, virtual channels, 4 values (Hre/Him/Vre/Vim), 216 frequency channels ]
    fb_data = np.zeros((10, 288, vc_mult3, 4, 216), dtype=np.int32)
    sval = np.zeros(12,dtype = np.int32)
    total_timesamples = np.zeros((10,vc_mult3),dtype=np.int64)
    first_frame_set = False
    first_frame = 0
    for line in tb_file:
        dval = line.split()
        dint = [int(di,16) for di in dval]
        vc0 = dint[2]
        vc1 = dint[3]
        vc2 = dint[4]
        if (not first_frame_set):
            first_frame = dint[1]
            frame = 0
            first_frame_set = True
        else:
            frame = dint[1] - first_frame
        if dint[0] > 0:
            fine_frequency = 0
            time_sample = total_timesamples[frame,vc0]
            total_timesamples[frame,vc0] = total_timesamples[frame,vc0] + 1
        else:
            fine_frequency += 1
        
        for dt in range(5,17):
            if dint[dt] > 127:
                sval[dt-5] = dint[dt] - 256
            else:
                sval[dt-5] = dint[dt]
        
        fb_data[frame,time_sample,vc0,0,fine_frequency] = sval[0]
        fb_data[frame,time_sample,vc0,1,fine_frequency] = sval[1]
        fb_data[frame,time_sample,vc0,2,fine_frequency] = sval[2]
        fb_data[frame,time_sample,vc0,3,fine_frequency] = sval[3]
        fb_data[frame,time_sample,vc1,0,fine_frequency] = sval[4]
        fb_data[frame,time_sample,vc1,1,fine_frequency] = sval[5]
        fb_data[frame,time_sample,vc1,2,fine_frequency] = sval[6]
        fb_data[frame,time_sample,vc1,3,fine_frequency] = sval[7]
        fb_data[frame,time_sample,vc2,0,fine_frequency] = sval[8]
        fb_data[frame,time_sample,vc2,1,fine_frequency] = sval[9]
        fb_data[frame,time_sample,vc2,2,fine_frequency] = sval[10]
        fb_data[frame,time_sample,vc2,3,fine_frequency] = sval[11]
    return fb_data

def get_tb_data(tb_file, virtual_channels):
    # Load data saved by the testbench
    # File Format : text
    #   - 3 lines of meta data, one per channel
    #                  <1-3> HdeltaP, HoffsetP, VdeltaP, VoffsetP, frame, virtual channel
    # array element:    0       1       2         3          4       5         6   
    #   - 192 lines of data   
    #      4 <re Hpol> <im Hpol> <re Vpol> <im Vpol> ... (x3 for 3 virtual channels)

    first_frame_set = False
    first_frame = 0
    # Number of packets received for each integration, frame and virtual channel
    # Data comes from the corner turn in bursts of 3 virtual channels.
    vc_mult3 = np.int32(3*np.ceil(virtual_channels/3))
    packet_count = np.zeros((10,vc_mult3),dtype=np.int32)
    # meta data : [frame, packet, vc, hdelta/Hoffset/Vdelta/Voffset]
    # 271 packets = 15 preload + 256 per frame
    meta_data = np.zeros((10,271,vc_mult3,4),dtype = np.int32)
    # data [frame,packet,vc,Hre/Him/Vre/Vim,sample]
    data_data = np.zeros((10,271,vc_mult3,4,192), dtype = np.int32)
    vc_list = np.zeros(4,dtype = np.int32)
    for line in tb_file:
        dval = line.split()
        dint = [int(di,16) for di in dval]
        if dint[0] == 1:
            if not first_frame_set:
                first_frame_set = True
                first_frame = dint[5]
        if (dint[0] == 1 or dint[0] == 2 or dint[0] == 3):
            frame = dint[5] - first_frame
            vc = dint[6]
            vc_list[dint[0]-1] = vc
            # meta data indexed by [integration, frame (0,1,2), packet (), vc, parameter]
            #  where parameter : 0 = HdeltaP, 1 = HoffsetP, 2 = VdeltaP, 3 = VoffsetP
            #print(f'vc = {vc}, frame = {frame}, packet_count = {packet_count[frame, vc]}')
            meta_data[frame, packet_count[frame, vc], vc, 0] = dint[1]
            meta_data[frame, packet_count[frame, vc], vc, 1] = dint[2]
            meta_data[frame, packet_count[frame, vc], vc, 2] = dint[3]
            meta_data[frame, packet_count[frame, vc], vc, 3] = dint[4]
            packet_count[frame, vc] = packet_count[frame, vc] + 1
            dcount = 0
        else:
            # dint[0] == 4, the data part
            # data_data index by [integration, frame, packet_count, vc, Hre/Him/Vre/Vim, sample
            data_data[frame, packet_count[frame,vc] - 1, vc_list[0], 0, dcount] = dint[1]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[0], 1, dcount] = dint[2]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[0], 2, dcount] = dint[3]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[0], 3, dcount] = dint[4]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[1], 0, dcount] = dint[5]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[1], 1, dcount] = dint[6]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[1], 2, dcount] = dint[7]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[1], 3, dcount] = dint[8]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[2],0, dcount] = dint[9]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[2],1, dcount] = dint[10]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[2],2, dcount] = dint[11]
            data_data[frame, packet_count[frame,vc] - 1, vc_list[2],3, dcount] = dint[12]
            dcount = dcount + 1
    
    return (meta_data, data_data, packet_count)

def main():
    # Read command-line arguments
    args = command_line_args()
    config = parse_config(args.configuration)
    # convert config into a data file to load into the firmware
    (cfg_array0, cfg_array1, vc_max) = ct1_config(config)
    # Write to file.
    # Writes are in blocks of 20 words, preceded by the address to write to.
    total_blocks = vc_max+1
    first_block = True
    for b in range(total_blocks):
        # 80 bytes per block
        block_addr = b*80
        block_addr2 = 1024*80 + b*80
        non_zero = np.any(cfg_array0[b*20:(b*20+20)])
        if non_zero:
            if not first_block:
                args.data.write("\n")
            first_block = False
            args.data.write(f"[{block_addr:08x}]")
            for n in range(20):
                args.data.write(f"\n{cfg_array0[b*20+n]:08x}")
            args.data.write(f"\n[{block_addr2:08x}]")
            for n in range(20):
                args.data.write(f"\n{cfg_array1[b*20+n]:08x}")
    
    # Also write a configuration file that initialises the entire 1 MByte
    # of configuration address space.
    # This can be used for initialising the hardware, otherwise the ultraRAM
    # buffer could contain anything on startup.
    # 2 buffers, 1024 streams, 20 x 4 byte words
    if args.cfgfull:
        full_config_array = np.zeros(2*1024*20, np.uint32)
        # Load both buffers
        full_config_array[0:cfg_array0.size] = cfg_array0[:]
        full_config_array[20480:(20480+cfg_array1.size)] = cfg_array1[:]
        # Write in blocks of 4 kByte (40 * 4096 = 160 kBytes)
        for block in range(40):
            # Offset is in units of 4 bytes (?)
            if block > 0:
                args.cfgfull.write("\n")
            args.cfgfull.write(f"[vd_datagen.vd_ram.data][{(block*1024)}]")
            for n in range(1024):
                args.cfgfull.write(f"\n0x{full_config_array[block*1024+n]:08x}")

    # If we are using the random number generator, then write out the random values.
    if config["use_rng"]:
        rng = np.random.default_rng(seed = 10)  # fix the seed so results are consistent.
        # Generate random values, write to file, and keep a copy in a numpy array.
        # 3-d numpy array : (2 pol) x (virtual channels) x (RNG_SAMPLES)
        sps_data = np.zeros((2,vc_max+1,RNG_SAMPLES),dtype = np.complex128)
        with open('rng_init.txt','w') as f:
            for vc in range(vc_max+1):
                for b in range(RNG_SAMPLES):
                    v1 = rng.integers(low=-20, high = 20, size = 4)
                    sps_data[0,vc,b] = v1[3] + 1j * v1[2]  # odd order for the 4 elements because of the way "v32" is constructed below.
                    sps_data[1,vc,b] = v1[1] + 1j * v1[0]
                    v32 = 0
                    for v1i in range(4):
                        if v1[v1i] < 0:
                            v1[v1i] = v1[v1i] + 256
                        v32 = v32*256 + v1[v1i]
                    f.write(f"{v32:08x}\n")
    
    
    # Get the output of the simulation
    if args.tbdata:
        (meta_data, data_data, packet_count) = get_tb_data(args.tbdata, total_blocks)
        tb_valid = True
    else:
        tb_valid = False

    if args.tb_fb_data:
        tb_fb_data = get_tb_fb_data(args.tb_fb_data, vc_max+1)
        tb_fb_valid = False
    else:
        tb_fb_valid = False
    
    # Initialise the filterbank 
    fb1 = fb.PolyphaseFilterBank(fir_file = args.filtertaps,
                                 fft_length = 256,
                                 fir_taps = 12,
                                 oversample_numerator = 4,
                                 oversample_denominator= 3)
    
    # Calculate the expected delays for each virtual channel
    frame_start = config["ct_frame_start"]
    sim_frames = config["sim_frames"]
    data_mismatch = 0
    data_match = 0
    meta_match = 0
    meta_mismatch = 0
    for frame in range(sim_frames):
        frame_from_epoch = frame + frame_start
        for vc in range(vc_max + 1):
            # Find the config entry for this virtual channel
            vc_found = False
            for n, src_cfg in config["polynomials"].items():
                this_vc = src_cfg["virtual_channel"]
                if vc == this_vc:
                    if vc_found == True:
                        print(f"!!!! Multiple instances of virtual channel {this_vc} in config yaml file")
                    vc_found = True
                    cfg_n = n
            if  not vc_found:
                print(f"!!! frame {frame}, No specification for virtual channel {vc}")
            else:
                src_cfg = config["polynomials"][vc]
                if (src_cfg["valid0"]==1) and (frame_from_epoch >= src_cfg["ct_frame0"]):
                    cfg0_valid = True
                else:
                    cfg0_valid = False
                if (src_cfg["valid1"]==1) and (frame_from_epoch >= src_cfg["ct_frame1"]):
                    cfg1_valid = True
                else:
                    cfg1_valid = False
               
                if cfg1_valid and ((not cfg0_valid) or (src_cfg["ct_frame1"] > src_cfg["ct_frame0"])):
                    # select second configuration
                    poly = src_cfg["poly1"]
                    # sky frequency in GHz
                    sky_freq = src_cfg["sky_freq1"]
                    # Validity time : 32 bit buf_integration: Integration period at which the polynomial becomes valid.
                    frame_validity = src_cfg["ct_frame1"]
                    # seconds from the polynomial epoch to the start of the integration period, as a double precision value
                    buf_offset = src_cfg["buf_offset1"]
                    # Double precision offset in ns for the second polarisation (relative to the first polarisation). 
                    Ypol_offset = src_cfg["Ypol_offset1"]
                else:
                    if (not cfg0_valid) and (not cfg1_valid):
                        print(f"No valid polynomials, ")
                    # select first configuration
                    poly = src_cfg["poly0"]
                    sky_freq = src_cfg["sky_freq0"]
                    frame_validity = src_cfg["ct_frame0"]
                    buf_offset = src_cfg["buf_offset0"]
                    Ypol_offset = src_cfg["Ypol_offset0"]
                
                # 271 packets produced by CT1 for each frame
                # for coarse delay of 0, 8 from the previous frame, 256 from the current frame, and 7 from the next frame.
                # Time in seconds in the polynomial
                t = buf_offset + (frame_from_epoch - frame_validity) * 0.053084160
                t_frame_base = t
                delay_Xpol = poly[0] + poly[1]*t + poly[2]*(t**2) + poly[3]*(t**3) + poly[4]*(t**4) + poly[5]*(t**5)
                delay_Ypol = delay_Xpol + Ypol_offset
                delay_samples_Xpol = delay_Xpol / 1080.0
                coarse_delay = np.int32(np.floor(delay_samples_Xpol))
                delay_samples_Ypol = delay_Ypol / 1080.0
                
                # Generate the expected output from the corner turn for this frame and virtual channel
                # total number of samples is 15*192 (preload) + 24*2048 (corner turn frame) = 58176
                first_sample = frame_from_epoch * 24 * 2048 - 6 * 256 - coarse_delay
                # Output from the corner turn : (2 pol) x (52032 time samples)
                # time samples = 24*2048 (ct frame) + (12*256 - 192) (preload + postload) = 52032
                # Total output packets = time_samples/192  (192 samples per ct1 output packet to the filterbanks)
                ct1_samples_per_frame = 24*2048 + 12*256 - 192
                ct1_packets = int(ct1_samples_per_frame / 192)
                ct1_data = np.zeros((2,ct1_samples_per_frame),dtype = np.complex128)
                # Output from the filterbank : (2 pol) x (288 time samples) x (216 frequency channels)
                fb_out = np.zeros((2,256,216), dtype = np.complex128)
                for pol in range(2):
                    if config["use_rng"]:
                        for sample in range(ct1_samples_per_frame):
                            ct1_data[pol,sample] = sps_data[pol,vc,(first_sample+sample)%RNG_SAMPLES]
                    else:  # use a counter 
                        for sample in range(ct1_samples_per_frame):
                            if pol == 0: # When using a counter, pol 0 = sample count, pol 1 = sample count bits (23:16), and virtual channel (7:0) in imaginary part.
                                ct1_data[pol,sample] = (first_sample+sample) % 256 + 1j * (np.floor((first_sample+sample)/256) % 256)
                            else:
                                ct1_data[pol,sample] = np.floor((first_sample+sample)/65536) % 256 + 1j * vc
                    # Apply the filterbank
                    fb_out[pol,:,:] = fb1.filter(din = ct1_data[pol,:],time_steps = 256, derotate = True, keep = 216, filter_scale = 512, fft_scale = 256, saturate = False, zero_pad = False, zero_pad_length = 0)
                
                if vc == 0:
                    print(f"pol0 filterbank input : ")
                    print(ct1_data[0,0:2])
                    print(f"pol1 filterbank input : ")
                    print(ct1_data[1,0:2])
                    print(f"virtual channel {vc} pol 0 filterbank output: ")
                    print(fb_out[0,0,0:2])
                    print(f"virtual channel {vc} pol 1 filterbank output: ")
                    print(fb_out[1,0,0:2])
                
                # Check the testbench output against the expected output 
                for packet in range(ct1_packets):

                    # Each packet is 192 * 1080ns = 207360 ns
                    if packet > 14:
                        t = t_frame_base + (packet-15) * 0.000207360
                    delay_Xpol = poly[0] + poly[1]*t + poly[2]*(t**2) + poly[3]*(t**3) + poly[4]*(t**4) + poly[5]*(t**5)
                    delay_Ypol = delay_Xpol + Ypol_offset
                    delay_samples_Xpol = delay_Xpol / 1080.0
                    delay_samples_Ypol = delay_Ypol / 1080.0
                    fine_delay_Xpol = delay_samples_Xpol - coarse_delay
                    fine_delay_Ypol = delay_samples_Ypol - coarse_delay
                    if (fine_delay_Xpol >= 0):
                        fine_delay_Xpol = np.int64(np.floor(fine_delay_Xpol * 16384*65536))
                    else:
                        fine_delay_Xpol = 65536*65536 - np.int64(np.floor(-fine_delay_Xpol*16384*65536))
                    if (fine_delay_Ypol >= 0):
                        fine_delay_Ypol = np.int64(np.floor(fine_delay_Ypol * 16384*65536))
                    else:
                        fine_delay_Ypol = 65536*65536 - np.int64(np.floor(-fine_delay_Ypol*16384*65536))
                    phase_X = delay_Xpol * sky_freq
                    phase_Y = delay_Ypol * sky_freq
                    phase_X = np.int64(np.floor(65536*65536 * (phase_X - np.floor(phase_X))))
                    phase_Y = np.int64(np.floor(65536*65536 * (phase_Y - np.floor(phase_Y))))
                    if phase_X >= 2**31 :
                        phase_X = phase_X - 2**32  # firmware gives negative values, (phase_X - np.floor(phase_X)) is always +ve.
                    if phase_Y >= 2**31 :
                        phase_Y = phase_Y - 2**32
                    
                    # print(f"VC = {vc}, (int,frame,packet) = ({integration},{frame_in_integration},{packet}) coarse = {coarse_delay}, fine X = {fine_delay_Xpol}, fine Y = {fine_delay_Ypol}, phase X = {phase_X}, phase_Y = {phase_Y}")
                    if tb_fb_valid:
                        if packet > 14:
                            # compare filterbank output data with the testbench result
                            # Python : for virtual channel = vc, frame = frame, 
                            #         filterbank output is fb_out[(2 pol) x (288 time samples) x (216 frequency channels)]
                            # Testbench : tb_fb_data[frames, time samples, virtual channels, 4 values (Hre/Him/Vre/Vim), 216 frequency channels ]
                            for fine_freq in range(216):
                                python_X = fb_out[1,packet-15,fine_freq] * np.exp(-1j * 2 * np.pi * (1/(2**32)) * (phase_X + fine_delay_Xpol * (fine_freq-108)/128))
                                python_Y = fb_out[0,packet-15,fine_freq] * np.exp(-1j * 2 * np.pi * (1/(2**32)) * (phase_Y + fine_delay_Ypol * (fine_freq-108)/128))
                                tb_Y = tb_fb_data[frame,packet-15,vc,0,fine_freq] + 1j * tb_fb_data[frame,packet-15,vc,1,fine_freq]
                                tb_X = tb_fb_data[frame,packet-15,vc,2,fine_freq] + 1j * tb_fb_data[frame,packet-15,vc,3,fine_freq]
                                diff_X = tb_X - python_X
                                diff_Y = tb_Y - python_Y
                                if ((np.abs(diff_X) > 2) or (np.abs(diff_Y) > 1)):
                                    print(f"frame {frame}, vc {vc}, packet {packet-15}, fine channel {fine_freq}, tb_X = {tb_X}, python_X = {python_X}, tb_Y = {tb_Y}, python_Y = {python_Y}")
                        
                    if tb_valid:
                        # Compare expected data with the data loaded from the testbench
                        # Calculate which sample this packet should start at
                        # Simulation puts the sample number in the data, where the 
                        # sample number is the number of samples since the epoch
                        first_sample = frame_from_epoch * 24 * 2048 - 6*256 - coarse_delay
                        for sample in range(192):
                            Xre = data_data[frame,packet,vc,0,sample]
                            Xim = data_data[frame,packet,vc,1,sample]
                            Yre = data_data[frame,packet,vc,2,sample]
                            Yim = data_data[frame,packet,vc,3,sample]
                            tb_sample = Xre + 256*Xim + 65536*Yre
                            tb_vc = Yim
                            if ((tb_sample != (first_sample+(packet*192)+sample)) or (vc != tb_vc)):
                                if data_mismatch < 20:  # Only display the first few errors.
                                    print(f"Bad sample : VC = {vc}, (frame_from_epoch,frame,packet) = ({frame_from_epoch},{frame},{packet}) coarse = {coarse_delay}")
                                    print(f"   At sample {sample}, expected {first_sample+(packet*192)+sample}, got {tb_sample}, for vc = {vc}, (tb_vc = {tb_vc})")
                                data_mismatch += 1
                            else:
                                data_match += 1 
                        # Compare fine delays
                        fine_delay_Xpol_tb = meta_data[frame,packet,vc,0]
                        phase_X_tb = meta_data[frame,packet,vc,1]
                        fine_delay_Ypol_tb = meta_data[frame,packet,vc,2]
                        phase_Y_tb = meta_data[frame,packet,vc,3]
                        # Allow a difference of 1 to prevent flagging differences in rounding.
                        # A difference of 1 in a 32-bit phase value is insignificant.
                        if ((np.abs(fine_delay_Xpol_tb - fine_delay_Xpol) > 1) or
                            (np.abs(fine_delay_Ypol_tb - fine_delay_Ypol) > 1) or
                            (np.abs(phase_X_tb - phase_X) > 1) or
                            (np.abs(phase_Y_tb - phase_Y) > 1)):
                            if meta_mismatch < 20:
                                print(f"PYTHON : VC = {vc}, (frame,packet) = ({frame},{packet}) coarse = {coarse_delay}, fine X = {fine_delay_Xpol}, fine Y = {fine_delay_Ypol}, phase X = {phase_X}, phase_Y = {phase_Y}")    
                                print(f"    TB : fine X = {fine_delay_Xpol_tb}, fine Y = {fine_delay_Ypol_tb}, phase X = {phase_X_tb}, phase_Y = {phase_Y_tb}")
                            meta_mismatch += 1
                        else:
                            meta_match += 1
                    elif packet < 10:
                        print(f"No tb data : VC = {vc}, (frame,packet) = ({frame},{packet}) coarse = {coarse_delay}, fine X = {fine_delay_Xpol}, fine Y = {fine_delay_Ypol}, phase X = {phase_X}, phase_Y = {phase_Y}")

    if tb_valid:
        print(f"checked {sim_frames} frames against simulation")
        print(f"    data sample mismatch = {data_mismatch}, data samples matched = {data_match} ")
        print(f"    meta data mismatch = {meta_mismatch}, meta data matched = {meta_match}")

if __name__ == "__main__":
    main()