----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 19.03.2020 00:49:53
-- Module Name: free_list_URAM - Behavioral
-- Description: 
--  Instantiate URAM for the free list for the corner turn.
--  3 cycle read latency.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library xpm;
use xpm.vcomponents.all;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;

entity free_list_URAM is
    generic(
        g_USE_2GBYTE : boolean := false
    );
    port (
        i_clk    : in std_logic;
        i_wrAddr : in std_logic_vector(15 downto 0);
        i_wrData : in std_logic_vector(15 downto 0);
        i_wrEn   : in std_logic;
        i_rdAddr : in std_logic_vector(15 downto 0); 
        o_rdData : out std_logic_vector(15 downto 0)
    );
end free_list_URAM;

architecture Behavioral of free_list_URAM is

    constant c_ADDR_WIDTH : integer := sel(g_USE_2GBYTE,15,14);
    constant c_URAM_ADDR_WIDTH : integer := sel(g_USE_2GBYTE,13,12);
    constant c_FREE_LIST_N_URAMS : integer := sel(g_USE_2GBYTE,2,1);

    signal dout,din : std_logic_vector(63 downto 0);
    signal wrAddr, rdAddr : std_logic_vector(13 downto 0);
    signal wrEn : std_logic_vector(7 downto 0);
    signal rdAddrLowDel2, rdAddrLowDel1 : std_logic_vector(1 downto 0);
    
begin
    
    -- Note xpm macros aren't very smart - they cannot resize an ultraRAM from 4096x64 to 16384x16, they just use 4 URAMs.
    -- So the interface width has to be 64, and we have to mux down separately.
    free_list_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => c_URAM_ADDR_WIDTH,  -- DECIMAL  -- 64 bit wide by 4096 or 8192 deep.
        ADDR_WIDTH_B => c_URAM_ADDR_WIDTH,  -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL
        --CASCADE_HEIGHT => 0,           -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => c_FREE_LIST_N_URAMS*16384*16, -- DECIMAL; total size in bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "read_first"      -- String
    )
    port map (
        dbiterrb => open,           -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
        doutb    => Dout,   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,           -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra    => wrAddr(c_URAM_ADDR_WIDTH-1 downto 0), -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb    => rdAddr(c_URAM_ADDR_WIDTH-1 downto 0), -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka     => i_clk,          -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb     => i_clk,          -- 1-bit input, unused when clocking mode is "common_clock"
        dina     => Din,    -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena      => '1',            -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
        enb      => '1',            -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
        injectdbiterra => '0',      -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
        injectsbiterra => '0',      -- 1-bit input: Controls single bit error injection on input data when ECC enabled
        regceb => '1',              -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',                -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
        sleep => '0',               -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => wrEn         -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    
    rdAddr <= i_rdAddr(15 downto 2);
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            -- Put the data in the right place for writes
            if i_wrEn = '1' then
                if i_wrAddr(1 downto 0) = "00" then
                    wrEn <= "00000011";
                elsif i_wrAddr(1 downto 0) = "01" then
                    wrEn <= "00001100";
                elsif i_wrAddr(1 downto 0) = "10" then
                    wrEn <= "00110000";
                else
                    wrEn <= "11000000";
                end if;
            else
                wrEn <= "00000000";
            end if;
            Din <= i_wrData & i_wrData & i_wrData & i_wrData;
            wrAddr <= i_wrAddr(15 downto 2);
            
            -- Mux the requested read data out
            rdAddrLowDel1 <= i_rdAddr(1 downto 0);
            rdAddrLowDel2 <= rdAddrLowDel1;
            if rdAddrLowDel2 = "00" then
                o_rdData <= Dout(15 downto 0);
            elsif rdAddrLowDel2 = "01" then
                o_rdData <= Dout(31 downto 16);
            elsif rdAddrLowDel2 = "10" then
                o_rdData <= Dout(47 downto 32);
            else
                o_rdData <= Dout(63 downto 48);
            end if;
        end if;
    end process;

end Behavioral;


-- XPM_MEMORY instantiation template for Simple Dual Port RAM configurations
-- Refer to the targeted device family architecture libraries guide for XPM_MEMORY documentation
-- =======================================================================================================================

-- Parameter usage table, organized as follows:
-- +---------------------------------------------------------------------------------------------------------------------+
-- | Parameter name       | Data type          | Restrictions, if applicable                                             |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Description                                                                                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- +---------------------------------------------------------------------------------------------------------------------+
-- | ADDR_WIDTH_A         | Integer            | Range: 1 - 20. Default value = 6.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify the width of the port A address port addra, in bits.                                                        |
-- | Must be large enough to access the entire memory from port A, i.e. = $clog2(MEMORY_SIZE/WRITE_DATA_WIDTH_A).        |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | ADDR_WIDTH_B         | Integer            | Range: 1 - 20. Default value = 6.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify the width of the port B address port addrb, in bits.                                                        |
-- | Must be large enough to access the entire memory from port B, i.e. = $clog2(MEMORY_SIZE/READ_DATA_WIDTH_B).         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | AUTO_SLEEP_TIME      | Integer            | Range: 0 - 15. Default value = 0.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Number of clk[a|b] cycles to auto-sleep, if feature is available in architecture.                                   |
-- |                                                                                                                     |
-- |   0 - Disable auto-sleep feature                                                                                    |
-- |   3-15 - Number of auto-sleep latency cycles                                                                        |
-- |                                                                                                                     |
-- | Do not change from the value provided in the template instantiation.                                                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | BYTE_WRITE_WIDTH_A   | Integer            | Range: 1 - 4608. Default value = 32.                                    |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | To enable byte-wide writes on port A, specify the byte width, in bits.                                              |
-- |                                                                                                                     |
-- |   8- 8-bit byte-wide writes, legal when WRITE_DATA_WIDTH_A is an integer multiple of 8                              |
-- |   9- 9-bit byte-wide writes, legal when WRITE_DATA_WIDTH_A is an integer multiple of 9                              |
-- |                                                                                                                     |
-- | Or to enable word-wide writes on port A, specify the same value as for WRITE_DATA_WIDTH_A.                          |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | CASCADE_HEIGHT       | Integer            | Range: 0 - 64. Default value = 0.                                       |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | 0- No Cascade Height, Allow Vivado Synthesis to choose.                                                             |
-- | 1 or more - Vivado Synthesis sets the specified value as Cascade Height.                                            |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | CLOCKING_MODE        | String             | Allowed values: common_clock, independent_clock. Default value = common_clock.|
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Designate whether port A and port B are clocked with a common clock or with independent clocks.                     |
-- |                                                                                                                     |
-- |   "common_clock"- Common clocking; clock both port A and port B with clka                                           |
-- |   "independent_clock"- Independent clocking; clock port A with clka and port B with clkb                            |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | ECC_MODE             | String             | Allowed values: no_ecc, both_encode_and_decode, decode_only, encode_only. Default value = no_ecc.|
-- |---------------------------------------------------------------------------------------------------------------------|
-- |                                                                                                                     |
-- |   "no_ecc" - Disables ECC                                                                                           |
-- |   "encode_only" - Enables ECC Encoder only                                                                          |
-- |   "decode_only" - Enables ECC Decoder only                                                                          |
-- |   "both_encode_and_decode" - Enables both ECC Encoder and Decoder                                                   |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | MEMORY_INIT_FILE     | String             | Default value = none.                                                   |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify "none" (including quotes) for no memory initialization, or specify the name of a memory initialization file.|
-- | Enter only the name of the file with .mem extension, including quotes but without path (e.g. "my_file.mem").        |
-- | File format must be ASCII and consist of only hexadecimal values organized into the specified depth by              |
-- | narrowest data width generic value of the memory. See the Memory File (MEM) section for more                        |
-- | information on the syntax. Initialization of memory happens through the file name specified only when parameter     |
-- | MEMORY_INIT_PARAM value is equal to "".                                                                             |
-- | When using XPM_MEMORY in a project, add the specified file to the Vivado project as a design source.                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | MEMORY_INIT_PARAM    | String             | Default value = 0.                                                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify "" or "0" (including quotes) for no memory initialization through parameter, or specify the string          |
-- | containing the hex characters. Enter only hex characters with each location separated by delimiter (,).             |
-- | Parameter format must be ASCII and consist of only hexadecimal values organized into the specified depth by         |
-- | narrowest data width generic value of the memory.For example, if the narrowest data width is 8, and the depth of    |
-- | memory is 8 locations, then the parameter value should be passed as shown below.                                    |
-- | parameter MEMORY_INIT_PARAM = "AB,CD,EF,1,2,34,56,78"                                                               |
-- | Where "AB" is the 0th location and "78" is the 7th location.                                                        |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | MEMORY_OPTIMIZATION  | String             | Allowed values: true, false. Default value = true.                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify "true" to enable the optimization of unused memory or bits in the memory structure. Specify "false" to      |
-- | disable the optimization of unused memory or bits in the memory structure.                                          |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | MEMORY_PRIMITIVE     | String             | Allowed values: auto, block, distributed, ultra. Default value = auto.  |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Designate the memory primitive (resource type) to use.                                                              |
-- |                                                                                                                     |
-- |   "auto"- Allow Vivado Synthesis to choose                                                                          |
-- |   "distributed"- Distributed memory                                                                                 |
-- |   "block"- Block memory                                                                                             |
-- |   "ultra"- Ultra RAM memory                                                                                         |
-- |                                                                                                                     |
-- | NOTE: There may be a behavior mismatch if Block RAM or Ultra RAM specific features, like ECC or Asymmetry, are selected with MEMORY_PRIMITIVE set to "auto".|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | MEMORY_SIZE          | Integer            | Range: 2 - 150994944. Default value = 2048.                             |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify the total memory array size, in bits. For example, enter 65536 for a 2kx32 RAM.                             |
-- |                                                                                                                     |
-- |   When ECC is enabled and set to "encode_only", then the memory size has to be multiples of READ_DATA_WIDTH_B       |
-- |   When ECC is enabled and set to "decode_only", then the memory size has to be multiples of WRITE_DATA_WIDTH_A      |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | MESSAGE_CONTROL      | Integer            | Range: 0 - 1. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify 1 to enable the dynamic message reporting such as collision warnings, and 0 to disable the message reporting|
-- +---------------------------------------------------------------------------------------------------------------------+
-- | READ_DATA_WIDTH_B    | Integer            | Range: 1 - 4608. Default value = 32.                                    |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify the width of the port B read data output port doutb, in bits.                                               |
-- |                                                                                                                     |
-- |   When ECC is enabled and set to "encode_only", then READ_DATA_WIDTH_B has to be multiples of 72-bits               |
-- |   When ECC is enabled and set to "decode_only" or "both_encode_and_decode", then READ_DATA_WIDTH_B has to be        |
-- | multiples of 64-bits                                                                                                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | READ_LATENCY_B       | Integer            | Range: 0 - 100. Default value = 2.                                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify the number of register stages in the port B read data pipeline. Read data output to port doutb takes this   |
-- | number of clkb cycles (clka when CLOCKING_MODE is "common_clock").                                                  |
-- | To target block memory, a value of 1 or larger is required- 1 causes use of memory latch only; 2 causes use of      |
-- | output register. To target distributed memory, a value of 0 or larger is required- 0 indicates combinatorial output.|
-- | Values larger than 2 synthesize additional flip-flops that are not retimed into memory primitives.                  |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | READ_RESET_VALUE_B   | String             | Default value = 0.                                                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify the reset value of the port B final output register stage in response to rstb input port is assertion.      |
-- | As this parameter is a string, please specify the hex values inside double quotes. As an example,                   |
-- | If the read data width is 8, then specify READ_RESET_VALUE_B = "EA";                                                |
-- | When ECC is enabled, reset value is not supported.                                                                  |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | RST_MODE_A           | String             | Allowed values: SYNC, ASYNC. Default value = SYNC.                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Describes the behaviour of the reset                                                                                |
-- |                                                                                                                     |
-- |   "SYNC" - when reset is applied, synchronously resets output port douta to the value specified by parameter READ_RESET_VALUE_A|
-- |   "ASYNC" - when reset is applied, asynchronously resets output port douta to zero                                  |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | RST_MODE_B           | String             | Allowed values: SYNC, ASYNC. Default value = SYNC.                      |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Describes the behaviour of the reset                                                                                |
-- |                                                                                                                     |
-- |   "SYNC" - when reset is applied, synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B|
-- |   "ASYNC" - when reset is applied, asynchronously resets output port doutb to zero                                  |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | SIM_ASSERT_CHK       | Integer            | Range: 0 - 1. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | 0- Disable simulation message reporting. Messages related to potential misuse will not be reported.                 |
-- | 1- Enable simulation message reporting. Messages related to potential misuse will be reported.                      |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | USE_EMBEDDED_CONSTRAINT| Integer            | Range: 0 - 1. Default value = 0.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify 1 to enable the set_false_path constraint addition between clka of Distributed RAM and doutb_reg on clkb    |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | USE_MEM_INIT         | Integer            | Range: 0 - 1. Default value = 1.                                        |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify 1 to enable the generation of below message and 0 to disable generation of the following message completely.|
-- | "INFO - MEMORY_INIT_FILE and MEMORY_INIT_PARAM together specifies no memory initialization.                         |
-- | Initial memory contents will be all 0s."                                                                            |
-- | NOTE: This message gets generated only when there is no Memory Initialization specified either through file or      |
-- | Parameter.                                                                                                          |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | WAKEUP_TIME          | String             | Allowed values: disable_sleep, use_sleep_pin. Default value = disable_sleep.|
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Specify "disable_sleep" to disable dynamic power saving option, and specify "use_sleep_pin" to enable the           |
-- | dynamic power saving option                                                                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | WRITE_DATA_WIDTH_A   | Integer            | Range: 1 - 4608. Default value = 32.                                    |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | multiples of 64-bits                                                                                                |
-- | When ECC is enabled and set to "decode_only", then WRITE_DATA_WIDTH_A has to be multiples of 72-bits                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | WRITE_MODE_B         | String             | Allowed values: no_change, read_first, write_first. Default value = no_change.|
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write mode behavior for port B output data port, doutb.                                                             |
-- +---------------------------------------------------------------------------------------------------------------------+

-- Port usage table, organized as follows:
-- +---------------------------------------------------------------------------------------------------------------------+
-- | Port name      | Direction | Size, in bits                         | Domain  | Sense       | Handling if unused     |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Description                                                                                                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- +---------------------------------------------------------------------------------------------------------------------+
-- | addra          | Input     | ADDR_WIDTH_A                          | clka    | NA          | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Address for port A write operations.                                                                                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | addrb          | Input     | ADDR_WIDTH_B                          | clkb    | NA          | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Address for port B read operations.                                                                                 |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | clka           | Input     | 1                                     | NA      | Rising edge | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".                         |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | clkb           | Input     | 1                                     | NA      | Rising edge | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Clock signal for port B when parameter CLOCKING_MODE is "independent_clock".                                        |
-- | Unused when parameter CLOCKING_MODE is "common_clock".                                                              |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | dbiterrb       | Output    | 1                                     | clkb    | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Status signal to indicate double bit error occurrence on the data output of port B.                                 |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | dina           | Input     | WRITE_DATA_WIDTH_A                    | clka    | NA          | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Data input for port A write operations.                                                                             |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | doutb          | Output    | READ_DATA_WIDTH_B                     | clkb    | NA          | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Data output for port B read operations.                                                                             |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | ena            | Input     | 1                                     | clka    | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Memory enable signal for port A.                                                                                    |
-- | Must be high on clock cycles when write operations are initiated. Pipelined internally.                             |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | enb            | Input     | 1                                     | clkb    | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Memory enable signal for port B.                                                                                    |
-- | Must be high on clock cycles when read operations are initiated. Pipelined internally.                              |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | injectdbiterra | Input     | 1                                     | clka    | Active-high | Tie to 1'b0            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Controls double bit error injection on input data when ECC enabled (Error injection capability is not available in  |
-- | "decode_only" mode).                                                                                                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | injectsbiterra | Input     | 1                                     | clka    | Active-high | Tie to 1'b0            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Controls single bit error injection on input data when ECC enabled (Error injection capability is not available in  |
-- | "decode_only" mode).                                                                                                |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | regceb         | Input     | 1                                     | clkb    | Active-high | Tie to 1'b1            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Clock Enable for the last register stage on the output data path.                                                   |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | rstb           | Input     | 1                                     | clkb    | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Reset signal for the final port B output register stage.                                                            |
-- | Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.                      |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | sbiterrb       | Output    | 1                                     | clkb    | Active-high | DoNotCare              |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Status signal to indicate single bit error occurrence on the data output of port B.                                 |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | sleep          | Input     | 1                                     | NA      | Active-high | Tie to 1'b0            |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | sleep signal to enable the dynamic power saving feature.                                                            |
-- +---------------------------------------------------------------------------------------------------------------------+
-- | wea            | Input     | WRITE_DATA_WIDTH_A                    | clka    | Active-high | Required               |
-- |---------------------------------------------------------------------------------------------------------------------|
-- | Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.                     |
-- | In byte-wide write configurations, each bit controls the writing one byte of dina to address addra.                 |
-- | For example, to synchronously write only bits [15-8] of dina when WRITE_DATA_WIDTH_A is 32, wea would be 4'b0010.   |
-- +---------------------------------------------------------------------------------------------------------------------+


-- xpm_memory_sdpram : In order to incorporate this function into the design,
--       VHDL        : the following instance declaration needs to be placed
--     instance      : in the body of the design code.  The instance name
--    declaration    : (xpm_memory_sdpram_inst) and/or the port declarations after the
--       code        : "=>" declaration maybe changed to properly reference and
--                   : connect this function to the design.  All inputs and outputs
--                   : must be connected.

--      Library      : In addition to adding the instance declaration, a use
--    declaration    : statement for the UNISIM.vcomponents library needs to be
--        for        : added before the entity declaration.  This library
--      Xilinx       : contains the component declarations for all Xilinx
--    primitives     : primitives and points to the models that will be used
--                   : for simulation.

--  Please reference the appropriate libraries guide for additional information on the XPM modules.

--  Copy the following two statements and paste them before the
--  Entity declaration, unless they already exist.
