.. vim: syntax=rst

Low CBF Firmware - PST
======================

This repository contains the VHDL source code to implement the 
**P**\ ul\ **s**\ ar **T**\ iming (PST) output product of Low CBF.

This document focusses on PST-specific information, see also
`Low CBF Firmware Common <https://developer.skao.int/projects/ska-low-cbf-fw-common/en/latest/?badge=latest>`_.

Building Firmware
-----------------

The build process is automated via the GitLab CI/CD pipeline, and local builds
can be made `via the build.sh script in the common module <https://developer.skao.int/projects/ska-low-cbf-fw-common/en/latest/scripts.html#local-build>`_.


Configuring Firmware
--------------------

PST can now only be configured by appropriate software.
This software is written to control the registers that are described in YAML files.
There is YAML file for each major element in the firmware design and they are spread across PST and Common Repositories.

The full list of YAML files is:

- `LFAADecode100G <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pst/-/blob/main/libraries/signalProcessing/LFAADecode100G/LFAADecode100G.peripheral.yaml?ref_type=heads>`_.
- `pst_ct1 <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pst/-/blob/main/libraries/signalProcessing/corner_turner/ct1/pst_ct1.peripheral.yaml?ref_type=heads>`_.
- `ct2 <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pst/-/blob/main/libraries/signalProcessing/corner_turner/ct2/ct2.peripheral.yaml?ref_type=heads>`_.
- `PSTbeamformer_dp <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pst/-/blob/main/libraries/signalProcessing/beamformer/PSTbeamformer_dp.peripheral.yaml?ref_type=heads>`_.
- `Packetiser <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-common/-/blob/main/libraries/Packetiser100G/Packetiser.peripheral.yaml?ref_type=heads>`_.


Firmware Resets
---------------

LFAADecode module has a series of registers to deal with the HBM interface. These do not apply to PST as of 1.0.0.

The 100G CMAC IP core provides a locked signal when the ALVEO is connected to another 100G device.
If locked des-asserts, this places the packetiser circuitry into a reset state.
The locked state can be queried through the system peripheral.

The U55 ALVEO kernel does not have a global reset signal.
Programming the kernel provides a reset, as long as the kernel is not already programmed.
Reseting the card can be accomplished using XBUTIL from Xilinx or by programming a different version or kernel into the card.
This will also restore the default values listed in the YAML files.


Simulating & Verifying Firmware
--------------------

Within firmware repo, there seperate project creation scripts for test benches.

These are located at:

- designs/pst/src/create_project_ct1_tb.tcl
- designs/pst/src/create_project_ct2_tb.tcl

There are also test benches are contained in the main project creation script.

Verification is achieved using scripts in:

- designs/pst/src/test_ct2_bf/*


Processing Modules
------------------

.. image:: images/pst-block-diagram.png


The processing steps are :

- Ingest from the LFAA. The LFAA ingest also includes statistics logging and the ability to generate a test data stream.
- Filterbank Corner Turn and coarse delay. To limit the overhead associated with initialising the filterbanks, a corner turn operation is required. The corner turn uses the HBM memory. The output of the corner turn is blocks samples about 60 ms long to be processed by the filterbanks.
- PST filterbank. 256 point FFT, keeping the central 216 channels, oversampled by 4/3.
- Fine delay. Applies a phase slope to the data.
- RFI Flagging. (STILL PENDING).
- Beamformer corner turn. This takes 60ms blocks of data from the filterbanks and outputs data with all stations for a small number of channels.
- PST Beamformer (16 beams) . The beamformer combines data from all the stations to form beams. This also includes Jones corrections.
   - Per station and per Beam Jones correction. (16 beams) * (512 stations) * (4 matrix elements) * (2 bytes/value) * (2 complex) * (2 coarse channels) = 262144 bytes = 64 BRAMs
   - Beamformer. Weights are derived from delay polynomials, with a linear approximation for the phase.
   - Per beam Jones correction
- PST packetiser. Packages data according to the ICD.

Below are diagrams showing these steps.

.. image:: images/PST_firmware_overview.JPG

PST Firmware Overview

.. image:: images/PST_beamformer_pipeline.JPG

PST Beamformer Pipeline structure per Alveo (U55)

.. image:: images/SPS_ingest_and_CT1.JPG

SPS ingest and Corner Turn 1

.. image:: images/filterbank.JPG

Filterbank

.. image:: images/CT2.JPG

Cornerturn 2 and Delay Polynomial calculation

.. image:: images/beamformer.JPG

Beamformer

.. image:: images/PSR_packetiser.JPG

PSR Packetiser
