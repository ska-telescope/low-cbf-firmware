----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08.11.2020 09:04:05
-- Module Name: PSTbeamformer - Behavioral
-- Description: 
--
--  PST beamformer Data flow:
--  ------------------------
--  
--                                          i_data -------> 2x2 Jones matrix multiplier  ---------->  phase rotation multiplier --> Accumulate across stations --> output packetiser
--   i_virtualChannel --> lookup Jones Matrix -> Jones --/                                       /
--                                                                                              /
--   i_virtualChannel --> phase lookup --------> time+frequency correction --> sin/cos lookup -
--
--
-- Packetiser Data Flow
-- --------------------
--
--   Notation : T = Time, runs from 0 to 31
--              S = Station, runs from 0 to however many stations there are (up to 511)
--              F = Fine channel, run from 0 to 47, which is two packets of fine channels
--
--   Data in :               < All data for 2 packets = 48 fine channels > < All data for 2 packets = next 48 fine channels> < etc..>
--                            <T=0                 > <T=1>  ...    <T=31>   <T=0> <T=1> ...                          <T=31>
--                             < S = 0  > <S=1>... 
--                              <F=0:47>
--                                                   
--   Write to tempWeightStore  ------------------------------------<+++>----------------------------------------------<+++>------------------------
--                                                                   |                                                  |
--                                                                   -divider-                                          -divider-
--                                                                           |                                                  |
--   Write to FinalWeightStore ------------------------------------------<++++++++++++>-------------------------------------<++++++++++++>---------
--   (finalWeightStore has space for 4 packets)
--
--   Write to outputBuffer ------------------------------------------------<+++++++++++++++++++>----------------------------<++++++++++++++++++>---
--   (Output buffer has space for 4 packets)
--
--   Packet Data output  -----------------------------------------------------------------------<++++++++++++++++++++++++++++++++++++++++++>----<+++++++++++++++ 
--
--  Registers
--  Each beamformer instance has 64kb of register address space, implemented in 2 URAMs.
--   Address 0-31767 = Jones Matrices memory.
--                     This is double buffered so that it can be updated.
--                     Each half of the buffer is 16 kbytes = (1024 matrices) * (16 bytes/matrix)
--                     Each matrix is 4 x (16+16) bit complex numbers.
--                     There is one matrix for each of the 1024 virtual channels.
--   Address 32768-65535 = Phase tracking and weights.
--                         Double buffered so it can be updated.
--                         Each half of the buffer is 16 kbytes = (1024 virtual channels) * (16 bytes each)
--                         The 16 bytes per virtual channel include:
--                          - 4 byte phase  (applied to both polarisations)
--                               * Phase is specified such that 2^31 = 180 degrees.
--                               * Phase is specified for the lowest frequency PST fine channel for this virtual channel.
--                          - 4 byte phase step per 3 PST fine channels.
--                          - 4 byte phase step per time.
--                          - 2 byte weight.
--                          - 2 bytes unused.
--
--  Note that the weight value is used to calculated the overall weight for a sample,
--  but is not used to actually weight the data; the weight must also be incorporated into the Jones matrices.
--
--  Data output order :
--   
-- 
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSTbeamformer is
    generic(
        g_BEAM_NUMBER : integer;
        -- Number of clock cycles to wait after a packet is ready to send before actually sending it on the BFData interface.
        -- This is to ensure there are no clashes on the BFdata bus between different beamformers.
        -- Up to 20 bits. It takes about 776 clocks to send a packet, so a 20 bit value enables sharing between up to 1048575/776 = 1351 beamformer instances. 
        g_PACKET_OFFSET : natural range 0 to 1048575
        -- 2 packets are prepared simultaneously, but we only send the second packet after all beamformer instances have sent the first packet. 
        -- So we have to wait a certain length of time before starting the second batch of packets.
        -- The second packet is sent (g_SECOND_PACKET_OFFSET + g_PACKET_OFFSET) clocks from when it is available. 
        --g_SECOND_PACKET_OFFSET : natural range 0 to 1048575
    );
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        -- Data in to the beamformer
        i_data    : in std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock. Must have a six cycle latency relative to the other inputs ("i_fine", "i_station", etc)
        i_flagged : in std_logic_vector(2 downto 0);
        i_fine    : in std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in i_data is (i_fine*3)
        i_coarse  : in std_logic_vector(9 downto 0);  -- Coarse channel index.
        i_firstStation : in std_logic;                -- Indicates that this is the first station in a group of stations. Used to reset the beam accumulator.
        i_lastStation  : in std_logic;                -- Indicates that this is the last station in a group of stations. Used to move the result of the accumulation to the output buffer.
        i_timeStep     : in std_logic_vector(4 downto 0);   -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
        i_virtualChannel : in std_logic_vector(9 downto 0); -- virtual channel.
        i_packetCount : in std_logic_vector(36 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        i_valid   : in std_logic;
        -- Data out to the next beamformer (pipelined version of the data in) (2 pipeline stages in this module)
        o_data    : out std_logic_vector(95 downto 0);  -- pipelined i_data 
        o_flagged : out std_logic_vector(2 downto 0);
        o_fine    : out std_logic_vector(7 downto 0);   -- pipelined i_fine
        o_coarse  : out std_logic_vector(9 downto 0);   -- pipelined i_coarse
        o_firstStation : out std_logic;                 -- pipelined i_firstStation
        o_lastStation  : out std_logic;                 -- pipelined i_lastStation
        o_timeStep     : out std_logic_vector(4 downto 0);   -- pipelined i_timeStep
        o_virtualChannel : out std_logic_vector(9 downto 0); --
        o_packetCount : out std_logic_vector(36 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        o_valid   : out std_logic;
        ------------------------------------------------------------------------
        -- Register interface (just address + data)
        i_regAddr : in std_logic_vector(23 downto 0); -- Byte address of the data (low two bits will always be "00")
        i_regData : in std_logic_vector(31 downto 0);
        i_WrEn    : in std_logic;
        -- Pipelined version of the register interface, for the next beamformer.
        o_regAddr : out std_logic_Vector(23 downto 0);
        o_regData : out std_logic_Vector(31 downto 0);
        o_wrEn    : out std_logic;
        -- register read data, 5 cycle latency from i_regAddr
        o_regReadData : out std_logic_vector(31 downto 0);
        -- Miscellaneous register settings:
        -- The phase information updates with every timestep, so i_phaseBuffer must be set at the correct time, i.e. corresponding to the time for the initial value of the phase in the registers. 
        i_jonesBuffer : in std_logic;  -- which of the two buffers to use for the Jones matrices
        i_phaseBuffer : in std_logic;  -- which of the two buffers to use for the phase
        i_beamsEnabled : in std_logic_vector(7 downto 0); -- number of beams enabled; This beamformer will output data packets if g_BEAM_NUMBER < i_beamsEnabled
        i_scale_exp_frac : in std_logic_vector(7 downto 0);
        -- pipelined outputs for buffer settings
        o_jonesBuffer : out std_logic;
        o_phaseBuffer : out std_logic;
        o_beamsEnabled : out std_logic_vector(7 downto 0);
        o_scale_exp_frac : out std_logic_vector(7 downto 0);
        ---------------------------------------------------------------------
        -- Packets of data from the previous beamformer, to be passed on to the next beamformer.
        i_BFdata        : in std_logic_vector(63 downto 0);
        i_BFBeam        : in std_logic_vector(7 downto 0);
        i_BFFreqIndex   : in std_logic_vector(10 downto 0);
        i_BFvalid       : in std_logic;
        -- Packets of data out to the 100G interface
        o_BFdata        : out std_logic_vector(63 downto 0);  -- PSR packetiser expects the first 16 bit value in bits 63:48, next in bits 47:32, etc.
        o_BFpacketCount : out std_logic_vector(36 downto 0);  -- Copy of i_packetCount, held for the duration of the output packets with the same timestamp. This is only to be used for the final beamformer in the daisy chain.
        o_BFFreqIndex   : out std_logic_vector(10 downto 0);  -- bits(3:0) index the fine channel, and count from 0 to 8 inclusive for the 9 different output packets per LFAA coarse channel, bits(11:4) index the coarse channel
        o_BFBeam        : out std_logic_vector(7 downto 0);  
        o_BFvalid       : out std_logic;
        i_badPacket     : in std_logic;  -- just used to trigger the debug core.
        o_badPacket     : out std_logic
    );
    
    -- prevent optimisation between adjacent instances of the PSTbeamformer.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of PSTbeamformer : entity is "yes";
    
end PSTbeamformer;

architecture Behavioral of PSTbeamformer is

    -- create_ip -name dds_compiler -vendor xilinx.com -library ip -version 6.0 -module_name sincosLookup
    -- set_property -dict [list CONFIG.Component_Name {sincosLookup} CONFIG.PartsPresent {SIN_COS_LUT_only} CONFIG.Noise_Shaping {None} CONFIG.Phase_Width {12} CONFIG.Output_Width {18} CONFIG.Amplitude_Mode {Unit_Circle} CONFIG.Parameter_Entry {Hardware_Parameters} CONFIG.Has_Phase_Out {false} CONFIG.DATA_Has_TLAST {Not_Required} CONFIG.S_PHASE_Has_TUSER {Not_Required} CONFIG.M_DATA_Has_TUSER {Not_Required} CONFIG.Latency {6} CONFIG.Output_Frequency1 {0} CONFIG.PINC1 {0}] [get_ips sincosLookup]
    --generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/sincosLookup/sincosLookup.xci]
    component sincosLookup
    port (
        aclk                : in std_logic;
        s_axis_phase_tvalid : in std_logic;
        s_axis_phase_tdata  : in std_logic_vector(15 downto 0);  -- Phase in bits 11:0
        m_axis_data_tvalid  : out std_logic;
        m_axis_data_tdata   : out std_logic_vector(47 downto 0)); -- cosine in bits 17:0, sine in bits 41:24, 6 clock latency
    end component;

    -- create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name BF_cMult27x18
    -- set_property -dict [list CONFIG.Component_Name {BF_cMult27x18} CONFIG.APortWidth {27} CONFIG.BPortWidth {18} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {46} CONFIG.MinimumLatency {4}] [get_ips BF_cMult27x18]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/BF_cMult27x18/BF_cMult27x18.xci]
    component BF_cMult27x18
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(95 DOWNTO 0));
    end component;
    
    signal jonesBuffer, phaseBuffer : std_logic;
    signal regAddr, regAddrDel1, regAddrDel2, regAddrDel3 : std_logic_vector(23 downto 0);
    signal regData : std_logic_vector(31 downto 0);
    signal regWrEn : std_logic;
    signal BFdata : std_logic_vector(63 downto 0);
    signal BFpacketCount : std_logic_vector(36 downto 0);
    signal BFbeam : std_logic_vector(7 downto 0);
    signal regModuleSelect : std_logic_vector(7 downto 0);
    signal jonesWrEn, phaseWrEn : std_logic;
    signal jonesRegReadData : std_logic_vector(31 downto 0);
    signal phaseRegReadData : std_logic_vector(31 downto 0);
    
    signal data    : std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock.
    signal fine    : std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
    signal coarse  : std_logic_vector(9 downto 0);  -- Coarse channel index.
    signal firstStation : std_logic;                -- First station.
    signal lastStation : std_logic;                 -- Last Station.
    signal timeStep    : std_logic_vector(4 downto 0);  -- time step within the output packet.
    signal virtualChannel : std_logic_vector(9 downto 0);
    signal packetCount : std_logic_vector(36 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
    signal valid   : std_logic;
    
    signal fineDel : t_slv_8_arr(31 downto 0); -- delay line for fine frequency channel
    signal timeStepDel : t_slv_5_arr(31 downto 0); -- delay line for the time step
    signal flaggedDel : t_slv_3_arr(31 downto 0);  -- delay line for RFI flagging. 
    signal firstStationDel : std_logic_vector(31 downto 0);
    signal lastStationDel : std_logic_vector(31 downto 0);
    signal validDel : std_logic_vector(31 downto 0);
    
    signal loadJones : std_logic := '0';
    signal jonesAddr, phaseAddr : std_logic_vector(14 downto 0);
    signal jonesRdData : std_logic_vector(63 downto 0);
    signal loadJonesDel1, loadJonesDel2, loadJonesDel3, loadJonesDel4, loadJonesDel5, loadJonesDel6 : std_logic := '0';
    signal JonesWord0Temp : std_logic_vector(63 downto 0);
    signal jonesWord0, jonesWord1 : std_logic_vector(63 downto 0);
    signal matrixMultR0real, matrixMultR1real, matrixMultR0imag, matrixMultR1imag : t_slv_28_arr(2 downto 0);
    signal phaseDel2 : std_logic_vector(15 downto 0);
    signal sinCos : std_logic_vector(47 downto 0);
    signal phaseRdData : std_logic_vector(63 downto 0);
    signal matrixMultPol0 : t_slv_64_arr(2 downto 0);
    signal matrixMultPol1 : t_slv_64_arr(2 downto 0);
    
    signal weightedPol0, weightedPol1 : t_slv_96_arr(2 downto 0);
    signal weightedPol0Real, weightedPol0Imag, weightedPol1Real, weightedPol1Imag : t_slv_32_arr(2 downto 0);
    signal weightedSumPol0Real, weightedSumPol1Real, weightedSumPol0Imag, weightedSumPol1Imag : t_slv_32_arr(2 downto 0);
    signal accDout, accDin : t_slv_128_arr(2 downto 0);
    signal AccWriteAddr : std_logic_vector(3 downto 0);
    signal AccReadAddr : std_logic_vector(3 downto 0);
    signal phaseBaseTemp, phaseStepFreqTemp, phaseStepFreqTemp_x48, phaseBase, phaseStepTime, phaseStepFreq, phaseStepFreqTemp_x16, phaseStepFreqTemp_x32, phaseStepFreqTemp_x64, phaseDel1 : std_logic_vector(31 downto 0);
    signal phaseWeight : std_logic_vector(15 downto 0);
    signal loadPhase : std_logic_vector(1 downto 0);
    
    signal accumulatorPol0Real, accumulatorPol0Imag, accumulatorPol1Real, accumulatorPol1Imag : t_slv_32_arr(2 downto 0);
    signal lastFineGroup : std_logic := '0';
    signal phaseWeightDel1 : std_logic_vector(31 downto 0);
    signal flagged : std_logic_vector(2 downto 0);
    signal weightSum : t_slv_32_arr(2 downto 0);
    signal weightAccDout : t_slv_32_arr(2 downto 0);
    signal weightAccReadAddr : std_logic_vector(3 downto 0);
    signal weightAccWriteAddr : std_logic_vector(3 downto 0);
    signal weightAccWrEn : std_logic_vector(0 downto 0);
    
    signal maxWeightSum : t_slv_32_arr(2 downto 0);
    signal maxWeightAccDout : t_slv_32_arr(2 downto 0);
    signal tempWeightStoreDin, tempWeightStoreDout : t_slv_64_arr(2 downto 0);
    signal tempWeightStoreWrEn : std_logic_vector(0 downto 0);
    signal weightsCountUp, weightsCountDown : std_logic_vector(8 downto 0);
    signal div32ValidIn : std_logic;
    signal dividerRunning, get48Weights, get24Weights : std_logic;
    
    signal packetBufPol0Din, packetBufPol1Din : t_slv_64_arr(2 downto 0);
    signal packetBufPol0Dout, packetBufPol1Dout : t_slv_64_arr(2 downto 0);
    signal packetBufWrAddr : std_logic_vector(8 downto 0);
    signal packetBufWrEn : t_slv_1_arr(2 downto 0);
    signal AccWrEn : std_logic_vector(0 downto 0);
    signal absPol0Sum : t_slv_32_arr(2 downto 0);
    signal absPol1Sum : t_slv_32_arr(2 downto 0);
    signal firstAbsSum : std_logic := '0';
    signal absSumAll : std_logic_vector(31 downto 0);
    signal absSum : t_slv_32_arr(2 downto 0);
    signal absSumAllExt : std_logic_vector(43 downto 0);
    signal packet0AbsSum, packet1AbsSum : std_logic_vector(43 downto 0);
    signal getScalingInputValid, packet0AbsSumValid, packet1AbsSumValid : std_logic;
    signal getScalingInput : std_logic_vector(43 downto 0);
 
    signal packet0AbsSumValidDel2, packet0AbsSumValidDel1, packet0AbsSumValidDel3, packet0AbsSumValidDel4 : std_logic;
    signal packet1AbsSumValidDel2, packet1AbsSumValidDel1, packet1AbsSumValidDel3, packet1AbsSumValidDel4 : std_logic;
    signal packet0AbsSumValidOnlyDel1, packet0AbsSumValidOnly, packet0AbsSumValidOnlyDel2, packet0AbsSumValidOnlyDel3, packet0AbsSumValidOnlyDel4 : std_logic;
    
    signal output32bitFine0Time0real, output32bitFine0Time0Imag, output32bitFine1Time0real : std_logic_vector(31 downto 0);
    signal output32bitFine1Time0Imag, output32bitFine2Time0real, output32bitFine2Time0Imag : std_logic_vector(31 downto 0);    
    signal output32bitFine0Time1real, output32bitFine0Time1Imag, output32bitFine1Time1real : std_logic_vector(31 downto 0);
    signal output32bitFine1Time1Imag, output32bitFine2Time1real, output32bitFine2Time1Imag : std_logic_vector(31 downto 0);
    signal output32bitFine2Time0RealDel1, output32bitFine2Time0ImagDel1 : std_logic_vector(31 downto 0);
    signal sample32bit0, sample32bit1, sample32bit2, sample32bit3 : std_logic_vector(31 downto 0);
 
    signal pol0re : std_logic_vector(31 downto 0);
    signal pol0im : std_logic_vector(31 downto 0);
    signal pol1re : std_logic_vector(31 downto 0);
    signal pol1im : std_logic_vector(31 downto 0);
    signal outputBufDin : std_logic_vector(63 downto 0);
    signal scalingShift : std_logic_vector(3 downto 0);
    signal scalingMantissa : std_logic_vector(3 downto 0);
    
    signal packet0Shift : std_logic_vector(3 downto 0);
    signal packet0Mantissa : std_logic_vector(3 downto 0);
    signal packet0Only : std_logic := '0';
    signal packet0Waiting : std_logic := '0';
    signal packet1Shift : std_logic_vector(3 downto 0);
    signal packet1Mantissa : std_logic_vector(3 downto 0);
    signal packetBufRdAddr : std_logic_vector(8 downto 0);
    
    signal outputFine : std_logic_vector(3 downto 0);
    signal outputTime : std_logic_vector(4 downto 0);
    type output_fsm_type is (idle, pol0read0, pol0read1, pol0wait, pol1read0, pol1read1, pol1wait, done);
    signal output_fsm : output_fsm_type := idle;
    signal output_fsmDel1, output_fsmDel2, output_fsmDel3, output_fsmDel4 : output_fsm_type := idle;
    signal outputPacket0Only : std_logic := '0';
    signal shiftFinal, scaleFinal : std_logic_vector(3 downto 0);
    
    signal outputFineDel1, outputFineDel2, outputFineDel3, outputFineDel4 : std_logic_vector(3 downto 0);
    signal sample16bit0, sample16bit1, sample16bit2, sample16bit3 : std_logic_vector(15 downto 0);
    signal finalWeightDin : std_logic_vector(63 downto 0);
    signal weight16bit : t_slv_16_arr(2 downto 0);
    signal finalWeightOverflow : std_logic_vector(31 downto 0);
    signal div32ValidOut : std_logic_vector(2 downto 0);
    signal finalWeightWrEn : std_logic_vector(0 downto 0);
    signal finalWeightPhase : std_logic_vector(3 downto 0);
    signal finalWeightsSOF, finalWeightsSOFDel1 : std_logic;
    signal finalWeightWrAddr : std_logic_vector(4 downto 0);

    signal waitCount2Done : std_logic := '0';
    signal finalWeightRdAddr : std_logic_vector(4 downto 0);
    signal outputBufRdAddr : std_logic_vector(11 downto 0);
    type readout_fsm_type is (start, waitOffset, sendHeaderWord, sendWeights, sendData, waitSecondPacket, waitWholeFrameDone, done);
    signal readout_fsm : readout_fsm_type := done;
    signal readout_waitCount2 : std_logic_vector(19 downto 0);
    signal readout_waitCount : std_logic_vector(19 downto 0);
    
    signal outputSelectData, outputSelectWeights : std_logic;
    signal outputSelectDataDel1, outputSelectWeightsDel1 : std_logic;
    signal outputBufDout, finalWeightDout : std_logic_vector(63 downto 0);
    signal BFValid : std_logic;
    
    signal outputFineDel3ext : std_logic_vector(4 downto 0);
    signal outputFineDel3ext_x2 : std_logic_vector(4 downto 0);
    signal outputFineDel4_x3 : std_logic_vector(4 downto 0);
    signal outputTimeDel1, outputTimeDel2, outputTimeDel3, outputTimeDel4 : std_logic_vector(4 downto 0);
    signal outputBufTime : std_logic_vector(3 downto 0);
    signal outputBufPol : std_logic;
    signal outputBufFinePacket : std_logic;
    signal outputBufFineChannel : std_logic_vector(4 downto 0);
    signal outputBufWrAddr, outputBufWrAddrAdv1, outputBufWrAddrAdv2, outputBufWrAddrAdv3, outputBufWrAddrAdv4 : std_logic_vector(11 downto 0);
    
    signal outputBufWrEnAdv9, outputBufWrEnAdv8, outputBufWrEnAdv7, outputBufWrEnAdv6, outputBufWrEnAdv5, outputBufWrEnAdv4 : std_logic;
    signal outputBufWrEnAdv3, outputBufWrEnAdv2, outputBufWrEnAdv1 : std_logic;
    signal outputBufWrEn : std_logic_vector(0 downto 0);
    
    signal outputBuffer : std_logic := '1';
    signal triggerSendPacket : std_logic;
    signal triggerSendOnePacketOnly : std_logic := '0';
    signal readoutBufferSelect : std_logic;
    signal readoutOnePacketOnly : std_logic;
    
    signal shiftFinalExt : std_logic_vector(7 downto 0);
    signal fp32Exp : std_logic_vector(7 downto 0);
    signal fp32Frac : std_logic_vector(3 downto 0);
    signal buf0Packet0Scale, buf0Packet1Scale, buf1Packet0Scale, buf1Packet1Scale : std_logic_vector(12 downto 0); -- high 13 bits of the fp32 scale factor (rest of the bits are zeros).
    signal headerWord : std_logic_vector(12 downto 0);
    signal outputSelectHeaderWordDel1, outputSelectHeaderWord : std_logic;
    signal outputBufferPacketCount, BFpacketCountHold : std_logic_vector(36 downto 0) := (others => '0');
    signal outputBufferVirtualChannel, BFvirtualChannelHold, BFvirtualChannel : std_logic_vector(9 downto 0);
    signal phaseDel1_16bit : std_logic_vector(15 downto 0);
    signal loadDel3 : std_logic_vector(1 downto 0);
    signal phaseStepFreqTemp_x : std_logic_vector(31 downto 0);
    
    signal weightedSumPol0RealAbs, weightedSumPol0ImagAbs, weightedSumPol1RealAbs, weightedSumPol1ImagAbs : t_slv_32_arr(2 downto 0);
    signal absSum01 : std_logic_vector(31 downto 0);
    signal absSum2 : std_logic_vector(31 downto 0);
    signal phaseBaseDel1, phaseBaseDel1Mod, phaseBaseUpdated : std_logic_vector(31 downto 0);
    signal sel_neg32, loadJonesDel7 : std_logic;
    signal beamsEnabled : std_logic_vector(7 downto 0);
    signal beamEnabled : std_logic;
    signal lastStationAndValidDel16 : std_logic;
    signal BFFreqIndex : std_logic_vector(10 downto 0);
    signal BFCoarse, BFCoarseDel, BFCoarseHold, outputBufferCoarse : std_logic_vector(9 downto 0);
    signal BFFine, BFFineDel, BFFineHold, outputBufferFine : std_logic_vector(3 downto 0);
    signal lastStationDel1 : std_logic;
    signal beamsEnabled_x512, beamsEnabled_x256, beamsEnabled_x32, beamsEnabled_x768, beamsEnabled_x32_plus100, secondPacketOffset : std_logic_vector(15 downto 0);
    signal wholeFrameDone : std_logic := '0';
    signal wholeFrameWaitCount : std_logic_vector(19 downto 0);
    signal triggerSendPacketHold : std_logic := '0';
    signal triggerSendOnePacketOnlyHold : std_logic := '0';
    signal outputBufferHold : std_logic := '0';

    signal dreadout_waitCount2 : std_logic_vector(19 downto 0);
    signal dwholeFrameWaitCount : std_logic_vector(19 downto 0);
    signal dreadout_fsm : std_logic_vector(3 downto 0);
    signal zeros : std_logic_vector(63 downto 0);
    signal dTriggerSendPacket, dTriggerSendOnePacketOnly, dTriggerSendPacketHold, dOutputBufferHold, dTriggerSendOnePacketOnlyHold : std_logic;
    signal dbadPacket : std_logic;
    signal badPacket : std_logic;
    signal scale_exp_frac : std_logic_vector(7 downto 0);
    
    component ila_2
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(63 downto 0)); 
    end component;
    
    component ila_beamData
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(119 downto 0)); 
    end component;
    
begin
    
    -- Pipelines that go through to the next beamformer
    
    regModuleSelect <= i_regAddr(23 downto 16);  -- Each beamformer has 64kbytes of address space; register writes are for this module if regModuleSelect = g_BEAM_NUMBER
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- Buffer settings
            jonesBuffer <= i_jonesBuffer;
            phaseBuffer <= i_phaseBuffer;
            beamsEnabled <= i_beamsEnabled;
            scale_exp_frac <= i_scale_exp_frac;
            
            o_jonesBuffer <= jonesBuffer;
            o_phaseBuffer <= phaseBuffer;
            o_beamsEnabled <= beamsEnabled;
            o_scale_exp_frac <= scale_exp_frac;
            
            badPacket <= i_badPacket;
            o_badPacket <= badPacket;
            
            if (g_BEAM_NUMBER < unsigned(beamsEnabled)) then
                beamEnabled <= '1';
            else
                beamEnabled <= '0';
            end if; 
            
            -- Register writes
            regAddr <= i_regAddr;
            regData <= i_regData;
            regWrEn <= i_wrEn;
            
            if (unsigned(regModuleSelect) = g_BEAM_NUMBER) and i_regAddr(15) = '0' and i_wrEn = '1' then
                jonesWrEn <= '1';
            else
                jonesWrEn <= '0';
            end if;
            
            if (unsigned(regModuleSelect) = g_BEAM_NUMBER) and i_regAddr(15) = '1' and i_wrEn = '1' then
                phaseWrEn <= '1';
            else
                phaseWrEn <= '0';
            end if;
            
            -- Output registers to pipeline the register commands to the next module.
            o_regAddr <= regAddr;
            o_regData <= regData;
            o_wrEn <= regWrEn;
            
            -- Output registers to select read data from the memory
            regAddrDel1 <= regAddr;
            regAddrDel2 <= regAddrDel1;
            regAddrDel3 <= regAddrDel2;
            
            if regAddrDel3(15) = '0' then
                o_regReadData <= jonesRegReadData;
            else 
                o_regReadData <= phaseRegReadData;
            end if;
            
            -- Pipeline the input data
            data <= i_data;               -- (95:0); 3 consecutive fine channels delivered every clock.
            flagged <= i_flagged;         -- (2:0);
            fine <= i_fine;               -- (7:0); fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
            coarse <= i_coarse;           -- (9:0);
            firstStation <= i_firstStation; -- first Station in the burst.
            lastStation <= i_lastStation;   -- last station in the burst.
            timeStep <= i_timeStep;         -- (4:0); Time step within the output packet.
            virtualChannel <= i_virtualChannel; -- (9:0)
            packetCount <= i_packetCount;       -- (36;0); The packet count for this packet, based on the original packet count from LFAA.
            valid <= i_valid;
            
            o_data <= data;
            o_flagged <= flagged;
            o_fine <= fine;  
            o_coarse <= coarse;
            o_firstStation <= firstStation;
            o_lastStation <= lastStation;
            o_timeStep <= timeStep;
            o_virtualChannel <= virtualChannel;
            o_packetCount <= packetCount;
            o_valid <= valid;
            
            -- Capture packetCount and virtualChannel and delay them so the are aligned with the output packets. 
            if unsigned(fine) = 15 and firstStation = '1' and unsigned(timeStep) = 0 and valid = '1' then
                BFpacketCount <= packetCount;  -- capture part way through the first packet, so we don't overwrite it before it is used.
                BFvirtualChannel <= virtualChannel;
            end if;
            
            if valid = '1' and fine(3 downto 0) = "0000" and firstStation = '1' then
                BFCoarse <= coarse;
                BFFine <= fine(7 downto 4); -- high 4 bits of fine is the group;
            end if;
            
            lastStationDel1 <= lastStation;
            if lastStation = '1' and lastStationDel1 = '0' then
                BFCoarseDel <= BFCoarse;
                BFFineDel <= BFFine;
            end if;
            
            if (get48Weights = '1' or get24Weights = '1') then
                BFpacketCountHold <= BFpacketCount;
                BFvirtualChannelHold <= BFvirtualChannel;
                BFCoarseHold <= BFCoarseDel;
                BFFineHold <= BFFineDel;
            end if;
            
            if (readout_fsm = start) then
                outputBufferPacketCount <= BFpacketCountHold;
                outputBufferVirtualChannel <= BFvirtualChannelHold;
                outputBufferCoarse <= BFCoarseHold;
                outputBufferFine <= BFFineHold(2 downto 0) & '0';
            elsif readout_fsm = waitSecondPacket and waitCount2Done = '1' then
                outputBufferFine <= std_logic_vector(unsigned(outputBufferFine) + 1);
            end if;
            
            --  Packets of beamformed data
            if (outputSelectHeaderWordDel1 = '1' and beamEnabled = '1') then
                BFdata(63 downto 54) <= outputBufferVirtualChannel;
                BFdata(53 downto 32) <= (others => '0');
                
                BFdata(31 downto 19) <= headerword; -- header word with the scaling factor
                BFdata(18 downto 0) <= (others => '0');
                
                BFbeam <= std_logic_vector(to_unsigned(g_BEAM_NUMBER,8));
                BFFreqIndex <= outputBufferCoarse(6 downto 0) & outputBufferFine;
                BFValid <= '1';
            elsif outputSelectWeightsDel1 = '1' and beamEnabled = '1' then
                BFdata <= finalWeightDout(15 downto 0) & finalWeightDout(31 downto 16) & finalWeightDout(47 downto 32) & finalWeightDout(63 downto 48);
                BFbeam <= std_logic_vector(to_unsigned(g_BEAM_NUMBER,8));
                BFFreqIndex <= outputBufferCoarse(6 downto 0) & outputBufferFine;
                BFValid <= '1';
            elsif outputSelectDataDel1 = '1' and beamEnabled = '1' then
                BFdata <= outputBufDout(15 downto 0) & outputBufDout(31 downto 16) & outputBufDout(47 downto 32) & outputBufDout(63 downto 48);
                BFbeam <= std_logic_vector(to_unsigned(g_BEAM_NUMBER,8));
                BFFreqIndex <= outputBufferCoarse(6 downto 0) & outputBufferFine;
                BFValid <= '1';
            else
                BFdata <= i_BFdata;
                BFbeam <= i_BFbeam;
                BFFreqIndex <= i_BFFreqIndex;
                BFValid <= i_BFvalid;
            end if;
            o_BFdata <= BFdata;
            o_BFpacketCount <= outputBufferPacketCount;
            o_BFFreqIndex <= BFFreqIndex; -- outputBufferFreqIndex;  -- outputBufferVirtualChannel;
            o_BFbeam <= BFbeam;
            o_BFValid <= BFValid;
        end if;
    end process; 
    
    
    --------------------------------------------------------------
    -- Registers :
    --  The beamformer has the following register map :
    --    - Jones Matrices (= Address 0-32767)
    --       - 1024 channels (e.g. 512 stations x 2 coarse)
    --       - 2x2 Jones Matrix
    --       - 16+16bit values
    --       - (1024 matrices) * (4 complex numbers per matrix) * (4 bytes per number) * (2 <double buffered>) = 32768 bytes = 1 URAM
    --       - 128 bits per Jones matrix = 2 memory locations, which are fixed by the virtual channel
    --    - Phase  (= Address 32768-65535)
    --       - 1024 channels
    --       - 32 bit phase for polarisation 0, + 32 bit phase for polarisation 1. This will need to be updated to track over time + frequency. 
    --       - (1024 numbers) * (4+4 bytes) * (2 <double buffered>) = 16384 bytes (use 1 URAM, with half spare)
    --  So with 64kbytes of memory used, we have 16 bits to address within the module, and the remaining address bits to select 
    --  which beamformer to use.
    --
    
    jonesmem : entity bf_lib.PSTBF_uram_wrapper
    port map(
        i_clk   => i_BF_clk, -- in std_logic;
        -- Port A. Both reads and writes are 32 bits wide.
        i_addrA => regAddr(14 downto 0), -- in(14:0); -- byte address
        i_dataA => regData,              -- in(31:0); -- 
        o_dataA => jonesRegReadData,    -- out(31:0); -- 3 cycle latency
        i_wrEnA => jonesWrEn,            -- in std_logic;
        -- Port B. Reads are 64 bits wide, write are 32 bits wide.
        i_addrB => jonesAddr,        -- in(14:0); -- byte address
        i_dataB => (others => '0'),  -- in(31:0); -- 
        o_dataB => jonesRdData,      -- out(63:0); -- 3 cycle latency
        i_wrEnB => '0'               -- in std_logic
    );
    
    
    phasemem : entity bf_lib.PSTBF_uram_wrapper
    port map(
        i_clk   => i_BF_clk, -- in std_logic;
        -- Port A. Both reads and writes are 32 bits wide.
        i_addrA => regAddr(14 downto 0), -- in(14:0); -- byte address
        i_dataA => regData,              -- in(31:0); -- 
        o_dataA => phaseRegReadData,     -- out(31:0); -- 3 cycle latency
        i_wrEnA => phaseWrEn,            -- in std_logic;
        -- Port B. Reads are 64 bits wide, write are 32 bits wide.
        i_addrB => phaseAddr,        -- in(14:0); -- byte address
        i_dataB => phaseBaseUpdated,        -- in(31:0); -- 
        o_dataB => phaseRdData,      -- out(63:0); -- 3 cycle latency
        i_wrEnB => loadJonesDel7     -- in std_logic
    );
    
    --------------------------------------------------------------------------------------------
    --
    -- Processing pipeline
    --  Jones correction -> phase correction -> sum -> output packet memory.
    -- 
    
    process(i_BF_clk)
        variable phaseStepFreqTemp_v_x16 : std_logic_vector(31 downto 0);
        variable phaseStepFreqTemp_v_x32 : std_logic_vector(31 downto 0);
        variable phaseStepTime_x32_v : std_logic_vector(31 downto 0);
    begin
        if rising_edge(i_BF_clk) then
            -- Use the virtual channel to look up the required matrix in the Jones matrix memory.
            -- Detect when we need a new jones matrix, and copy Jones data into registers to use in the matrix multiplier.
            -- Note the processing order for data coming in is:
            --   
            --    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 60 ms corner turn, this is 0:(288/32-1) = 0:8
            --       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
            --          For FineGroup = 0:4                                    -- Process 4 groups of fine channels (0:47, 48:95, 96:143, 144:191, 191:215)
            --             For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
            --                For Station = 0:(i_stations-1)
            --                   For fine_offset = 0:3:45  (or 0:3:21 for the last group of fine channels, where fine_channel_start = 192)
            --                      Process a 96 bit word (i.e. 3 fine channels).
            --                   *Update Phase memory for next time step:
            --                     -32 timesteps if this is the last of 32 timesteps, but not the last finegroup, otherwise +1 timesteps
            --                *On the last time step (Time = 31), write the weights to the tempWeightStore memory.     
            --             Output packet(s) complete, send the packet(s) (two packets for fineGroup 0,1,2,3, one packet for fineGroup = 4)
            --
            -- 
            -- So we get bursts of either 16 or 8 x 96 bit words for the same station and coarse channel
            if ((unsigned(fine) = 0 or unsigned(fine) = 16 or unsigned(fine) = 32 or unsigned(fine) = 48 or unsigned(fine) = 64) and valid = '1') then
                -- This is the start of a new group of 8 or 16 x 96 bit words, so need to get the Jones Matrix for this station/coarse channel
                loadJones <= '1';
                jonesAddr <= jonesBuffer & virtualChannel & '0' & "000"; 
                -- Get the phase.
                -- Assumes the phase is the same for groups of 3 fine channels 
                -- The frequency correction is small, worst case is (1) * 3.6 kHz = 3.6 kHz on a 50 MHz channel = about 1 in 16000. 
                phaseAddr <= phaseBuffer & virtualChannel & '0' & "000";  -- phaseAddr aligns with JonesAddr
            elsif loadJones = '1' then
                jonesAddr(3) <= '1';
                phaseAddr(3) <= '1';
                loadJones <= '0';
            else
                loadJones <= '0';
                phaseAddr(3) <= '0';  -- back to the address of phaseBase, for the writeback of the updated value, which occurs on loadJonesDel7
            end if;
            
            loadJonesDel1 <= loadJones;
            loadJonesDel2 <= loadJonesDel1;
            loadJonesDel3 <= loadJonesDel2;
            loadJonesDel4 <= loadJonesDel3;
            loadJonesDel5 <= loadJonesDel4;
            loadJonesDel6 <= loadJonesDel5;
            loadJonesDel7 <= loadJonesDel6;
            
            if loadJonesDel3 = '1' then
                JonesWord0Temp <= jonesRdData;
            end if;
            if loadJonesDel4 = '1' then
                JonesWord0 <= jonesWord0Temp;
                JonesWord1 <= jonesRdData;  -- JonesWord1 has a 6 cycle latency relative to "fine", "station", "coarse", "virtualChannel" etc.
            end if;
            
            -- Data returned from memory in phaseRdData will align with JonesRdData
            -- So the 12 bit phaseDel1 will align with JonesWord0, JonesWord1
            -- 6 cycles latency in the sincos lookup, so "sincos" aligns with "matrixMultPol0"
            --
            -- There is also an update process going on to track the phase across frequency channel and across time.
            -- We have at least 8 clocks between loading new data from the memory, since it takes a minimum of 8 clocks to deliver the final burst of 24 fine channels.
            -- For the burst of data where fineGroup = 0, 1, 2, or 3 :
            --    phaseBase <= phaseBase - 32 when timestep = 31 else 
            --                 phaseBase + phaseStepTime; 
            -- For the burst of data where fineGroup = 4 (i.e. fine = 64)
            --    phaseBase <= phaseBase + phaseStepTime
            
            if (unsigned(fineDel(3)) = 0 or unsigned(fineDel(3)) = 16 or unsigned(fineDel(3)) = 32 or unsigned(fineDel(3)) = 48) then
                lastFineGroup <= '0';
            else
                lastFineGroup <= '1';
            end if;
            
            if (validDel(2) = '1' and (unsigned(fineDel(2)) = 0)) then
                loadDel3 <= "00";
            elsif (validDel(2) = '1' and unsigned(fineDel(2)) = 16) then
                loadDel3 <= "01";
            elsif (validDel(2) = '1' and unsigned(fineDel(2)) = 32) then
                loadDel3 <= "10";
            else
                loadDel3 <= "11";
            end if;
            
            if loadJonesDel3 = '1' then  -- first word of phase data back from the memory 
                phaseBaseTemp <= phaseRdData(31 downto 0);
                phaseStepFreqTemp <= phaseRdData(63 downto 32);
                phaseStepFreqTemp_v_x16 := phaseRdData(59 downto 32) & "0000";
                phaseStepFreqTemp_v_x32 := phaseRdData(58 downto 32) & "00000";
                phaseStepFreqTemp_x48 <= std_logic_vector(unsigned(phaseStepFreqTemp_v_x16) + unsigned(phaseStepFreqTemp_v_x32));
                if (loadDel3 = "00") then  -- convoluted to improve timing. Takes the phase base and adds the frequency offset
                    phaseStepFreqTemp_x <= (others => '0');
                elsif (loadDel3 = "01") then
                    phaseStepFreqTemp_x <= phaseRdData(59 downto 32) & "0000";
                elsif (loadDel3 = "10") then
                    phaseStepFreqTemp_x <= phaseRdData(58 downto 32) & "00000";
                else -- if (x64) then
                    phaseStepFreqTemp_x <= phaseRdData(57 downto 32) & "000000";
                end if;
            elsif loadJonesDel4 = '1' then -- second word of phase data back from the memory, one clock after the first word.
                phaseBase <= phaseBaseTemp;
                phaseStepTime <= phaseRdData(31 downto 0);
                phaseWeight <= phaseRdData(47 downto 32);
                phaseStepFreq <= phaseStepFreqTemp;
            -- Done below with an extra pipeline stage.
--            elsif loadJonesDel5 = '1' then
--                if (timeStepDel(5) = "11111" and lastFineGroup = '0') then
--                    phaseStepTime_x32_v := phaseStepTime(26 downto 0) & "00000";
--                    phaseBase <= std_logic_vector(signed(phaseBase) - signed(phaseStepTime_x32_v));
--                else
--                    phaseBase <= std_logic_vector(signed(phaseBase) + signed(phaseStepTime));
--                end if;
            end if;
            
            if (timeStepDel(4) = "11111" and lastFineGroup = '0') then
                sel_neg32 <= '1';
            else
                sel_neg32 <= '0';
            end if;
            
            if loadJonesDel5 = '1' then
                phaseBaseDel1 <= phaseBase;
                if sel_neg32 = '1' then
                    phaseStepTime_x32_v := phaseStepTime(26 downto 0) & "00000";
                    phaseBaseDel1Mod <= std_logic_vector(-signed(phaseStepTime_x32_v));
                else
                    phaseBaseDel1Mod <= phaseStepTime;
                end if;
            end if;
            
            if loadJonesDel6 = '1' then
                phaseBaseUpdated <= std_logic_vector(signed(phaseBaseDel1) + signed(phaseBaseDel1Mod));
            end if;
            
            phaseWeightDel1 <= x"0000" & phaseWeight;  -- Delay 1 clock so it aligns with "flagged" signal, and extend to 32 bits. We need enough bits for 32 times and up to 512 stations.
            
            if (validDel(3) = '1' and (unsigned(fineDel(3)) = 0 or unsigned(fineDel(3)) = 16 or unsigned(fineDel(3)) = 32 or unsigned(fineDel(3)) = 64)) then
                loadPhase <= "00";
            elsif (validDel(3) = '1' and unsigned(fineDel(3)) = 48) then
                loadPhase <= "01";
            elsif validDel(3) = '1' then
                loadPhase <= "10";
            else
                loadPhase <= "11";
            end if;
            
            if (loadPhase = "00") then
                phaseDel1 <= std_logic_vector(unsigned(phaseBaseTemp) + unsigned(phaseStepFreqTemp_x));
            elsif (loadPhase = "01") then
                phaseDel1 <= std_logic_vector(unsigned(phaseBaseTemp) + unsigned(phaseStepFreqTemp_x48));
            elsif (loadPhase = "10") then
                phaseDel1 <= std_logic_vector(unsigned(phaseDel1) + unsigned(phaseStepFreq));
            end if;
            
            ----------------------------------------------------------------------------------
            -- Accumulate across all the different stations
            --  Delay from data input:
            -- count   Delayed signal Sample signal                                          Comment
            -- -----   -------------- -------------                                          -------
            --   0     fine,valid
            --   1     fineDel(0)     loadJones, phaseAddr, jonesAddr
            --   2     fineDel(1)     loadJonesDel1
            --   3     fineDel(2)     loadJonesDel2
            --   4     fineDel(3)     loadJonesDel3
            --   5     fineDel(4)     loadJonesDel4, jonesWord0Temp, phaseBaseTemp         
            --   6     fineDel(5)     JonesWord0, JonesWord1, data, phaseDel1, phaseWeight   Input to the matrix multiplier. Note i_data has a 6 clock latency relative to the other inputs such as i_fine. 
            --   7     fineDel(6)     flagged(2:0) 
            --   ...                  
            --   11    fineDel(10)    matrixMultR0real                                       Output from the matrix multiplier (5 clock latency from "data")
            --   12    fineDel(11)    matrixMultPol0, sincos                                 6 clock latency to get sin+cos from "phaseDel1"
            --   ...
            --   15    fineDel(14)    accReadAddr                                            read address into the accumulator memory, so 
            --   16    fineDel(15)    weightedPol0Real                                       output from the phase multiplier (4 clock latency from "matrixMultPol0")
            --   17    fineDel(16)    weightedSumPol0Real                                    accumulated beam
            --
            
            fineDel(0) <= fine;
            fineDel(31 downto 1) <= fineDel(30 downto 0);
            
            timeStepDel(0) <= timeStep;
            timeStepDel(31 downto 1) <= timeStepDel(30 downto 0);
            
            firstStationDel(0) <= firstStation;
            firstStationDel(31 downto 1) <= firstStationDel(30 downto 0);
            
            lastStationDel(0) <= lastStation;
            lastStationDel(31 downto 1) <= lastStationDel(30 downto 0);
            
            validDel(0) <= valid;
            validDel(31 downto 1) <= validDel(30 downto 0);
            
            flaggedDel(0) <= flagged;
            flaggedDel(31 downto 1) <= flaggedDel(30 downto 0);
            
            AccReadAddr <= fineDel(13)(3 downto 0);  -- we operate on groups of 48 fine channels, 3 at a time, so fine counts from 0 to 15 within a group of fine channels.
            
            -- Accumulate weights. A separate weight is calculated for each fine channel.
            weightAccReadAddr <= fineDel(4)(3 downto 0);   -- So weights read address is valid on (5), read data ("weightAccDout") is valid on (6), and weightSum valid on fineDel(7)
            weightAccWriteAddr <= fineDel(6)(3 downto 0);
            weightAccWrEn(0) <= validDel(6);
            
            for i in 0 to 2 loop
                if validDel(6) = '1' then
                    if (firstStationDel(6) = '1' and timeStepDel(6) = "00000") then
                        if flaggedDel(6)(i) = '1' then
                            weightSum(i) <= (others => '0');
                        else
                            weightSum(i) <= phaseWeightDel1;
                        end if;
                    elsif flaggedDel(6)(i) = '0' then
                        weightSum(i) <= std_logic_vector(unsigned(weightAccDout(i)) + unsigned(phaseWeightDel1));
                    else
                        weightSum(i) <= weightAccDout(i);
                    end if;
                end if;
            end loop;
            
            -- Accumulate the maximum possible weights. This is used to scale the weight at the end of the packet.
            for i in 0 to 2 loop
                if validDel(6) = '1' then
                    if (firstStationDel(6) = '1' and timeStepDel(6) = "00000") then
                        maxWeightSum(i) <= phaseWeightDel1;
                    elsif flaggedDel(6)(i) = '0' then
                        maxWeightSum(i) <= std_logic_vector(unsigned(maxWeightAccDout(i)) + unsigned(phaseWeightDel1));
                    else
                        maxWeightSum(i) <= std_logic_vector(maxWeightAccDout(i));
                    end if;
                end if;
            end loop;
            
            -- For the last time sample, send the weightSum and maxWeightSum to a divider to calculate the weight that goes in the packet.
            -- (Space for 4 packets in the output buffer, so we need space for 4 sets of weights, each of which is 24x 16 bit values).
            --  - On the last time sample, write weights and max weights to another distributed memory
            --  - Then trigger a state machine that reads each entry in the memory and does the division to get the 16 bit weight required
            --     The division is serial, with 3 parallel instances, each of which needs 16 clocks per division.
            --     So at the end of a block of 48 fine channels, it will take 48 * 16/3 = 256 clocks to calculate the output weights.
            --     At the end of the final block (which only has 24 fine channels), it will take 128 clocks to calculate the output weights.
            --     Note : The time required for data to come in for the shortest possible frame is (8 clocks) * (N stations) * (32 times) = 256 * N,
            --            so we always finish the division operation before the next frame appears.
            --  - Which then write the final 16-bit weights to another memory which is 64 bits wide, by 32 deep.
            --    The final memory has :
            --     Address 0-5 = packet buffer 0 weights, with 4 weights (=8 bytes) in each entry. Note 6 * 4 = 24 weights per packet buffer.
            --     Address 8-13 = packet buffer 1 weights.
            --     Address 16-21 = packet buffer 2 weights.
            --     Address 24-29 = packet buffer 3 weights.
            if (validDel(6) = '1' and (timeStepDel(6) = "11111") and lastStationDel(6) = '1') then
                tempWeightStoreWrEn(0) <= '1';
            else
                tempWeightStoreWrEn(0) <= '0';
            end if;
            
            if validDel(6) = '1' and timeStepDel(6) = "11111" and lastStationDel(6) = '1' and (unsigned(fineDel(6)) = 15 or unsigned(fineDel(6)) = 31 or unsigned(fineDel(6)) = 47 or unsigned(fineDel(6)) = 63) then
                get48Weights <= '1';  -- All weight data has been written to tempWeightStore, and so now there are 16 new weights to process through the divider.
            else
                get48Weights <= '0';
            end if;
            
            if validDel(6) = '1' and timeStepDel(6) = "11111" and lastStationDel(6) = '1' and unsigned(fineDel(6)) = 71 then
                get24Weights <= '1';   -- 8 new weights to process through the divider.
            else
                get24Weights <= '0';
            end if;
              
            if get48Weights = '1' then
                weightsCountDown <= "100000000"; -- 48 weights, 3 parallel dividers, count down from (48/3) * 16 = 256. 
                weightsCountUp <= "000000000";  -- top 5 bits of the count up are used as the address into the memory, so we get new data every 16 clocks.
                dividerRunning <= '1';
            elsif get24Weights = '1' then
                weightsCountDown <= "010000000"; -- 24 weights, 3 parallel dividers, count down from (24/3) * 16 = 128.
                weightsCountUp <= "000000000";
                dividerRunning <= '1';
            elsif weightsCountDown /= "000000000" then
                weightsCountDown <= std_logic_vector(unsigned(weightsCountDown) - 1);
                weightsCountUp <= std_logic_vector(unsigned(weightsCountUp) + 1);
            else
                dividerRunning <= '0';
            end if;
            
            if weightsCountUp(3 downto 0) = "0000" and dividerRunning = '1' then
                div32ValidIn <= '1';
            else
                div32ValidIn <= '0';
            end if;
            
            finalWeightsSOF <= get48Weights or get24Weights;
            finalWeightsSOFDel1 <= finalWeightsSOF;
            
            if finalWeightsSOF = '1' then 
                outputBuffer <= not outputBuffer;
            end if;
            
            -- Accumulate weighted values across all the stations.
            
            if validDel(15) = '1' then
                AccWrEn(0) <= '1';
            else
                AccWrEn(0) <= '0';
            end if;
            AccWriteAddr <= fineDel(15)(3 downto 0);
            
            lastStationAndValidDel16 <= lastStationDel(15) and validDel(15);
            
            for i in 0 to 2 loop
                if validDel(15) = '1' then
                    if firstStationDel(15) = '1' then
                        weightedSumPol0Real(i) <= weightedPol0Real(i);
                        weightedSumPol0Imag(i) <= weightedPol0Imag(i);
                        weightedSumPol1Real(i) <= weightedPol1Real(i);
                        weightedSumPol1Imag(i) <= weightedPol1Imag(i);
                    else
                        weightedSumPol0Real(i) <= std_logic_vector(signed(weightedPol0Real(i)) + signed(accumulatorPol0Real(i)));
                        weightedSumPol0Imag(i) <= std_logic_vector(signed(weightedPol0Imag(i)) + signed(accumulatorPol0Imag(i)));
                        weightedSumPol1Real(i) <= std_logic_vector(signed(weightedPol1Real(i)) + signed(accumulatorPol1Real(i)));
                        weightedSumPol1Imag(i) <= std_logic_vector(signed(weightedPol1Imag(i)) + signed(accumulatorPol1Imag(i)));
                    end if;
                end if;

                -- find absolute sum of the values for use in the scaling
                if (signed(weightedSumPol0Real(i)) < 0) then
                    weightedSumPol0RealAbs(i) <= std_logic_vector(-signed(weightedSumPol0Real(i)));
                else
                    weightedSumPol0RealAbs(i) <= weightedSumPol0Real(i);
                end if;
                
                if (signed(weightedSumPol0Imag(i)) < 0) then
                    weightedSumPol0ImagAbs(i) <= std_logic_vector(-signed(weightedSumPol0Imag(i)));
                else
                    weightedSumPol0ImagAbs(i) <= weightedSumPol0Imag(i);
                end if;

                if (signed(weightedSumPol1Real(i)) < 0) then
                    weightedSumPol1RealAbs(i) <= std_logic_vector(-signed(weightedSumPol1Real(i)));
                else
                    weightedSumPol1RealAbs(i) <= weightedSumPol1Real(i);
                end if;
                
                if (signed(weightedSumPol1Imag(i)) < 0) then
                    weightedSumPol1ImagAbs(i) <= std_logic_vector(-signed(weightedSumPol1Imag(i)));
                else
                    weightedSumPol1ImagAbs(i) <= weightedSumPol1Imag(i);
                end if;
                
                absPol0Sum(i) <= std_logic_vector(signed(weightedSumPol0RealAbs(i)) + signed(weightedSumPol0ImagAbs(i)));
                absPol1Sum(i) <= std_logic_vector(signed(weightedSumPol1RealAbs(i)) + signed(weightedSumPol1ImagAbs(i)));
                
                absSum(i) <= std_logic_vector(unsigned(absPol0Sum(i)) + unsigned(absPol1Sum(i)));
                
                
                -- Write to the packet buffer. After a complete packet is in the packet buffer, it is read out and converted to 16 bits and written to the output buffer.
                if lastStationAndValidDel16 = '1' then
                    -- Matrix multiplier is 16 bit * 8 bit => 24 bit result
                    -- Phase correction is 24 bit * 17 bit sin+cos, drop 22 bits => 19 bit result
                    -- Sum across 512 antennas could give up to 28 bits, but for gaussian noise it will be about 18 bits.
                    packetBufPol0Din(i)(31 downto 0) <= weightedSumPol0Real(i);
                    packetBufPol0Din(i)(63 downto 32) <= weightedSumPol0Imag(i);
                    packetBufPol1Din(i)(31 downto 0) <= weightedSumPol1Real(i);
                    packetBufPol1Din(i)(63 downto 32) <= weightedSumPol1Imag(i);
                    packetBufWrEn(i)(0) <= '1';
                else
                    packetBufWrEn(i)(0) <= '0';
                end if;
                
            end loop;
            
            -- On accumulating the last station, sum it to find the scaling factor.
            -- there are enough spare bits to ensure that absSumAll cannot overflow.
            absSum01 <= std_logic_vector(unsigned(absSum(0)) + unsigned(absSum(1)));
            absSum2 <= absSum(2);
            
            absSumAll <= std_logic_vector(unsigned(absSum01) + unsigned(absSum2));
            
            if fineDel(20)(2 downto 0) = "000" and timeStepDel(20) = "00000" then
                firstAbsSum <= '1';
            else
                firstAbsSum <= '0';
            end if;
            
            if lastStationDel(21) = '1' and validDel(21) = '1' then
                if (fineDel(21)(3) = '0') then
                    -- Sum for the first packet, i.e. fineDel is 0 to 7 or 16 to 23 etc.
                    if firstAbsSum = '1' then
                        packet0AbsSum <= absSumAllext;
                    else
                        packet0AbsSum <= std_logic_vector(unsigned(packet0AbsSum) + unsigned(absSumAllext));
                    end if;
                else
                    -- Sum for the second packet, i.e. fineDel is 8 to 15, or 23 to 31 etc.
                    if firstAbsSum = '1' then
                        packet1AbsSum <= absSumAllext;
                    else
                        packet1AbsSum <= std_logic_vector(unsigned(packet1AbsSum) + unsigned(absSumAllext));
                    end if;
                end if;
            end if;
            
            if lastStationDel(21) = '1' and validDel(21) = '1' and fineDel(21)(3 downto 0) = "0111" and timeStepDel(21) = "11111" then
                packet0AbsSumValid <= '1';
            else
                packet0AbsSumValid <= '0';
            end if;
            
            if lastStationDel(21) = '1' and validDel(21) = '1' and fineDel(21)(6 downto 0) = "1000111" and timeStepDel(21) = "11111" then
                packet0AbsSumValidOnly <= '1';   -- "only" packet0 will be valid; There are only 24 fine channels in the last set of data, so only one output packet is created.
            else
                packet0AbsSumValidOnly <= '0';
            end if;
            
            if lastStationDel(21) = '1' and validDel(21) = '1' and fineDel(21)(3 downto 0) = "1111" and timeStepDel(21) = "11111" then
                packet1AbsSumValid <= '1';
            else
                packet1AbsSumValid <= '0';
            end if;
            
            if packet0AbsSumValid = '1' then
                getScalingInputValid <= '1';
                getScalingInput <= packet0AbsSum;
            elsif packet1AbsSumValid = '1' then  -- This will occur at least 8 clocks after packet0AbsSumValid, since it takes another 8 clocks to deliver the remaining 24 fine channels.
                getScalingInputValid <= '1';
                getScalingInput <= packet1AbsSum;
            else
                getScalingInputValid <= '0';
            end if;
            
            -- Trigger readout state machine to copy beamformed data to the output buffer and scale it to 16 bits.
            packet0AbsSumValidDel1 <= packet0AbsSumValid;      -- del1 aligns with getScalingInputValid
            packet0AbsSumValidDel2 <= packet0AbsSumValidDel1;  
            packet0AbsSumValidDel3 <= packet0AbsSumValidDel2;  
            packet0AbsSumValidDel4 <= packet0AbsSumValidDel3;  -- Del4 aligns with the output of the PSTBFScaling block.
            
            packet0AbsSumValidOnlyDel1 <= packet0AbsSumValidOnly;
            packet0AbsSumValidOnlyDel2 <= packet0AbsSumValidOnlyDel1;
            packet0AbsSumValidOnlyDel3 <= packet0AbsSumValidOnlyDel2;
            packet0AbsSumValidOnlyDel4 <= packet0AbsSumValidOnlyDel3;
            
            packet1AbsSumValidDel1 <= packet1AbsSumValid;      -- del1 aligns with getScalingInputValid
            packet1AbsSumValidDel2 <= packet1AbsSumValidDel1;  
            packet1AbsSumValidDel3 <= packet1AbsSumValidDel2;  
            packet1AbsSumValidDel4 <= packet1AbsSumValidDel3;
            
            -- The first stage output buffer stores 32+32 bit complex data. (data is converted to 16+16 bit complex when it is moved to the second stage output buffer)
            -- It has space for 2 packets. The write address has :
            --      bits 8:5 = floor((fine channel)/3), where fine channel runs from 0 to 47 to cover all the fine channels in two packets (24 fine channels per packet).
            --      bits 4:0 = time sample
            packetBufWrAddr(8 downto 5) <= fineDel(16)(3 downto 0);
            packetBufWrAddr(4 downto 0) <= timeStepDel(16);
            
        end if;
    end process;
    
    absSumAllExt <= "000000000000" & absSumAll;
    
    phaseStepFreqTemp_x16 <= phaseStepFreqTemp(27 downto 0) & "0000";
    phaseStepFreqTemp_x32 <= phaseStepFreqTemp(26 downto 0) & "00000";
    
    phaseStepFreqTemp_x64 <= phaseStepFreqTemp(25 downto 0) & "000000";
    
    
    -- Convert the absolute sum of the data in the packet into a scaling factor that we can scale all the data back to 16 bits when copying from the packet buffer to the output buffer.
    PSTscaleInst : entity bf_lib.PSTBFScaling
    port map (
        i_clk   => i_BF_clk,
        i_data  => getScalingInput,      -- in (43:0);
        i_valid => getScalingInputValid, -- in std_logic;
        --
        o_shift    => scalingShift,    --  out(3:0);
        o_mantissa => scalingMantissa, -- out(3:0);
        o_valid    => open             -- out std_logic  -- 3 clock latency; not used, separate delay line in this module is used, "packet0AbsSumValidDel4", "packet1AbsSumValidDel4"
    );
    
    -- full parallel 2x2 complex matrix multiplier
    -- Matrix data is in JonesWord0, JonesWord1, vector data is in "data"
    -- 3 instances, one for each of data(31:0), data(63:32), data(95:64)
    matrixmultGen : for i in 0 to 2 generate
        mmult : entity bf_lib.jonesMatrixMult
        port map (
            i_clk => i_BF_clk,
            -- The 2x2 matrix
            i_m00real => jonesWord0(15 downto 0),  -- in(15:0);
            i_m00imag => jonesWord0(31 downto 16), -- in(15:0);
            i_m01real => jonesWord0(47 downto 32), -- in(15:0);
            i_m01imag => jonesWord0(63 downto 48), -- in(15:0);
            i_m10real => jonesWord1(15 downto 0),  -- in(15:0);
            i_m10imag => jonesWord1(31 downto 16), -- in(15:0);
            i_m11real => jonesWord1(47 downto 32), -- in(15:0);
            i_m11imag => jonesWord1(63 downto 48), -- in(15:0);
            -- 2x1 vector
            i_v0real => data(i*32 + 7 downto i*32),     -- in(7:0);
            i_v0imag => data(i*32 + 15 downto i*32+8),  -- in(7:0);
            i_v1real => data(i*32 + 23 downto i*32+16), -- in(7:0);
            i_v1imag => data(i*32 + 31 downto i*32+24), -- in(7:0);
            i_rfi    => flagged(i),
            -- 2x1 vector result (5 clock latency)
            o_r0real => matrixMultR0real(i), -- out(27:0)  (top bit is superfluous)
            o_r0imag => matrixMultR0imag(i), -- out(27:0)
            o_r1real => matrixMultR1real(i), -- out(27:0)
            o_r1imag => matrixMultR1imag(i)  -- out (27:0)
        );
        
        process(i_BF_clk)
        begin
            if rising_edge(i_BF_clk) then
                matrixMultPol0(i)(26 downto 0) <= matrixMultR0real(i)(26 downto 0);
                matrixMultPol0(i)(58 downto 32) <= matrixMultR0imag(i)(26 downto 0);
                matrixMultPol1(i)(26 downto 0) <= matrixMultR1real(i)(26 downto 0);
                matrixMultPol1(i)(58 downto 32) <= matrixMultR1imag(i)(26 downto 0);
            end if;
        end process;
        
        -- complex multiplier for the phase, first polarisation 
        bmultPol0 : BF_cMult27x18
        port map (
            aclk            => i_BF_clk,
            s_axis_a_tvalid => '1',
            s_axis_a_tdata  => matrixMultPol0(i), -- in (63:0); real in 26:0, imaginary in 58:32
            s_axis_b_tvalid => '1',
            s_axis_b_tdata  => sinCos,  -- in 47:0; real in 17:0, imaginary in 41:24; unity is 2^17.
            m_axis_dout_tvalid => open,    -- out std_logic;
            m_axis_dout_tdata  => weightedPol0(i)    -- out 95:0, real in 45:0, imaginary in 93:48
        );
        
        -- keep the top 23 bits, sign extend by 8 bits for use in the accumulator
        weightedPol0Real(i) <= weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45 downto 22);
        weightedPol0Imag(i) <= weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93 downto 70);
        
        -- second polarisation
        bmultPol1 : BF_cMult27x18
        port map (
            aclk            => i_BF_clk,
            s_axis_a_tvalid => '1',
            s_axis_a_tdata  => matrixMultPol1(i), -- in (63:0); real in 26:0, imaginary in 58:32
            s_axis_b_tvalid => '1',
            s_axis_b_tdata  => sinCos,  -- in 47:0; real in 17:0, imaginary in 41:24
            m_axis_dout_tvalid => open, -- out STD_LOGIC;
            m_axis_dout_tdata  => weightedPol1(i) -- out 95:0, real in 45:0, imaginary in 93:48
        );
        
        weightedPol1Real(i) <= weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45 downto 22);
        weightedPol1Imag(i) <= weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93 downto 70);
        
        -- Distributed memories to accumulate results for 16 fine channels.
        -- 3 memories (one for each parallel input sample), each 16 deep, 128 bits wide (real+imag, dual pol, each 32 bits)
        xpm_memory_sdpram_Acc_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 4,               -- DECIMAL
            ADDR_WIDTH_B => 4,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 128,       -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 2048,             -- DECIMAL   128 bits wide x 16 deep = 2048 bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 128,        -- DECIMAL
            READ_LATENCY_B => 1,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 128,       -- DECIMAL
            WRITE_MODE_B => "read_first"      -- String
        )
        port map (
            dbiterrb => open,  
            doutb => AccDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => AccWriteAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => AccReadAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => accDin(i),          -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',           -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',             -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => AccWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );

        -- Distributed memories to accumulate the weights for each fine channel.
        -- Three memories, one for each parallel input sample, each 16 deep, 32 bits wide. 
        -- 3 memories * 16 deep = 48 weights, one for each fine channel that is being accumulated in the two packets that are processed at a time.
        xpm_memory_weightAcc_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 4,                 -- DECIMAL
            ADDR_WIDTH_B => 4,                 -- DECIMAL
            AUTO_SLEEP_TIME => 0,              -- DECIMAL
            BYTE_WRITE_WIDTH_A => 32,          -- DECIMAL
            CASCADE_HEIGHT => 0,               -- DECIMAL
            CLOCKING_MODE => "common_clock",   -- String
            ECC_MODE => "no_ecc",              -- String
            MEMORY_INIT_FILE => "none",        -- String
            MEMORY_INIT_PARAM => "0",          -- String
            MEMORY_OPTIMIZATION => "true",     -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 512,                -- DECIMAL   32 bits wide x 16 deep = 512 bits
            MESSAGE_CONTROL => 0,              -- DECIMAL
            READ_DATA_WIDTH_B => 32,           -- DECIMAL
            READ_LATENCY_B => 1,               -- DECIMAL
            READ_RESET_VALUE_B => "0",         -- String
            RST_MODE_A => "SYNC",              -- String
            RST_MODE_B => "SYNC",              -- String
            SIM_ASSERT_CHK => 0,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,      -- DECIMAL
            USE_MEM_INIT => 0,                 -- DECIMAL
            WAKEUP_TIME => "disable_sleep",    -- String
            WRITE_DATA_WIDTH_A => 32,          -- DECIMAL
            WRITE_MODE_B => "read_first"       -- String
        )
        port map (
            dbiterrb => open,  
            doutb => weightAccDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => weightAccWriteAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => weightAccReadAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => weightSum(i),          -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',           -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',             -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => weightAccWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );

        -- find the maximum possible weight for each frequency
        xpm_memory_maxWeightAcc_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 4,                 -- DECIMAL
            ADDR_WIDTH_B => 4,                 -- DECIMAL
            AUTO_SLEEP_TIME => 0,              -- DECIMAL
            BYTE_WRITE_WIDTH_A => 32,          -- DECIMAL
            CASCADE_HEIGHT => 0,               -- DECIMAL
            CLOCKING_MODE => "common_clock",   -- String
            ECC_MODE => "no_ecc",              -- String
            MEMORY_INIT_FILE => "none",        -- String
            MEMORY_INIT_PARAM => "0",          -- String
            MEMORY_OPTIMIZATION => "true",     -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 512,               -- DECIMAL   32 bits wide x 16 deep = 512 bits
            MESSAGE_CONTROL => 0,              -- DECIMAL
            READ_DATA_WIDTH_B => 32,           -- DECIMAL
            READ_LATENCY_B => 1,               -- DECIMAL
            READ_RESET_VALUE_B => "0",         -- String
            RST_MODE_A => "SYNC",              -- String
            RST_MODE_B => "SYNC",              -- String
            SIM_ASSERT_CHK => 0,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,      -- DECIMAL
            USE_MEM_INIT => 0,                 -- DECIMAL
            WAKEUP_TIME => "disable_sleep",    -- String
            WRITE_DATA_WIDTH_A => 32,          -- DECIMAL
            WRITE_MODE_B => "read_first"       -- String
        )
        port map (
            dbiterrb => open,  
            doutb => maxWeightAccDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => weightAccWriteAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => weightAccReadAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => maxWeightSum(i),          -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',           -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',             -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => weightAccWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
        tempWeightStoreDin(i)(31 downto 0) <= weightSum(i);
        tempWeightStoreDin(i)(63 downto 32) <= maxWeightSum(i);
        
        -- Store the weights and max weights while we do the divisions required to get the 16-bit final weight used in the output packet.
        -- The weights are written to this memory at the conclusion of the packet input data, i.e. when the last sample is weight for the fine channel is accumulated,
        --  instead of being written to the weight accumulator memories.
        --  Note the data input to the memory is the same signal as the input to the weight and max weight accumulator memories.
        xpm_memory_TempWeightStore_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 4,                 -- DECIMAL
            ADDR_WIDTH_B => 4,                 -- DECIMAL
            AUTO_SLEEP_TIME => 0,              -- DECIMAL
            BYTE_WRITE_WIDTH_A => 64,          -- DECIMAL
            CASCADE_HEIGHT => 0,               -- DECIMAL
            CLOCKING_MODE => "common_clock",   -- String
            ECC_MODE => "no_ecc",              -- String
            MEMORY_INIT_FILE => "none",        -- String
            MEMORY_INIT_PARAM => "0",          -- String
            MEMORY_OPTIMIZATION => "true",     -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 1024,               -- DECIMAL   64 bits wide x 16 deep = 1024 bits
            MESSAGE_CONTROL => 0,              -- DECIMAL
            READ_DATA_WIDTH_B => 64,           -- DECIMAL
            READ_LATENCY_B => 1,               -- DECIMAL
            READ_RESET_VALUE_B => "0",         -- String
            RST_MODE_A => "SYNC",              -- String
            RST_MODE_B => "SYNC",              -- String
            SIM_ASSERT_CHK => 0,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,      -- DECIMAL
            USE_MEM_INIT => 0,                 -- DECIMAL
            WAKEUP_TIME => "disable_sleep",    -- String
            WRITE_DATA_WIDTH_A => 64,          -- DECIMAL
            WRITE_MODE_B => "read_first"       -- String
        )
        port map (
            dbiterrb => open,  
            doutb => tempWeightStoreDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => weightAccWriteAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => weightsCountUp(7 downto 4),    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => tempWeightStoreDin(i),       -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',              -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',                -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',               -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => tempWeightStoreWrEn  -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
        
        -- 3 Dividers to get the final weights used in the output packet.
        div32 : entity bf_lib.div32_32_16
        port map (
            i_clk => i_BF_clk, -- in std_logic;
            -- data input; numerator should be less than or equal to denominator
            i_numerator => tempWeightStoreDout(i)(31 downto 0), -- in std_logic_vector(31 downto 0);  -- unsigned input, but top bit should be zero.
            i_denominator => tempWeightStoreDout(i)(63 downto 32), -- in std_logic_vector(31 downto 0); -- unsigned input, but top bit should be zero.
            i_valid => div32ValidIn, -- in std_logic;  -- Can take new input data every 16 clocks.
            -- Data out
            o_quotient => weight16bit(i),   -- out std_logic_vector(15 downto 0); -- output is an unsigned value.
            o_valid    => div32ValidOut(i)  -- out std_logic  -- will go high 17 clocks after i_valid
        );
        
        
        AccDin(i)(31 downto 0) <= weightedSumPol0Real(i);
        AccDin(i)(63 downto 32) <= weightedSumPol0Imag(i);
        AccDin(i)(95 downto 64) <= weightedSumPol1Real(i);
        AccDin(i)(127 downto 96) <= weightedSumPol1Imag(i);
        
        accumulatorPol0Real(i) <= accDout(i)(31 downto 0);
        accumulatorPol0Imag(i) <= accDout(i)(63 downto 32);
        accumulatorPol1Real(i) <= accDout(i)(95 downto 64);
        accumulatorPol1Imag(i) <= accDout(i)(127 downto 96);
        
        -- Block RAM to hold the output packet data while we find the scale factor.
        --  The beamformed data is stored as 32+32 bit complex numbers prior to scaling.
        --  Each of the three memories stores data for 1/3 of the fine channels.
        --  Output packets have :
        --   (24 fine channels) * (2 bytes) * (2 [complex]) * (2 pol) * (32 times) = 6144 bytes
        --  1/3 of these come from each memory, i.e. 8 fine channels.
        --  So each memory has packets with 8*2*2*2*32 = 2048 bytes.
        --  We generate 2 packets at a time, since we process 48 fine channels in a burst (this is for efficiency of the HBM corner turn)
        --  These are stored here since we don't know the scaling factor until the end of the packet.
        --  At the end of the packet, the data is read out, scaled and stored in an ultraram while we wait for our turn to use the packet output bus.
        --
        --  Altogether 6 memories instantiated here.
        --  The memories are 512 deep x 64 bits wide.
        --  There is space for 2 packets. The write address is
        --       bits 8:5 = floor((fine channel)/3), where fine channel runs from 0 to 47 to cover all the fine channels in two packets.
        --       bits 4:0 = time sample 
        --
        --  Read side is also 512 deep by 64 bits wide.
        --  
        --
        --  fine 0,3,6,9...  |  fine 1,4,7...    |  fine 2,5,8,...   |
        --  Pol 0  |  Pol 1  |  Pol 0  |  Pol 1  |  Pol 0  |  Pol 1  |
        --
        xpm_mem_sdpram_PacketPol0_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,               -- DECIMAL
            ADDR_WIDTH_B => 9,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "auto",      -- String
            MEMORY_SIZE => 32768,            -- DECIMAL; 64 bits wide x 512 deep = 32768 bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 64,         -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
            WRITE_MODE_B => "read_first"     -- String
        )
        port map (
            dbiterrb => open,  
            doutb => packetBufPol0Dout(i), -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => packetBufWrAddr,      -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => packetBufRdAddr,      -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => packetBufPol0Din(i),   -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',                 -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',                   -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',                  -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => packetBufWrEn(i)        -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );

        xpm_mem_sdpram_PacketPol1_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,               -- DECIMAL
            ADDR_WIDTH_B => 9,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "auto",      -- String
            MEMORY_SIZE => 32768,            -- DECIMAL; 64 bits wide x 512 deep = 32768 bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 64,         -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
            WRITE_MODE_B => "read_first"     -- String
        )
        port map (
            dbiterrb => open,  
            doutb => packetBufPol1Dout(i),   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => packetBufWrAddr,        -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => packetBufRdAddr,        -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => packetBufPol1Din(i), -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',               -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',                 -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',                -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => packetBufWrEn(i)      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
    end generate;
    
    sclookupPol0 : sincosLookup
    port map (
        aclk                => i_BF_clk,  -- in std_logic;
        s_axis_phase_tvalid => '1',       -- in std_logic;
        s_axis_phase_tdata  => phaseDel1_16bit, -- in std_logic_vector(15 downto 0);  -- Phase in bits 11:0
        m_axis_data_tvalid  => open,      -- out std_logic;
        m_axis_data_tdata   => sinCos     -- out std_logic_vector(47 downto 0)); -- cosine in bits 17:0, sine in bits 41:24, 6 clock latency
    );

    phaseDel1_16bit <= phaseDel1(31) & phaseDel1(31) & phaseDel1(31) & phaseDel1(31) & phaseDel1(31 downto 20);

    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- 
            
            if packet0AbsSumValidDel4 = '1' then
                packet0Shift <= scalingShift;
                packet0Mantissa <= scalingMantissa;
                packet0Waiting <= '1';
                packet0Only <= packet0AbsSumValidOnlyDel4;   -- Last group of 24 fine channels, only one packet to send.
            elsif output_fsm = done then
                packet0Waiting <= '0';
            end if;
            
            if packet1AbsSumValidDel4 = '1' then
                packet1Shift <= scalingShift;
                packet1Mantissa <= scalingMantissa;
            end if;
            
            case output_fsm is
                -- This state machine reads data from the packet memory "xpm_mem_sdpram_PacketPol0_inst", "xpm_mem_sdpram_PacketPol1_inst",
                -- and copies it into the output memory.
                -- Tasks :
                --   - Convert from 6 x 32 bit samples read simultaneously from the packet memory to 4 x 16 bit samples
                --      - 32 bit values get scaled and saturated to 16 bits
                --      - Simple gearbox to convert from 6 to 4 samples.The packet memory reads 2 out of every 3 clocks, the output memory writes every clock. 
                when idle =>
                    if packet0Waiting = '1' then
                        -- See the comments below state machine code for the order data is read from the buffer.
                        outputPacket0Only <= packet0Only;
                        output_fsm <= pol0read0;
                        outputFine <= "0000";  -- fine channel/3; 16 *3 = 48 fine channels in two packets.
                        outputTime <= "00000";  -- 32 times per packet
                    end if;
                    outputBufWrEnAdv9 <= '0';
                
                when pol0read0 =>
                    output_fsm <= pol0read1;
                    outputTime(0) <= '1';
                    outputBufWrEnAdv9 <= '1';
                
                when pol0read1 =>  -- read a second time for pol0
                    output_fsm <= pol0wait;
                    outputBufWrEnAdv9 <= '1';
                
                when pol0wait => -- Only read on 2 out of 3 clocks, since we need 3 clocks to write the data to the output buffer
                    output_fsm <= pol1read0;
                    outputTime(0) <= '0';
                    outputBufWrEnAdv9 <= '1';
                    
                when pol1read0 =>
                    output_fsm <= pol1read1;
                    outputTime(0) <= '1';   -- in the next state, read the second of two time samples.
                    outputBufWrEnAdv9 <= '1';
                
                when pol1read1 =>  -- read a second time for pol1
                    output_fsm <= pol1wait;
                    outputBufWrEnAdv9 <= '1';
                
                when pol1wait =>
                    if outputPacket0Only = '1' then
                        if outputFine = "0111" then
                            outputFine <= "0000";
                            if outputTime = "11111" then
                                output_fsm <= done;
                            else
                                outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                output_fsm <= pol0read0;
                            end if;
                        else
                            outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                            outputTime(0) <= '0';
                            output_fsm <= pol0read0;
                        end if;
                    else
                        if outputFine = "1111" then
                            outputFine <= "0000";
                            if outputTime = "11111" then
                                output_fsm <= done;
                            else
                                outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                output_fsm <= pol0read0;
                            end if;
                        else
                            outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                            outputTime(0) <= '0';
                            output_fsm <= pol0read0;
                        end if;
                    end if;
                    outputBufWrEnAdv9 <= '1';
                   
                when done =>
                    output_fsm <= idle;
                    outputBufWrEnAdv9 <= '0';
                
                when others =>
                    output_fsm <= idle;
                    outputBufWrEnAdv9 <= '0';
            
            end case;
            
            if output_fsm = pol1wait and outputFine = "0111" and outputTime = "11111" then
                triggerSendPacket <= '1';
                triggerSendOnePacketOnly <= outputPacket0Only;
            else
                triggerSendPacket <= '0';
            end if;
            
            output_fsmDel1 <= output_fsm;
            output_fsmDel2 <= output_fsmDel1;
            output_fsmDel3 <= output_fsmDel2;
            output_fsmDel4 <= output_fsmDel3;

            outputFineDel1 <= outputFine;
            outputFineDel2 <= outputFineDel1;
            outputFineDel3 <= outputFineDel2;
            outputFineDel4 <= outputFineDel3;
            
            outputFineDel3ext <= "00" & outputFineDel2(2 downto 0);
            outputFineDel3ext_x2 <= '0' & outputFineDel2(2 downto 0) & '0';
            outputFineDel4_x3 <= std_logic_vector(unsigned(outputFineDel3ext_x2) + unsigned(outputFineDel3ext));  -- low 3 bits of outputFine, x3
            
            outputTimeDel1 <= outputTime;
            outputTimeDel2 <= outputTimeDel1;
            outputTimeDel3 <= outputTimeDel2;
            outputTimeDel4 <= outputTimeDel3;
            
            outputBufWrEnAdv8 <= outputBufWrEnAdv9;
            outputBufWrEnAdv7 <= outputBufWrEnAdv8;  -- Adv8 aligns with outputTimeDel1 since it is assigned in the relevant state machine states, while outputTime is assigned so it is correct in the state, not one clock later.
            outputBufWrEnAdv6 <= outputBufWrEnAdv7;
            outputBufWrEnAdv5 <= outputBufWrEnAdv6;  -- Adv5 aligns with outputTimeDel4
            outputBufWrEnAdv4 <= outputBufWrEnAdv5;  -- Adv4 aligns with outputBufTime
            outputBufWrEnAdv3 <= outputBufWrEnAdv4;  -- adv3 aligns with outputBufWrAddrAdv3
            outputBufWrEnAdv2 <= outputBufWrEnAdv3;
            outputBufWrEnAdv1 <= outputBufWrEnAdv2;
            outputBufWrEn(0) <= outputBufWrEnAdv1;
            
            -- Reading from block ram packet buffers to the ultraRAM output buffer.
            -- The packet buffers consist of 6 memories organised as 512 deep x 64 bits wide.
            --  The six memories have outputs packetBufPol0Dout(0:2), packetBufPol1Dout(0:2):
            --  
            --   fine 0,3,6,9...  |  fine 1,4,7...    |  fine 2,5,8,...   |
            --   Pol 0  |  Pol 1  |  Pol 0  |  Pol 1  |  Pol 0  |  Pol 1  |
            --  
            --  Each packet buffer has space for 2 packets. The write address is
            --       bits 8:5 = floor((fine channel)/3), where fine channel runs from 0 to 47 to cover all the fine channels in two packets.
            --       bits 4:0 = time sample 
            --
            -- But the output buffer is in a different order :
            --  Sample |  Fine  |  Polarisation  |  Time
            --    0        0           0             0
            --    1        0           0             1
            --    ...
            --    31       0           0             31
            --    32       0           1             0
            --    33       0           1             1
            --    ...
            --    63       0           1             31
            --    64       1           0             0
            --    etc. 
            --
            -- We need to read in the order data is written to the packet buffer, so that it is free for new packets to be written to the packet buffer.
            -- This means reading both polarisations, then all fine channels, then going to the next time.  
            -- So:
            --  - Do 2 reads, to get 2 times for each of fine 0, fine 1, fine 2 
            --          Note : fine 0 is one of fine channels 0, 3, 6, 9, etc.
            --               : Reads of these 3 fine channels occur in parallel  
            --  - Do 3 writes (fine0, fine1, fine2)
            --
            
            if (output_fsmDel2 = pol0Read0) then
                output32bitFine0Time0real <= packetBufPol0Dout(0)(31 downto 0);
                output32bitFine0Time0Imag <= packetBufPol0Dout(0)(63 downto 32);
                output32bitFine1Time0real <= packetBufPol0Dout(1)(31 downto 0);
                output32bitFine1Time0Imag <= packetBufPol0Dout(1)(63 downto 32);
                output32bitFine2Time0real <= packetBufPol0Dout(2)(31 downto 0);
                output32bitFine2Time0Imag <= packetBufPol0Dout(2)(63 downto 32);
            elsif (output_fsmDel2 = pol1Read0) then
                output32bitFine0Time0real <= packetBufPol1Dout(0)(31 downto 0);
                output32bitFine0Time0Imag <= packetBufPol1Dout(0)(63 downto 32);
                output32bitFine1Time0real <= packetBufPol1Dout(1)(31 downto 0);
                output32bitFine1Time0Imag <= packetBufPol1Dout(1)(63 downto 32);
                output32bitFine2Time0real <= packetBufPol1Dout(2)(31 downto 0);
                output32bitFine2Time0Imag <= packetBufPol1Dout(2)(63 downto 32);
            end if;
            
            if (output_fsmDel2 = pol0Read1) then
                output32bitFine0Time1real <= packetBufPol0Dout(0)(31 downto 0);
                output32bitFine0Time1Imag <= packetBufPol0Dout(0)(63 downto 32);
                output32bitFine1Time1real <= packetBufPol0Dout(1)(31 downto 0);
                output32bitFine1Time1Imag <= packetBufPol0Dout(1)(63 downto 32);
                output32bitFine2Time1real <= packetBufPol0Dout(2)(31 downto 0);
                output32bitFine2Time1Imag <= packetBufPol0Dout(2)(63 downto 32);
            elsif (output_fsmDel2 = pol1Read1) then
                output32bitFine0Time1real <= packetBufPol1Dout(0)(31 downto 0);
                output32bitFine0Time1Imag <= packetBufPol1Dout(0)(63 downto 32);
                output32bitFine1Time1real <= packetBufPol1Dout(1)(31 downto 0);
                output32bitFine1Time1Imag <= packetBufPol1Dout(1)(63 downto 32);
                output32bitFine2Time1real <= packetBufPol1Dout(2)(31 downto 0);
                output32bitFine2Time1Imag <= packetBufPol1Dout(2)(63 downto 32);
            end if;
            
            output32bitFine2Time0RealDel1 <= output32bitFine2Time0Real;
            output32bitFine2Time0ImagDel1 <= output32bitFine2Time0Imag;
            
            -- 32 bit data into the pipeline that goes converts to 16 bits and writes to the output buffer
            if (output_fsmDel4 = pol0Read0) or (output_fsmDel4 = pol1Read0) then
                sample32bit0 <= output32bitFine0Time0Real;
                sample32bit1 <= output32bitFine0Time0Imag;
                sample32bit2 <= output32bitFine0Time1Real;
                sample32bit3 <= output32bitFine0Time1Imag;
                outputBufFineChannel <= outputFineDel4_x3;
            elsif (output_fsmDel4 = pol0Read1) or (output_fsmDel4 = pol1Read1) then
                sample32bit0 <= output32bitFine1Time0Real;
                sample32bit1 <= output32bitFine1Time0Imag;
                sample32bit2 <= output32bitFine1Time1Real;
                sample32bit3 <= output32bitFine1Time1Imag;
                outputBufFineChannel <= std_logic_vector(unsigned(outputFineDel4_x3) + 1);
            else
                sample32bit0 <= output32bitFine2Time0RealDel1; -- use del1 signal here as the original signal will be overwritten at this point.
                sample32bit1 <= output32bitFine2Time0ImagDel1;
                sample32bit2 <= output32bitFine2Time1Real;
                sample32bit3 <= output32bitFine2Time1Imag;
                outputBufFineChannel <= std_logic_vector(unsigned(outputFineDel4_x3) + 2);
            end if;
            
            -- Get the address that sample32bit0, sample32bit1, sample32bit2, sample32bit3 will be written to.
            if (output_fsmDel4 = pol0Read0 or output_fsmDel4 = pol0Read1 or output_fsmDel4 = pol0wait) then
                outputBufPol <= '0';
            else
                outputBufPol <= '1';
            end if;
            
            outputBufTime <= outputTimeDel4(4 downto 1);  -- There are two times in each 64 bit word written to the output buffer.
            
            outputBufFinePacket <= outputFineDel4(3);     -- outputFine values from 0 to 7 are the first 24 fine channels, which go into one packet, while 8 to 15 are the second 24 fine channels.
            
            -- 5 clock latency from sample32bit0 etc. to outputBufDin, so the same latency is needed for the address
            --  12 address bits to the output buffer: 
            --    - Buffer = 1 bit
            --    - FineChannel packet = 1 bit, selects fine channels 0-23 or 24-47
            --    - FineChannel = 5 bits, selects one of 24 fine channels
            --    - polarisation = 1 bit
            --    - time = 4 bits, 32 time stamps, 2 times per 64 bit write, so 4 bits of address.
            outputBufWrAddrAdv4 <= outputBuffer & outputBufFinePacket & outputBufFineChannel & outputBufPol & outputBufTime;
            outputBufWrAddrAdv3 <= outputBufWrAddrAdv4;
            outputBufWrAddrAdv2 <= outputBufWrAddrAdv3;
            outputBufWrAddrAdv1 <= outputBufWrAddrAdv2;
            outputBufWrAddr <= outputBufWrAddrAdv1;
            
            if scale_exp_frac(3) = '1' then
                shiftFinal <= scale_exp_frac(7 downto 4);
                scaleFinal <= scale_exp_frac(3 downto 0);
            elsif (outputFineDel4(3) = '0') then
                shiftFinal <= packet0Shift;
                scaleFinal <= packet0Mantissa;
            else
                shiftFinal <= packet1Shift;
                scaleFinal <= packet1Mantissa;
            end if;
            
            -- Convert shiftFinal and scaleFinal to a 32 bit floating point value
            -- The scale parameter is, from the PSR ICD, "The value by which beamformer voltage samples are scaled before conversion to the values in the packet"
            --
            -- Arithmetic Scaling :
            --   * Matrix multiplier is 16 bit * 8 bit => 24 bit result
            --   * Phase correction is 24 bit * 17 bit sin+cos, then drops 22 bits
            -- Treat unity for the matrix coefficients as 32768, so the matrix multiplier scales up by a factor of 2^15
            -- unity for the sin/cos lookup is 2^16
            -- So altogether the matrix multipler and phase correction scale up by 2^31, but then drop 22 bits, so they scale up by 2^9.
            -- 
            if scaleFinal(3) = '0' then -- we only generate one case where this is true, scaleFinal = "0111", so we need to add one extra to the exponent, since FP32 assumes the mantissa starts with a 1.
                fp32Exp <= std_logic_vector(to_unsigned(126 + 9,8) - unsigned(shiftFinalExt));
                fp32Frac <= scaleFinal(1 downto 0) & "00";
            else
                fp32Exp <= std_logic_vector(to_unsigned(127 + 9,8) - unsigned(shiftFinalExt)); -- i.e. to get the output value, we took the original value, scaled it up by 2^9 via the matrix multiplier and phase correction, and then scaled it down by 2^shiftFinal.
                fp32Frac <= scaleFinal(2 downto 0) & '0';
            end if;
            
            if outputBufWrEnAdv4 = '1' then
                if outputBufWrAddrAdv4(11) = '0' then
                    if outputBufWrAddrAdv4(10) = '0' then
                        buf0Packet0Scale <= '0' & fp32Exp & fp32Frac;
                    else
                        buf0Packet1Scale <= '0' & fp32Exp & fp32Frac;
                    end if;
                else
                    if outputBufWrAddrAdv4(10) = '0' then
                        buf1Packet0Scale <= '0' & fp32Exp & fp32Frac;
                    else
                        buf1Packet1Scale <= '0' & fp32Exp & fp32Frac;
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    shiftFinalExt <= "0000" & shiftFinal;
    
    packetBufRdAddr <= outputFine & outputTime;
    
    scalepol0Rei : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit0,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit0 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );
    
    scalepol0Imi : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit1,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit1 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );

    scalepol1Rei : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit2,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit2 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );
    
    scalepol1Imi : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit3,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit3 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );
    
    
    outputBufDin <= sample16bit3 & sample16bit2 & sample16bit1 & sample16bit0;
    
    -- One ultraram for the final output buffer.
    --
    -- Data output order (each sample is 32 bits = 16 bit real + 16 bit imaginary)
    --
    --  Sample |  Fine  |  Polarisation  |  Time
    --    0        0           0             0                    -- Note - 2 complex samples per 64 bit word in the output buffer
    --    1        0           0             1
    --   ...
    --    31       0           0             31
    --    32       0           1             0
    --    33       0           1             1
    --   ...
    --    63       0           1             31
    --    64       1           0             0
    --    etc.
    --
    -- The memory is 4096 deep x 64 wide,
    -- The data part of each packet occupies 6144 bytes = 768 x 64 bit words
    -- The memory is partitioned into space for 4 packets.
    finalOutputBuffer_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 12,              -- DECIMAL
        ADDR_WIDTH_B => 12,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => 262144,           -- DECIMAL; 64 bits wide x 4096 deep = 262144 bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 64,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 64,        -- DECIMAL
        WRITE_MODE_B => "read_first"     -- String
    )
    port map (
        dbiterrb => open,  
        doutb => outputBufDout,     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => outputBufWrAddr,    -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => outputBufRdAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_BF_clk,                 
        clkb => i_BF_clk,             
        dina => outputBufDin,    -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',                 -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',                     -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',                   -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => outputBufWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    

    -- Store final weights for each fine channel.
    -- Each output packet has 24 weights, one for each fine channel.
    -- Each weight is 2 bytes, so 48 bytes total.
    -- This memory has space for 256 bytes = 4 * 64 = space for weights for 4 packets.
    -- The weights come from the 3 dividers, so we get data in blocks of 3*16 = 48 bits.
    -- We have to convert to 64 bit wide data to write to the finalWeightStore memory.  
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            if (finalWeightsSOF = '1') then
                finalWeightPhase <= "0000";
                
                finalWeightWrEn(0) <= '0';
            elsif (div32ValidOut(0) = '1') then   -- div32ValidOut(1) and div32Validout(2) are the same as div32ValidOut(0).
                
                finalWeightPhase <= std_logic_vector(unsigned(finalWeightPhase) + 1);
                if finalWeightPhase(1 downto 0) = "00" then
                    finalWeightDin(47 downto 0) <= weight16bit(2) & weight16bit(1) & weight16bit(0);
                    finalWeightWrEn(0) <= '0'; 
                elsif finalWeightPhase(1 downto 0) = "01" then
                    finalWeightDin(63 downto 48) <= weight16bit(0);
                    finalWeightOverflow <= weight16bit(2) & weight16bit(1);
                    finalWeightWrEn(0) <= '1';
                elsif finalWeightPhase(1 downto 0) = "10" then
                    finalWeightDin(31 downto 0) <= finalWeightOverflow;
                    finalWeightDin(63 downto 32) <= weight16bit(1) & weight16bit(0);
                    finalWeightOverflow(15 downto 0) <= weight16bit(2);
                    finalWeightWrEn(0) <= '1';
                elsif finalWeightPhase(1 downto 0) = "11" then
                    finalWeightDin(15 downto 0) <= finalWeightOverflow(15 downto 0);
                    finalWeightDin(63 downto 16) <= weight16bit(2) & weight16bit(1) & weight16bit(0);
                    finalWeightWrEn(0) <= '1';
                end if;
            else
                finalWeightWrEn(0) <= '0';
            end if;
            
            if finalWeightsSOFDel1 = '1' then
                finalWeightWrAddr <= outputBuffer & "0000";
            elsif finalWeightWrEn(0) = '1' then
                if finalWeightWrAddr(2 downto 0) = "101" then
                    -- six 64-bit words of weight data for each frame, so move to the next frame.
                    -- Space for 4 frames in the buffer, selected by address bits (4:3).  
                    finalWeightWrAddr(3 downto 0) <= "1000";
                else
                    finalWeightWrAddr <= std_logic_vector(unsigned(finalWeightWrAddr) + 1);
                end if;
            end if;
            
        end if;
    end process;
    
    
    xpm_memory_FinalWeightStore_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 5,                 -- DECIMAL
        ADDR_WIDTH_B => 5,                 -- DECIMAL
        AUTO_SLEEP_TIME => 0,              -- DECIMAL
        BYTE_WRITE_WIDTH_A => 64,          -- DECIMAL;
        CASCADE_HEIGHT => 0,               -- DECIMAL
        CLOCKING_MODE => "common_clock",   -- String
        ECC_MODE => "no_ecc",              -- String
        MEMORY_INIT_FILE => "none",        -- String
        MEMORY_INIT_PARAM => "0",          -- String
        MEMORY_OPTIMIZATION => "true",     -- String
        MEMORY_PRIMITIVE => "distributed", -- String
        MEMORY_SIZE => 2048,               -- DECIMAL   64 bits wide x 32 deep = 2048 bits
        MESSAGE_CONTROL => 0,              -- DECIMAL
        READ_DATA_WIDTH_B => 64,           -- DECIMAL
        READ_LATENCY_B => 2,               -- DECIMAL
        READ_RESET_VALUE_B => "0",         -- String
        RST_MODE_A => "SYNC",              -- String
        RST_MODE_B => "SYNC",              -- String
        SIM_ASSERT_CHK => 0,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,      -- DECIMAL
        USE_MEM_INIT => 0,                 -- DECIMAL
        WAKEUP_TIME => "disable_sleep",    -- String
        WRITE_DATA_WIDTH_A => 64,          -- DECIMAL
        WRITE_MODE_B => "read_first"        -- String
    )
    port map (
        dbiterrb => open,  
        doutb => finalWeightDout,     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,
        addra => finalWeightWrAddr,   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb => finalWeightRdAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka => i_BF_clk,
        clkb => i_BF_clk,
        dina => finalWeightDin,       -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena => '1',    
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',              -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',                -- 1-bit input: Reset signal for the final port B output register stage. 
        sleep => '0',               -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => finalWeightWrEn      -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    
    
    --------------------------------------------------------------------------------
    -- Read out the packets from the finalWeightStore and finalOutputBuffer memories
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- The final packet in a coarse channel (packet 9) can be finished before the previous packets have been sent.
            -- So if triggerSendPacket occurs while readout_fsm is busy, we have to wait until readout_fsm is in the done state
            -- before acting on it.
            
            if triggerSendPacket = '1' then
                triggerSendPacketHold <= '1';
                outputBufferHold <= outputBuffer;
                triggerSendOnePacketOnlyHold <= triggerSendOnePacketOnly;
            elsif readout_fsm = done then
                triggerSendPacketHold <= '0';
            end if;
            
            if ((triggerSendPacketHold = '1') and readout_fsm = done) then
                readout_fsm <= start;
                readoutBufferSelect <= outputBufferHold;
                readoutOnePacketOnly <= triggerSendOnePacketOnlyHold;
            else
                case readout_fsm is
                    when start =>
                        finalWeightRdAddr <= readoutBufferSelect & "0000";
                        outputBufRdAddr <= readoutBufferSelect & "00000000000";
                        readout_waitCount <= std_logic_vector(to_unsigned(g_PACKET_OFFSET,20));
                        readout_fsm <= waitOffset;
                        
                    when waitOffset =>
                        -- Wait for our turn to send data on the output bus, so we don't clash with other beamformer modules.
                        readout_waitCount <= std_logic_vector(unsigned(readout_waitCount) - 1);
                        if (unsigned(readout_waitCount) = 0) then
                            readout_fsm <= sendHeaderWord;
                        end if; 
                    
                    when sendHeaderWord =>
                        readout_fsm <= sendWeights;
                    
                    when sendWeights =>
                        finalWeightRdAddr <= std_logic_vector(unsigned(finalWeightRdAddr) + 1);
                        if finalWeightRdAddr(2 downto 0) = "101" then
                            readout_fsm <= sendData;
                        end if;
                        
                    when sendData =>
                        outputBufRdAddr <= std_logic_vector(unsigned(outputBufRdAddr) + 1);
                        if outputBufRdAddr(9 downto 0) = "1011111111" then
                            if (readoutOnePacketOnly = '0') and outputBufRdAddr(10) = '0' then
                                -- if outputBufRdAddr(10) = '1' then we have already read the second packet.
                                readout_fsm <= waitSecondPacket;
                            else
                                readout_fsm <= waitWholeFrameDone;
                            end if;
                        end if;
                    
                    when waitSecondPacket =>
                        finalWeightRdAddr(3 downto 0) <= "1000"; -- select the base address of the second packet in the buffer
                        outputBufRdAddr(10 downto 0) <= "10000000000"; -- select the base address of the second packet in the buffer
                        readout_waitCount <= std_logic_vector(to_unsigned(g_PACKET_OFFSET,20));
                        if waitCount2Done = '1' then  -- i.e. all beams have had time to send their first packet, so now go to send our second packet.
                            readout_fsm <= waitOffset;
                        end if;
                    
                    when waitWholeFrameDone =>  -- This state ensures that all beams wait the same amount of time before starting the next beam, particularly important for the case where we are about to start the packet with the final set of fine channels.
                        if wholeFrameDone = '1' then
                            readout_fsm <= done;
                        end if;
                        
                    when done =>
                        readout_fsm <= done;
                        
                    when others =>
                        readout_fsm <= done;
                end case;
            end if;
            
            if readout_fsm = start then
                --readout_waitCount2 <= std_logic_vector(to_unsigned(g_SECOND_PACKET_OFFSET,20));
                readout_waitCount2 <= "0000" & secondPacketOffset;
                waitCount2Done <= '0';
            elsif (unsigned(readout_waitCount2) /= 0) then
                readout_waitCount2 <= std_logic_vector(unsigned(readout_waitCount2) - 1);
                waitCount2Done <= '0';
            else
                waitCount2Done <= '1';
            end if;
            
            if readout_fsm = start then
                if readoutOnePacketOnly = '1' then
                    wholeFrameWaitCount <= "0000" & secondPacketOffset;
                else
                    wholeFrameWaitCount <= "000" & secondPacketOffset & '0'; -- i.e. 2x the time required for one packet from all enabled beams.
                end if;
                wholeFrameDone <= '0';
            elsif (unsigned(wholeFrameWaitCount) /= 0) then
                wholeFrameWaitCount <= std_logic_vector(unsigned(wholeFrameWaitCount) - 1);
                wholeFrameDone <= '0';
            else
                wholeFrameDone <= '1';
            end if;
            
            -- We want :
            --  secondPacketOffset <= 800 * beamsEnabled + 100
            -- This used to be a calculated as a generic based on beams instantiated, but is more flexible if calculated as the number of beams
            -- that are in use for a particular configuration
            -- Note 800 = 512 + 256 + 32
            beamsEnabled_x768 <= std_logic_vector(unsigned(beamsEnabled_x512) + unsigned(beamsEnabled_x256));
            beamsEnabled_x32_plus100 <= std_logic_vector(unsigned(beamsEnabled_x32) + 100);
            
            secondPacketOffset <= std_logic_vector(unsigned(beamsEnabled_x768) + unsigned(beamsEnabled_x32_plus100));
            
            if readout_fsm = sendHeaderWord then
                outputSelectHeaderWord <= '1';
            else
                outputSelectHeaderWord <= '0';
            end if;
            outputSelectHeaderWordDel1 <= outputSelectHeaderWord; 
            
            if readout_fsm = sendWeights then
                outputSelectWeights <= '1';
            else
                outputSelectWeights <= '0';
            end if;
            outputSelectWeightsDel1 <= outputSelectWeights;
            
            if readout_fsm = sendData then
                outputSelectData <= '1';
            else
                outputSelectData <= '0';
            end if;
            outputSelectDataDel1 <= outputSelectData;
            
            if readoutBufferSelect = '0' then
                if outputBufRdAddr(10) = '0' then
                    headerWord <= buf0Packet0Scale;
                else
                    headerWord <= buf0Packet1Scale;
                end if;
            else
                if outputBufRdAddr(10) = '0' then
                    headerWord <= buf1Packet0Scale;
                else
                    headerWord <= buf1Packet1Scale;
                end if;
            end if;
            
        end if;
    end process;
    
    beamsEnabled_x512 <= beamsEnabled(6 downto 0) & "000000000";  -- Support a maximum of 127 beams.
    beamsEnabled_x256 <= '0' & beamsEnabled(6 downto 0) & "00000000";
    beamsEnabled_x32  <=  "0000" & beamsEnabled(6 downto 0) & "00000";
    
    
    ----------------------------------------------------------------------------------------------------
    process(i_BF_clk)
    begin
        if rising_Edge(i_BF_clk) then
            dbadPacket <= badPacket;
            dreadout_waitCount2 <= readout_waitCount2;
            dwholeFrameWaitCount <= wholeFrameWaitCount;
            
            if readout_fsm = start then
                dreadout_fsm <= "0000";
            elsif readout_fsm = waitOffset then
                dreadout_fsm <= "0001";
            elsif readout_fsm = sendHeaderWord then
                dreadout_fsm <= "0010";
            elsif readout_fsm = sendWeights then
                dreadout_fsm <= "0011";
            elsif readout_fsm = sendData then
                dreadout_fsm <= "0100";
            elsif readout_fsm = waitSecondPacket then
                dreadout_fsm <= "0101";
            elsif readout_fsm = waitWholeFrameDone then
                dreadout_fsm <= "0110";
            elsif readout_fsm = done then
                dreadout_fsm <= "0111";
            else
                dreadout_fsm <= "1111";
            end if;
            
            dTriggerSendPacket <= triggerSendPacket;
            dTriggerSendOnePacketOnly <= triggerSendOnePacketOnly;
            dTriggerSendPacketHold <= triggerSendPacketHold;
            dOutputBufferHold <= outputBufferHold;
            dTriggerSendOnePacketOnlyHold <= triggerSendOnePacketOnlyHold;
                
        end if;
    end process;
    
    
--    ila_beamdebug : ila_2
--    port map (
--        clk => i_BF_clk,
--        probe0(0) => dbadPacket,
--        probe0(20 downto 1) => dreadout_waitCount2,
--        probe0(40 downto 21) => dwholeFrameWaitCount,
--        probe0(44 downto 41) => dreadout_fsm,
--        probe0(45) => dTriggerSendPacket,
--        probe0(46) => dTriggerSendOnePacketOnly,
--        probe0(47) => dTriggerSendPacketHold,
--        probe0(48) => dOutputBufferHold,
--        probe0(49) => dTriggerSendOnePacketOnlyHold,
--        probe0(63 downto 50) => zeros(63 downto 50)
--    );
    zeros <= (others => '0');
    
    
--    ila_beamdebug : ila_beamdata
--    port map (
--        clk => i_BF_clk,
--        probe0(31 downto 0) => phaseBaseUpdated,
--        probe0(79 downto 32) => phaseRdData(47 downto 0),
--        probe0(80) => loadJonesDel7,
--        probe0(95 downto 81) => phaseAddr,
--        probe0(96) => valid,
--        probe0(104 downto 97) => fine,
--        probe0(109 downto 105) => timestep(4 downto 0),
--        probe0(110) => firstStation,
--        probe0(111) => lastStation,
--        probe0(119 downto 112) => zeros(7 downto 0)
--    );
    
    
end Behavioral;

