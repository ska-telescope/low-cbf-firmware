----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: dave.humphrey@csiro.au
-- 
-- Create Date: 19.03.2020 15:03:39
-- Module Name: used_list_URAM - Behavioral 
-- Description: 
--   
--  block memory for the used lists:
--   * (384 channels) * (40 pointers) * (3 bytes/pointer) = 46080 bytes,
--   * URAMs are 72 bits wide, so they fit 3 pointers per address. 
--  so 2 URAMs for the used pointer list allows up to 63 pointers per channel.
--  This is sufficient if 1Gbyte of HBM is allocated. When 2 Gbytes are allocated, double this is needed
--  i.e, 4 URAMs and up to 126 pointers per channel.
--  For the 1 GByte case, 2 URAMs are used :
--    Each channel has 21 URAM addresses each, and 21*384 = 8064 of the 8192 addresses are used.
--    (note : 8192 addresses since each URAM is (4096 deep) x (64 bits wide), so 2 URAMs => 8192 addresses)
--  For the 2 GByte case, 4 URAMs are used :
--    Each channel has 42 URAM addresses each, and 42*384 = 16128 of the 16384 addresses are used.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;

entity used_list_URAM is
    generic(
        g_USE_2GBYTE : boolean := false
    );
    port (
        i_clk    : in std_logic;
        -- writes
        i_wrChannel : in std_logic_vector(8 downto 0); -- channel list to write to.
        i_wrAddr : in std_logic_vector(6 downto 0);    -- index of the pointer in the channel list (0 to 62 for 1Gbyte version, of 0 to 125 for 2Gbyte version)
        i_wrData : in std_logic_vector(23 downto 0);   -- pointer value to write.
        i_wrEn   : in std_logic;
        -- reads
        i_rdChannel : in std_logic_vector(8 downto 0);
        i_rdAddr : in std_logic_vector(6 downto 0); 
        o_rdData : out std_logic_vector(23 downto 0)   -- 6 clock latency from i_rdChannel to o_rdData
    );
end used_list_URAM;

architecture Behavioral of used_list_URAM is
    
    constant c_URAM_ADDR_WIDTH : integer := sel(g_USE_2GBYTE,14,13);  -- 2^13 = 8192, 2^14 = 16384
    constant c_USED_LIST_N_URAMS : integer := sel(g_USE_2GBYTE,4,2);  -- 
    
    signal wrChannelx1, wrChannelx2, wrChannelx4, wrChannelx8, wrChannelx16, wrChannelx32 : std_logic_vector(15 downto 0);
    signal wrAddrBase : std_logic_vector(15 downto 0);
    signal wrAddrDiv3 : std_logic_vector(5 downto 0);
    signal wrAddrDiv3Del1 : std_logic_vector(15 downto 0);
    signal wrAddrFinal : std_logic_vector(15 downto 0);
    signal wrAddrMod3, wrAddrMod3Del1 : std_logic_vector(1 downto 0);
    signal WrEnWords : std_logic_vector(2 downto 0);
    signal wrEn : std_logic_vector(8 downto 0); -- 71 downto 0); -- byte write enable to the ultra RAM block, 9 x 8bits = 72 bits.
    signal wrDataDel1 : std_logic_vector(23 downto 0);
    signal din : std_logic_vector(71 downto 0);
    
    signal rdChannelx1, rdChannelx2, rdChannelx4, rdChannelx8, rdChannelx16, rdChannelx32 : std_logic_vector(15 downto 0);
    signal rdAddrBase : std_logic_vector(15 downto 0);
    signal rdAddrDiv3del1 : std_logic_vector(15 downto 0);
    signal rdAddrDiv3 : std_logic_vector(5 downto 0);
    signal rdAddrFinal : std_logic_vector(15 downto 0);
    signal rdAddrMod3del1, rdAddrMod3 : std_logic_vector(1 downto 0);
    signal rdAddrMod3Del2, rdAddrMod3Del3, rdAddrMod3Del4, rdAddrMod3Del5 : std_logic_vector(1 downto 0);
    signal Dout : std_logic_vector(71 downto 0);
    signal wrEnDel1 : std_logic;
    
begin
    
    wrChannelx1 <= "0000000" & i_wrChannel;
    wrChannelx4 <= "00000" & i_wrChannel & "00";
    wrChannelx16 <= "000" & i_wrChannel & "0000";
    
    wrChannelx2 <= "000000" & i_wrChannel & '0';
    wrChannelx8 <= "0000" & i_wrChannel & "000";
    wrChannelx32 <= "00" & i_wrChannel & "00000";

    rdChannelx1 <= "0000000" & i_rdChannel;
    rdChannelx4 <= "00000" & i_rdChannel & "00";
    rdChannelx16 <= "000" & i_rdChannel & "0000";
    
    rdChannelx2 <= "000000" & i_rdChannel & '0';
    rdChannelx8 <= "0000" & i_rdChannel & "000";
    rdChannelx32 <= "00" & i_rdChannel & "00000";
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            -- Calculate the address to write to
            --
            -- First pipeline stage:
            -- Multiply the channel by either 21 (when g_USE_2GBYTE = false) or 42 (when g_USE_2GBYTE = true)
            -- to get the base address for the pointers for this channel.
            -- Then add floor(i_wrAddr/3)
            -- Use mod(i_wrAddr,3) to get the place where the data sits in the 72 bits.
            if g_USE_2GBYTE then
                wrAddrBase <= std_logic_vector(unsigned(wrChannelx2) + unsigned(wrChannelx8) + unsigned(wrChannelx32));
            else
                wrAddrBase <= std_logic_vector(unsigned(wrChannelx1) + unsigned(wrChannelx4) + unsigned(wrChannelx16));
            end if;
            -- Need floor(i_wrAddr/3). i_wrAddr is only 7 bits, so just use a look up table.
            wrAddrDiv3del1 <= "0000000000" & wrAddrDiv3;
            wrAddrMod3del1 <= wrAddrMod3;
            wrDataDel1 <= i_wrData;
            wrEnDel1 <= i_wrEn;
            
            -- Second pipeline stage:
            wrAddrFinal <= std_logic_vector(unsigned(wrAddrBase) + unsigned(wrAddrDiv3del1));
            if wrEnDel1 = '0' then
                WrEnWords <= "000";
            else
                if wrAddrMod3del1 = "00" then
                    WrEnWords <= "001";
                elsif wrAddrMod3del1 = "01" then
                    WrEnWords <= "010";
                else
                    WrEnWords <= "100";
                end if;
            end if;
            din <= wrDataDel1 & wrDataDel1 & wrDataDel1;
            
            -------------------------------------------------------------------------------
            -- Calculate the address to read from
            if g_USE_2GBYTE then
                rdAddrBase <= std_logic_vector(unsigned(rdChannelx2) + unsigned(rdChannelx8) + unsigned(rdChannelx32));
            else
                rdAddrBase <= std_logic_vector(unsigned(rdChannelx1) + unsigned(rdChannelx4) + unsigned(rdChannelx16));
            end if;
            rdAddrDiv3del1 <= "0000000000" & rdAddrDiv3;
            rdAddrMod3del1 <= rdAddrMod3;
            
            -- Second pipeline stage
            rdAddrFinal <= std_logic_vector(unsigned(rdAddrBase) + unsigned(rdAddrDiv3del1));
            rdAddrMod3Del2 <= rdAddrMod3Del1;
            
            -- 3 cycle latency to read the memory
            rdAddrMod3Del3 <= rdAddrMod3Del2;
             
            rdAddrMod3Del4 <= rdAddrMod3Del3;
            
            rdAddrMod3Del5 <= rdAddrMod3Del4;
            
            -- select the correct set of bytes from the read data
            if rdAddrMod3Del5 = "00" then
                o_rdData <= Dout(23 downto 0);
            elsif rdAddrMod3Del5 = "01" then
                o_rdData <= Dout(47 downto 24);
            else
                o_rdData <= Dout(71 downto 48);
            end if;
            
        end if;
    end process;

    used_list_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => c_URAM_ADDR_WIDTH,    -- DECIMAL  -- 72 bit wide by 8192 or 16384 deep
        ADDR_WIDTH_B => c_URAM_ADDR_WIDTH,    -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL
        --CASCADE_HEIGHT => 0,           -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "ultra",     -- String
        MEMORY_SIZE => c_USED_LIST_N_URAMS*4096*72, -- DECIMAL; total size in bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 72,         -- DECIMAL
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 72,        -- DECIMAL
        WRITE_MODE_B => "read_first"      -- String
    )
    port map (
        dbiterrb => open,           -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
        doutb    => Dout,   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,           -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra    => wrAddrFinal(c_URAM_ADDR_WIDTH-1 downto 0), -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb    => rdAddrFinal(c_URAM_ADDR_WIDTH-1 downto 0), -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka     => i_clk,          -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb     => i_clk,          -- 1-bit input, unused when clocking mode is "common_clock"
        dina     => Din,    -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena      => '1',            -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
        enb      => '1',            -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
        injectdbiterra => '0',      -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
        injectsbiterra => '0',      -- 1-bit input: Controls single bit error injection on input data when ECC enabled
        regceb => '1',              -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',                -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
        sleep => '0',               -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => wrEn         -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    
    wrEn(2 downto 0) <= (others => wrEnWords(0));
    wrEn(5 downto 3) <= (others => wrEnWords(1));
    wrEn(8 downto 6) <= (others => wrEnWords(2));
    
    --wrEn(23 downto 0) <= (others => wrEnWords(0));
    --wrEn(47 downto 24) <= (others => wrEnWords(1));
    --wrEn(71 downto 48) <= (others => wrEnWords(2));

    -- Divide 7 bit numbers by 3.
    -- Generated by Matlab code:
    -- for n = 0:127
    --    disp(['        "' dec2bin(floor(n/3),6) '" when "' dec2bin(n,7) '", -- ' num2str(floor(n/3)) ' when ' num2str(n)]);
    -- end
    --
    -- for n = 0:127
    --    disp(['        "' dec2bin(mod(n,3),2) '" when "' dec2bin(n,7) '", -- ' num2str(mod(n,3)) ' when ' num2str(n)]);
    -- end
    
    with i_wrAddr select
        wrAddrDiv3 <=
            "000000" when "0000000", -- 0 when 0
            "000000" when "0000001", -- 0 when 1
            "000000" when "0000010", -- 0 when 2
            "000001" when "0000011", -- 1 when 3
            "000001" when "0000100", -- 1 when 4
            "000001" when "0000101", -- 1 when 5
            "000010" when "0000110", -- 2 when 6
            "000010" when "0000111", -- 2 when 7
            "000010" when "0001000", -- 2 when 8
            "000011" when "0001001", -- 3 when 9
            "000011" when "0001010", -- 3 when 10
            "000011" when "0001011", -- 3 when 11
            "000100" when "0001100", -- 4 when 12
            "000100" when "0001101", -- 4 when 13
            "000100" when "0001110", -- 4 when 14
            "000101" when "0001111", -- 5 when 15
            "000101" when "0010000", -- 5 when 16
            "000101" when "0010001", -- 5 when 17
            "000110" when "0010010", -- 6 when 18
            "000110" when "0010011", -- 6 when 19
            "000110" when "0010100", -- 6 when 20
            "000111" when "0010101", -- 7 when 21
            "000111" when "0010110", -- 7 when 22
            "000111" when "0010111", -- 7 when 23
            "001000" when "0011000", -- 8 when 24
            "001000" when "0011001", -- 8 when 25
            "001000" when "0011010", -- 8 when 26
            "001001" when "0011011", -- 9 when 27
            "001001" when "0011100", -- 9 when 28
            "001001" when "0011101", -- 9 when 29
            "001010" when "0011110", -- 10 when 30
            "001010" when "0011111", -- 10 when 31
            "001010" when "0100000", -- 10 when 32
            "001011" when "0100001", -- 11 when 33
            "001011" when "0100010", -- 11 when 34
            "001011" when "0100011", -- 11 when 35
            "001100" when "0100100", -- 12 when 36
            "001100" when "0100101", -- 12 when 37
            "001100" when "0100110", -- 12 when 38
            "001101" when "0100111", -- 13 when 39
            "001101" when "0101000", -- 13 when 40
            "001101" when "0101001", -- 13 when 41
            "001110" when "0101010", -- 14 when 42
            "001110" when "0101011", -- 14 when 43
            "001110" when "0101100", -- 14 when 44
            "001111" when "0101101", -- 15 when 45
            "001111" when "0101110", -- 15 when 46
            "001111" when "0101111", -- 15 when 47
            "010000" when "0110000", -- 16 when 48
            "010000" when "0110001", -- 16 when 49
            "010000" when "0110010", -- 16 when 50
            "010001" when "0110011", -- 17 when 51
            "010001" when "0110100", -- 17 when 52
            "010001" when "0110101", -- 17 when 53
            "010010" when "0110110", -- 18 when 54
            "010010" when "0110111", -- 18 when 55
            "010010" when "0111000", -- 18 when 56
            "010011" when "0111001", -- 19 when 57
            "010011" when "0111010", -- 19 when 58
            "010011" when "0111011", -- 19 when 59
            "010100" when "0111100", -- 20 when 60
            "010100" when "0111101", -- 20 when 61
            "010100" when "0111110", -- 20 when 62
            "010101" when "0111111", -- 21 when 63
            "010101" when "1000000", -- 21 when 64
            "010101" when "1000001", -- 21 when 65
            "010110" when "1000010", -- 22 when 66
            "010110" when "1000011", -- 22 when 67
            "010110" when "1000100", -- 22 when 68
            "010111" when "1000101", -- 23 when 69
            "010111" when "1000110", -- 23 when 70
            "010111" when "1000111", -- 23 when 71
            "011000" when "1001000", -- 24 when 72
            "011000" when "1001001", -- 24 when 73
            "011000" when "1001010", -- 24 when 74
            "011001" when "1001011", -- 25 when 75
            "011001" when "1001100", -- 25 when 76
            "011001" when "1001101", -- 25 when 77
            "011010" when "1001110", -- 26 when 78
            "011010" when "1001111", -- 26 when 79
            "011010" when "1010000", -- 26 when 80
            "011011" when "1010001", -- 27 when 81
            "011011" when "1010010", -- 27 when 82
            "011011" when "1010011", -- 27 when 83
            "011100" when "1010100", -- 28 when 84
            "011100" when "1010101", -- 28 when 85
            "011100" when "1010110", -- 28 when 86
            "011101" when "1010111", -- 29 when 87
            "011101" when "1011000", -- 29 when 88
            "011101" when "1011001", -- 29 when 89
            "011110" when "1011010", -- 30 when 90
            "011110" when "1011011", -- 30 when 91
            "011110" when "1011100", -- 30 when 92
            "011111" when "1011101", -- 31 when 93
            "011111" when "1011110", -- 31 when 94
            "011111" when "1011111", -- 31 when 95
            "100000" when "1100000", -- 32 when 96
            "100000" when "1100001", -- 32 when 97
            "100000" when "1100010", -- 32 when 98
            "100001" when "1100011", -- 33 when 99
            "100001" when "1100100", -- 33 when 100
            "100001" when "1100101", -- 33 when 101
            "100010" when "1100110", -- 34 when 102
            "100010" when "1100111", -- 34 when 103
            "100010" when "1101000", -- 34 when 104
            "100011" when "1101001", -- 35 when 105
            "100011" when "1101010", -- 35 when 106
            "100011" when "1101011", -- 35 when 107
            "100100" when "1101100", -- 36 when 108
            "100100" when "1101101", -- 36 when 109
            "100100" when "1101110", -- 36 when 110
            "100101" when "1101111", -- 37 when 111
            "100101" when "1110000", -- 37 when 112
            "100101" when "1110001", -- 37 when 113
            "100110" when "1110010", -- 38 when 114
            "100110" when "1110011", -- 38 when 115
            "100110" when "1110100", -- 38 when 116
            "100111" when "1110101", -- 39 when 117
            "100111" when "1110110", -- 39 when 118
            "100111" when "1110111", -- 39 when 119
            "101000" when "1111000", -- 40 when 120
            "101000" when "1111001", -- 40 when 121
            "101000" when "1111010", -- 40 when 122
            "101001" when "1111011", -- 41 when 123
            "101001" when "1111100", -- 41 when 124
            "101001" when "1111101", -- 41 when 125
            "101010" when "1111110", -- 42 when 126
            "101010" when others;    -- 42 when 127
            
    with i_wrAddr select
        wrAddrMod3 <=
            "00" when "0000000", -- 0 when 0
            "01" when "0000001", -- 1 when 1
            "10" when "0000010", -- 2 when 2
            "00" when "0000011", -- 0 when 3
            "01" when "0000100", -- 1 when 4
            "10" when "0000101", -- 2 when 5
            "00" when "0000110", -- 0 when 6
            "01" when "0000111", -- 1 when 7
            "10" when "0001000", -- 2 when 8
            "00" when "0001001", -- 0 when 9
            "01" when "0001010", -- 1 when 10
            "10" when "0001011", -- 2 when 11
            "00" when "0001100", -- 0 when 12
            "01" when "0001101", -- 1 when 13
            "10" when "0001110", -- 2 when 14
            "00" when "0001111", -- 0 when 15
            "01" when "0010000", -- 1 when 16
            "10" when "0010001", -- 2 when 17
            "00" when "0010010", -- 0 when 18
            "01" when "0010011", -- 1 when 19
            "10" when "0010100", -- 2 when 20
            "00" when "0010101", -- 0 when 21
            "01" when "0010110", -- 1 when 22
            "10" when "0010111", -- 2 when 23
            "00" when "0011000", -- 0 when 24
            "01" when "0011001", -- 1 when 25
            "10" when "0011010", -- 2 when 26
            "00" when "0011011", -- 0 when 27
            "01" when "0011100", -- 1 when 28
            "10" when "0011101", -- 2 when 29
            "00" when "0011110", -- 0 when 30
            "01" when "0011111", -- 1 when 31
            "10" when "0100000", -- 2 when 32
            "00" when "0100001", -- 0 when 33
            "01" when "0100010", -- 1 when 34
            "10" when "0100011", -- 2 when 35
            "00" when "0100100", -- 0 when 36
            "01" when "0100101", -- 1 when 37
            "10" when "0100110", -- 2 when 38
            "00" when "0100111", -- 0 when 39
            "01" when "0101000", -- 1 when 40
            "10" when "0101001", -- 2 when 41
            "00" when "0101010", -- 0 when 42
            "01" when "0101011", -- 1 when 43
            "10" when "0101100", -- 2 when 44
            "00" when "0101101", -- 0 when 45
            "01" when "0101110", -- 1 when 46
            "10" when "0101111", -- 2 when 47
            "00" when "0110000", -- 0 when 48
            "01" when "0110001", -- 1 when 49
            "10" when "0110010", -- 2 when 50
            "00" when "0110011", -- 0 when 51
            "01" when "0110100", -- 1 when 52
            "10" when "0110101", -- 2 when 53
            "00" when "0110110", -- 0 when 54
            "01" when "0110111", -- 1 when 55
            "10" when "0111000", -- 2 when 56
            "00" when "0111001", -- 0 when 57
            "01" when "0111010", -- 1 when 58
            "10" when "0111011", -- 2 when 59
            "00" when "0111100", -- 0 when 60
            "01" when "0111101", -- 1 when 61
            "10" when "0111110", -- 2 when 62
            "00" when "0111111", -- 0 when 63
            "01" when "1000000", -- 1 when 64
            "10" when "1000001", -- 2 when 65
            "00" when "1000010", -- 0 when 66
            "01" when "1000011", -- 1 when 67
            "10" when "1000100", -- 2 when 68
            "00" when "1000101", -- 0 when 69
            "01" when "1000110", -- 1 when 70
            "10" when "1000111", -- 2 when 71
            "00" when "1001000", -- 0 when 72
            "01" when "1001001", -- 1 when 73
            "10" when "1001010", -- 2 when 74
            "00" when "1001011", -- 0 when 75
            "01" when "1001100", -- 1 when 76
            "10" when "1001101", -- 2 when 77
            "00" when "1001110", -- 0 when 78
            "01" when "1001111", -- 1 when 79
            "10" when "1010000", -- 2 when 80
            "00" when "1010001", -- 0 when 81
            "01" when "1010010", -- 1 when 82
            "10" when "1010011", -- 2 when 83
            "00" when "1010100", -- 0 when 84
            "01" when "1010101", -- 1 when 85
            "10" when "1010110", -- 2 when 86
            "00" when "1010111", -- 0 when 87
            "01" when "1011000", -- 1 when 88
            "10" when "1011001", -- 2 when 89
            "00" when "1011010", -- 0 when 90
            "01" when "1011011", -- 1 when 91
            "10" when "1011100", -- 2 when 92
            "00" when "1011101", -- 0 when 93
            "01" when "1011110", -- 1 when 94
            "10" when "1011111", -- 2 when 95
            "00" when "1100000", -- 0 when 96
            "01" when "1100001", -- 1 when 97
            "10" when "1100010", -- 2 when 98
            "00" when "1100011", -- 0 when 99
            "01" when "1100100", -- 1 when 100
            "10" when "1100101", -- 2 when 101
            "00" when "1100110", -- 0 when 102
            "01" when "1100111", -- 1 when 103
            "10" when "1101000", -- 2 when 104
            "00" when "1101001", -- 0 when 105
            "01" when "1101010", -- 1 when 106
            "10" when "1101011", -- 2 when 107
            "00" when "1101100", -- 0 when 108
            "01" when "1101101", -- 1 when 109
            "10" when "1101110", -- 2 when 110
            "00" when "1101111", -- 0 when 111
            "01" when "1110000", -- 1 when 112
            "10" when "1110001", -- 2 when 113
            "00" when "1110010", -- 0 when 114
            "01" when "1110011", -- 1 when 115
            "10" when "1110100", -- 2 when 116
            "00" when "1110101", -- 0 when 117
            "01" when "1110110", -- 1 when 118
            "10" when "1110111", -- 2 when 119
            "00" when "1111000", -- 0 when 120
            "01" when "1111001", -- 1 when 121
            "10" when "1111010", -- 2 when 122
            "00" when "1111011", -- 0 when 123
            "01" when "1111100", -- 1 when 124
            "10" when "1111101", -- 2 when 125
            "00" when "1111110", -- 0 when 126
            "01" when others;    -- 1 when 127

    with i_rdAddr select
        rdAddrDiv3 <=
            "000000" when "0000000", -- 0 when 0
            "000000" when "0000001", -- 0 when 1
            "000000" when "0000010", -- 0 when 2
            "000001" when "0000011", -- 1 when 3
            "000001" when "0000100", -- 1 when 4
            "000001" when "0000101", -- 1 when 5
            "000010" when "0000110", -- 2 when 6
            "000010" when "0000111", -- 2 when 7
            "000010" when "0001000", -- 2 when 8
            "000011" when "0001001", -- 3 when 9
            "000011" when "0001010", -- 3 when 10
            "000011" when "0001011", -- 3 when 11
            "000100" when "0001100", -- 4 when 12
            "000100" when "0001101", -- 4 when 13
            "000100" when "0001110", -- 4 when 14
            "000101" when "0001111", -- 5 when 15
            "000101" when "0010000", -- 5 when 16
            "000101" when "0010001", -- 5 when 17
            "000110" when "0010010", -- 6 when 18
            "000110" when "0010011", -- 6 when 19
            "000110" when "0010100", -- 6 when 20
            "000111" when "0010101", -- 7 when 21
            "000111" when "0010110", -- 7 when 22
            "000111" when "0010111", -- 7 when 23
            "001000" when "0011000", -- 8 when 24
            "001000" when "0011001", -- 8 when 25
            "001000" when "0011010", -- 8 when 26
            "001001" when "0011011", -- 9 when 27
            "001001" when "0011100", -- 9 when 28
            "001001" when "0011101", -- 9 when 29
            "001010" when "0011110", -- 10 when 30
            "001010" when "0011111", -- 10 when 31
            "001010" when "0100000", -- 10 when 32
            "001011" when "0100001", -- 11 when 33
            "001011" when "0100010", -- 11 when 34
            "001011" when "0100011", -- 11 when 35
            "001100" when "0100100", -- 12 when 36
            "001100" when "0100101", -- 12 when 37
            "001100" when "0100110", -- 12 when 38
            "001101" when "0100111", -- 13 when 39
            "001101" when "0101000", -- 13 when 40
            "001101" when "0101001", -- 13 when 41
            "001110" when "0101010", -- 14 when 42
            "001110" when "0101011", -- 14 when 43
            "001110" when "0101100", -- 14 when 44
            "001111" when "0101101", -- 15 when 45
            "001111" when "0101110", -- 15 when 46
            "001111" when "0101111", -- 15 when 47
            "010000" when "0110000", -- 16 when 48
            "010000" when "0110001", -- 16 when 49
            "010000" when "0110010", -- 16 when 50
            "010001" when "0110011", -- 17 when 51
            "010001" when "0110100", -- 17 when 52
            "010001" when "0110101", -- 17 when 53
            "010010" when "0110110", -- 18 when 54
            "010010" when "0110111", -- 18 when 55
            "010010" when "0111000", -- 18 when 56
            "010011" when "0111001", -- 19 when 57
            "010011" when "0111010", -- 19 when 58
            "010011" when "0111011", -- 19 when 59
            "010100" when "0111100", -- 20 when 60
            "010100" when "0111101", -- 20 when 61
            "010100" when "0111110", -- 20 when 62
            "010101" when "0111111", -- 21 when 63
            "010101" when "1000000", -- 21 when 64
            "010101" when "1000001", -- 21 when 65
            "010110" when "1000010", -- 22 when 66
            "010110" when "1000011", -- 22 when 67
            "010110" when "1000100", -- 22 when 68
            "010111" when "1000101", -- 23 when 69
            "010111" when "1000110", -- 23 when 70
            "010111" when "1000111", -- 23 when 71
            "011000" when "1001000", -- 24 when 72
            "011000" when "1001001", -- 24 when 73
            "011000" when "1001010", -- 24 when 74
            "011001" when "1001011", -- 25 when 75
            "011001" when "1001100", -- 25 when 76
            "011001" when "1001101", -- 25 when 77
            "011010" when "1001110", -- 26 when 78
            "011010" when "1001111", -- 26 when 79
            "011010" when "1010000", -- 26 when 80
            "011011" when "1010001", -- 27 when 81
            "011011" when "1010010", -- 27 when 82
            "011011" when "1010011", -- 27 when 83
            "011100" when "1010100", -- 28 when 84
            "011100" when "1010101", -- 28 when 85
            "011100" when "1010110", -- 28 when 86
            "011101" when "1010111", -- 29 when 87
            "011101" when "1011000", -- 29 when 88
            "011101" when "1011001", -- 29 when 89
            "011110" when "1011010", -- 30 when 90
            "011110" when "1011011", -- 30 when 91
            "011110" when "1011100", -- 30 when 92
            "011111" when "1011101", -- 31 when 93
            "011111" when "1011110", -- 31 when 94
            "011111" when "1011111", -- 31 when 95
            "100000" when "1100000", -- 32 when 96
            "100000" when "1100001", -- 32 when 97
            "100000" when "1100010", -- 32 when 98
            "100001" when "1100011", -- 33 when 99
            "100001" when "1100100", -- 33 when 100
            "100001" when "1100101", -- 33 when 101
            "100010" when "1100110", -- 34 when 102
            "100010" when "1100111", -- 34 when 103
            "100010" when "1101000", -- 34 when 104
            "100011" when "1101001", -- 35 when 105
            "100011" when "1101010", -- 35 when 106
            "100011" when "1101011", -- 35 when 107
            "100100" when "1101100", -- 36 when 108
            "100100" when "1101101", -- 36 when 109
            "100100" when "1101110", -- 36 when 110
            "100101" when "1101111", -- 37 when 111
            "100101" when "1110000", -- 37 when 112
            "100101" when "1110001", -- 37 when 113
            "100110" when "1110010", -- 38 when 114
            "100110" when "1110011", -- 38 when 115
            "100110" when "1110100", -- 38 when 116
            "100111" when "1110101", -- 39 when 117
            "100111" when "1110110", -- 39 when 118
            "100111" when "1110111", -- 39 when 119
            "101000" when "1111000", -- 40 when 120
            "101000" when "1111001", -- 40 when 121
            "101000" when "1111010", -- 40 when 122
            "101001" when "1111011", -- 41 when 123
            "101001" when "1111100", -- 41 when 124
            "101001" when "1111101", -- 41 when 125
            "101010" when "1111110", -- 42 when 126
            "101010" when others;    -- 42 when 127
            
    with i_rdAddr select
        rdAddrMod3 <=
            "00" when "0000000", -- 0 when 0
            "01" when "0000001", -- 1 when 1
            "10" when "0000010", -- 2 when 2
            "00" when "0000011", -- 0 when 3
            "01" when "0000100", -- 1 when 4
            "10" when "0000101", -- 2 when 5
            "00" when "0000110", -- 0 when 6
            "01" when "0000111", -- 1 when 7
            "10" when "0001000", -- 2 when 8
            "00" when "0001001", -- 0 when 9
            "01" when "0001010", -- 1 when 10
            "10" when "0001011", -- 2 when 11
            "00" when "0001100", -- 0 when 12
            "01" when "0001101", -- 1 when 13
            "10" when "0001110", -- 2 when 14
            "00" when "0001111", -- 0 when 15
            "01" when "0010000", -- 1 when 16
            "10" when "0010001", -- 2 when 17
            "00" when "0010010", -- 0 when 18
            "01" when "0010011", -- 1 when 19
            "10" when "0010100", -- 2 when 20
            "00" when "0010101", -- 0 when 21
            "01" when "0010110", -- 1 when 22
            "10" when "0010111", -- 2 when 23
            "00" when "0011000", -- 0 when 24
            "01" when "0011001", -- 1 when 25
            "10" when "0011010", -- 2 when 26
            "00" when "0011011", -- 0 when 27
            "01" when "0011100", -- 1 when 28
            "10" when "0011101", -- 2 when 29
            "00" when "0011110", -- 0 when 30
            "01" when "0011111", -- 1 when 31
            "10" when "0100000", -- 2 when 32
            "00" when "0100001", -- 0 when 33
            "01" when "0100010", -- 1 when 34
            "10" when "0100011", -- 2 when 35
            "00" when "0100100", -- 0 when 36
            "01" when "0100101", -- 1 when 37
            "10" when "0100110", -- 2 when 38
            "00" when "0100111", -- 0 when 39
            "01" when "0101000", -- 1 when 40
            "10" when "0101001", -- 2 when 41
            "00" when "0101010", -- 0 when 42
            "01" when "0101011", -- 1 when 43
            "10" when "0101100", -- 2 when 44
            "00" when "0101101", -- 0 when 45
            "01" when "0101110", -- 1 when 46
            "10" when "0101111", -- 2 when 47
            "00" when "0110000", -- 0 when 48
            "01" when "0110001", -- 1 when 49
            "10" when "0110010", -- 2 when 50
            "00" when "0110011", -- 0 when 51
            "01" when "0110100", -- 1 when 52
            "10" when "0110101", -- 2 when 53
            "00" when "0110110", -- 0 when 54
            "01" when "0110111", -- 1 when 55
            "10" when "0111000", -- 2 when 56
            "00" when "0111001", -- 0 when 57
            "01" when "0111010", -- 1 when 58
            "10" when "0111011", -- 2 when 59
            "00" when "0111100", -- 0 when 60
            "01" when "0111101", -- 1 when 61
            "10" when "0111110", -- 2 when 62
            "00" when "0111111", -- 0 when 63
            "01" when "1000000", -- 1 when 64
            "10" when "1000001", -- 2 when 65
            "00" when "1000010", -- 0 when 66
            "01" when "1000011", -- 1 when 67
            "10" when "1000100", -- 2 when 68
            "00" when "1000101", -- 0 when 69
            "01" when "1000110", -- 1 when 70
            "10" when "1000111", -- 2 when 71
            "00" when "1001000", -- 0 when 72
            "01" when "1001001", -- 1 when 73
            "10" when "1001010", -- 2 when 74
            "00" when "1001011", -- 0 when 75
            "01" when "1001100", -- 1 when 76
            "10" when "1001101", -- 2 when 77
            "00" when "1001110", -- 0 when 78
            "01" when "1001111", -- 1 when 79
            "10" when "1010000", -- 2 when 80
            "00" when "1010001", -- 0 when 81
            "01" when "1010010", -- 1 when 82
            "10" when "1010011", -- 2 when 83
            "00" when "1010100", -- 0 when 84
            "01" when "1010101", -- 1 when 85
            "10" when "1010110", -- 2 when 86
            "00" when "1010111", -- 0 when 87
            "01" when "1011000", -- 1 when 88
            "10" when "1011001", -- 2 when 89
            "00" when "1011010", -- 0 when 90
            "01" when "1011011", -- 1 when 91
            "10" when "1011100", -- 2 when 92
            "00" when "1011101", -- 0 when 93
            "01" when "1011110", -- 1 when 94
            "10" when "1011111", -- 2 when 95
            "00" when "1100000", -- 0 when 96
            "01" when "1100001", -- 1 when 97
            "10" when "1100010", -- 2 when 98
            "00" when "1100011", -- 0 when 99
            "01" when "1100100", -- 1 when 100
            "10" when "1100101", -- 2 when 101
            "00" when "1100110", -- 0 when 102
            "01" when "1100111", -- 1 when 103
            "10" when "1101000", -- 2 when 104
            "00" when "1101001", -- 0 when 105
            "01" when "1101010", -- 1 when 106
            "10" when "1101011", -- 2 when 107
            "00" when "1101100", -- 0 when 108
            "01" when "1101101", -- 1 when 109
            "10" when "1101110", -- 2 when 110
            "00" when "1101111", -- 0 when 111
            "01" when "1110000", -- 1 when 112
            "10" when "1110001", -- 2 when 113
            "00" when "1110010", -- 0 when 114
            "01" when "1110011", -- 1 when 115
            "10" when "1110100", -- 2 when 116
            "00" when "1110101", -- 0 when 117
            "01" when "1110110", -- 1 when 118
            "10" when "1110111", -- 2 when 119
            "00" when "1111000", -- 0 when 120
            "01" when "1111001", -- 1 when 121
            "10" when "1111010", -- 2 when 122
            "00" when "1111011", -- 0 when 123
            "01" when "1111100", -- 1 when 124
            "10" when "1111101", -- 2 when 125
            "00" when "1111110", -- 0 when 126
            "01" when others;    -- 1 when 127

end Behavioral;
