# To convert command file generated by python model into a command file that can be used in the simulation
#quick hack for now, this should be autogenerated by the runme.sh script that automatically compiles the project now
#python3 ../../../../../tools/args/gen_tb_config.py -c cnic.ccfg -i registers.txt -o registers_tb.txt
#exit


# To convert command file generated by python model into a command file that can be used in the simulation
PERSONALITY="pst"
echo -e
echo -e "Usage : $0 <fpga build directory>"
echo -e "assumes the file registers.txt exists in the current directory"
echo -e "Argument 1: Optionally Specify the fpga buld directory where  ${PERSONALITY}.ccfg is located  or leave out to select latest fpga build directory"
echo -

pushd ../../../../
export GITREPO=$(pwd)
echo -e "\nBase Git directory: $GITREPO"
CCFG_DIR=$GITREPO/build/ARGS/$PERSONALITY/ 
popd

# Copy CCFG from ARGs to base of GIT REPO.
cp $CCFG_DIR/${PERSONALITY}.ccfg $GITREPO


echo "Generating registerUpload.txt for the testbench from HWData_pst.txt"
python3 $GITREPO/tools/args/gen_tb_config.py -c $GITREPO/${PERSONALITY}.ccfg -i $GITREPO/HWData_pst.txt -o $GITREPO/registerUpload.txt




