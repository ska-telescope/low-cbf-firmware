----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 31.03.2020 09:35:09
-- Design Name: 
-- Module Name: ct_vfc_malloc_tb - Behavioral
-- Description: 
--  testbench for the memory allocation module for the corner turn.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library ct_vfc_lib;


entity ct_vfc_malloc_tb is
--  Port ( );
end ct_vfc_malloc_tb;

architecture Behavioral of ct_vfc_malloc_tb is

    signal clk : std_logic := '0';
    signal resetCount : integer := 0;
    signal resetDone, reset : std_logic := '0';
    signal testCase : integer := 0;
    signal packetTriggerCount : std_logic_vector(7 downto 0);

    signal rr0_trigger, rr0_packetsPerFrame : integer;
    signal rr1_trigger, rr1_packetsPerFrame : integer;

    type packet_fsm_type is (idle, send_packet, wait_done);
    signal packet_fsm : packet_fsm_type := idle;

    -- configuration parameters
    signal freeMax     : std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
    signal baseAddr    : std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
    signal maxFutureCount : std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
    signal scanStartPacketCount : std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
    signal blocksPerFrame0 : std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
    signal preload0        : std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
    signal totalChannels0  : std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
    signal blocksPerFrame1 : std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
    signal preload1        : std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
    signal totalChannels1  : std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
    signal totalChannelsIn : std_logic_vector(8 downto 0); 
    -- interface to the channel order memory
    signal readChannelOrderAddress0 : std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 0
    signal readChannelOrderData0    : std_logic_vector(8 downto 0); -- virtual channel to read; assumes 3 clock latency from o_readChannelOrderAddress
    signal readChannelOrderAddress1 : std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 1
    signal readChannelOrderData1    : std_logic_vector(8 downto 0);
    signal readChannelOrderAddress0Del2, readChannelOrderAddress0Del1, readChannelOrderAddress1Del2, readChannelOrderAddress1Del1 : std_logic_vector(8 downto 0) := "000000000"; 
    -- miscellaneous
    signal packetCountSeen : std_logic;
    
    -- writes
    signal packetCountWrite : std_logic_vector(31 downto 0);  -- Packet count from the packet header
    signal channelWrite     : std_logic_vector(15 downto 0);  -- virtual channel from the packet header
    signal iwriteValid      : std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
    signal writeAddress     : std_logic_vector(23 downto 0); -- Address to write this packet to, in units of 8192 bytes.
    signal writeOK          : std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
    signal owriteValid      : std_logic;
    
    -- reads
    signal readAddress     : std_logic_vector(23 downto 0);   -- HBM address to read from, in units of 8192 bytes. Points to the start of a 2048 sample packet.
    signal readPacketCount : std_logic_vector(31 downto 0);   -- packet count for this read address
    signal readChannel     : std_logic_vector(15 downto 0);   -- Virtual channel at this address.
    signal readStart       : std_logic;                       -- Indicates this is the first read for this channel
    signal readOK          : std_logic;                       -- o_readAddress points to valid data
    signal readFree        : std_logic;                       -- Indicates readAddress can be freed once it has been read
    signal readValid0      : std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
    signal readReady0      : std_logic;                        -- port 0 read side can accept a new address to read from. Data is transfered when valid and ready are high.
    signal readValid1      : std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
    signal readReady1      : std_logic;                        -- port 1 read side can accept a new address to read from. Data is transfered when valid and ready are high.        
    signal free0           : std_logic;                        -- pulse to indicate that the free operation can go ahead.
    signal free1           : std_logic;    
    
    -- error monitoring from the module.
    signal freeSize : std_logic_vector(15 downto 0);      -- Number of entries in the free list;
    signal badStart0 : std_logic;                          -- i_startFrame0 went high, but still in the process of reading the previous frame.
    signal badStart1 : std_logic;                          -- i_startFrame1 went high, but still in the process of reading the previous frame.
    signal inputLateCount : std_logic_vector(15 downto 0);   -- Count of input LFAA blocks which have been dropped as they are too late. Top bit is sticky.
    signal inputTooSoonCount : std_logic_vector(15 downto 0); -- Count of input LFAA blocks which have been dropped as they are too far into the future. Top bit is sticky.
    signal duplicates : std_logic_vector(7 downto 0);    -- count of duplicate input packets (i.e. input packets with the same timestamp and channel as a previous packet). Top bit is sticky.
    signal missingBlocks0 : std_logic_vector(15 downto 0);   -- Count of blocks (1 block = 2048 time samples) on output 0 read with no data; top bit is sticky; low 15 bits wrap.
    signal missingBlocks1 : std_logic_vector(15 downto 0);   -- Count of blocks on output 1 read with no data; top bit is sticky; low 15 bits wrap.
    signal preload1_gt_preload0 : std_logic;                 -- Error flag; i_preload0 must be greater than or equal to i_preload1. Detected when new values are loaded after a module reset (via i_rst).
    signal badFreeFIFOState : std_logic; 
    
    signal readSpacing0, readSpacing1 : integer := 0;
    signal rd0TriggerCount, rd1TriggerCount : integer := 0;
    signal gen_requests0, gen_requests1 : std_logic := '0';
    signal requests0Remaining, requests1Remaining : integer := 0;
    signal free0Del : std_logic_vector(13 downto 0) := (others => '0');   -- delay line to signal free after some interval.
    signal free1Del : std_logic_vector(69 downto 0) := (others => '0');    
    signal busy : std_logic;
    
    -- Self checking.
    -- Keep a record of every pointer allocated, so it can be checked at the readout.
    -- e.g. for 0.3 second corner turn, there are 136 pointers to 2.2 ms LFAA blocks.
    -- Need to run several frames, so need at least 400 pointers per channel. Allow 
    type intArray_t is array(0 to 8191) of integer;
    type ptrRecord_t is array(0 to 383) of intArray_t;
    signal ptrRecord : ptrRecord_t := (others => (others => 0));
    
    signal rr0_channelCount, rr0_packet, rr1_channelCount, rr1_packet : integer := 0;
    signal rr0_channel_int, rr1_channel_int : integer := 0;
    signal rr0_packetCount, rr1_packetCount : integer := 0;
    signal preload0_int, preload1_int : integer;
    signal blocksPerFrame0_int, blocksPerFrame1_int : integer;
    signal totalChannels0_int, totalChannels1_int : integer;
    signal freeMin : std_logic_vector(15 downto 0);
    
    type fa_t is array(0 to 16383) of integer;
    signal freeCount : fa_t := (others => 0);  -- keep track of how many times a piece of memory has been freed; useful for tracking down memory leaks.
    signal firstZero : integer := 0;
    signal minValue, minIndex : integer := 0;
    signal maxValue, maxIndex : integer := 0;
    
begin
    
    clk <= not clk after 5 ns; -- 100 MHz (HBM reference clock).
    testCase <= 3;
    
    -- configuration as integers, used for self checking
    preload0_int <= 
        to_integer(unsigned(preload0(15 downto 11))) when preload0(10 downto 0) = "00000000000" else
        to_integer(unsigned(preload0(15 downto 11))) + 1;

    preload1_int <= 
        to_integer(unsigned(preload1(15 downto 11))) when preload1(10 downto 0) = "00000000000" else
        to_integer(unsigned(preload1(15 downto 11))) + 1;
    
    blocksPerFrame0_int <= to_integer(unsigned(blocksPerFrame0));
    blocksPerFrame1_int <= to_integer(unsigned(blocksPerFrame1));
    
    totalChannels0_int <= to_integer(unsigned(totalChannels0));
    totalChannels1_int <= to_integer(unsigned(totalChannels1));
    
    process(clk)
        variable addr_v, firstZero_v : integer;
        variable minValue_v, minIndex_v : integer;
        variable maxValue_v, maxIndex_v : integer;
    begin
        if rising_edge(clk) then
            -- Reset
            if resetCount < 32768 then
                resetDone <= '0';
                resetCount <= resetCount + 1;
            else
                resetDone <= '1';
            end if;
            if resetCount = 2 then
                reset <= '1';
            else
                reset <= '0';
            end if;
            
            -- Define the test case
            case testCase is
                when 1 => -- cut down case, 8 channels
                    freeMax     <= x"4000"; -- 16384 = 1 Gbyte of memory; std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
                    baseAddr    <= x"100000"; -- std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
                    -- maxFutureCount needs to be at least 2 frames worth, since it is taken from the place we are reading from.
                    maxFutureCount <= x"0200"; -- std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
                    scanStartPacketCount <= x"00000000"; -- std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
                    -- 0x88 = 136 = 136*(2048*1080e-9) = 0.3 sec corner turn.
                    blocksPerFrame0 <= x"088"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 22 * 2048 = 45056 = 0xB000
                    preload0        <= x"B000"; -- std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
                    -- Cut down to 8 channels.
                    totalChannels0  <= "000001000"; -- =8;  std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
                    -- 0x18 = 24 = 24 * (2048*1080e-9) = 53 ms corner turn.
                    blocksPerFrame1 <= x"018"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 2 * 2048 = 4096 = 0x1000
                    preload1        <= x"1000"; -- std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
                    totalChannels1  <= "000001000";  -- =8; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
                    -- Number of clocks between the read requests; needs to be less than the 64 clocks between incoming packets so we have time to do the preload reads.
                    readSpacing0 <= 49;
                    readSpacing1 <= 47;
                when 2 => -- full case, 384 channels
                    freeMax     <= x"4000"; -- 16384 = 1 Gbyte of memory; std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
                    baseAddr    <= x"100000"; -- std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
                    maxFutureCount <= x"0200"; -- std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
                    scanStartPacketCount <= x"00000000"; -- std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
                    -- 0x88 = 136 = 136*(2048*1080e-9) = 0.3 sec corner turn.
                    blocksPerFrame0 <= x"088"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 22 * 2048 = 45056 = 0xB000
                    preload0        <= x"B000"; -- std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
                    -- 384 channels.
                    totalChannels0  <= "110000000"; -- =x"180"; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
                    -- 0x18 = 24 = 24 * (2048*1080e-9) = 53 ms corner turn.
                    blocksPerFrame1 <= x"018"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 2 * 2048 = 4096 = 0x1000
                    preload1        <= x"1000"; -- std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
                    totalChannels1  <= "110000000"; -- =x"180"; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
                    -- Number of clocks between the read requests; needs to be less than the 64 clocks between incoming packets so we have time to do the preload reads.
                    readSpacing0 <= 49;
                    readSpacing1 <= 47;
                when 3 => -- full case, 384 channels, readouts on different orders, odd number of blocks per frame.
                    freeMax     <= x"4000"; -- 16384 = 1 Gbyte of memory; std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
                    baseAddr    <= x"100000"; -- std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
                    maxFutureCount <= x"0200"; -- std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
                    scanStartPacketCount <= x"00000000"; -- std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
                    -- 0x88 = 136 = 136*(2048*1080e-9) = 0.3 sec corner turn.
                    blocksPerFrame0 <= x"089"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 22 * 2048 = 45056 = 0xB000
                    preload0        <= x"B000"; -- std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
                    -- 384 channels.
                    totalChannels0  <= "110000000"; -- =x"180"; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
                    -- 0x18 = 24 = 24 * (2048*1080e-9) = 53 ms corner turn.
                    blocksPerFrame1 <= x"017"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 2 * 2048 = 4096 = 0x1000
                    preload1        <= x"1000"; -- std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
                    totalChannels1  <= "110000000"; -- =x"180"; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
                    -- Number of clocks between the read requests; needs to be less than the 64 clocks between incoming packets so we have time to do the preload reads.
                    readSpacing0 <= 49;
                    readSpacing1 <= 47;  
                    
                when others => -- full case, 384 channels, readouts on different orders.
                    freeMax     <= x"4000"; -- 16384 = 1 Gbyte of memory; std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
                    baseAddr    <= x"100000"; -- std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
                    maxFutureCount <= x"0200"; -- std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
                    scanStartPacketCount <= x"00000000"; -- std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
                    -- 0x88 = 136 = 136*(2048*1080e-9) = 0.3 sec corner turn.
                    blocksPerFrame0 <= x"088"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 22 * 2048 = 45056 = 0xB000
                    preload0        <= x"B000"; -- std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
                    -- 384 channels.
                    totalChannels0  <= "110000000"; -- =x"180"; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
                    -- 0x18 = 24 = 24 * (2048*1080e-9) = 53 ms corner turn.
                    blocksPerFrame1 <= x"018"; -- std_logic_vector(11 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
                    -- 2 * 2048 = 4096 = 0x1000
                    preload1        <= x"1000"; -- std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
                    totalChannels1  <= "110000000"; -- =x"180"; -- std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
                    -- Number of clocks between the read requests; needs to be less than the 64 clocks between incoming packets so we have time to do the preload reads.
                    readSpacing0 <= 49;
                    readSpacing1 <= 47;  
            end case;
            
            if (unsigned(totalChannels0) > unsigned(totalChannels1)) then
                totalChannelsIn <= totalChannels0;
            else
                totalChannelsIn <= totalChannels1;
            end if;
            
            -- Generate input packets
            -- The ct_vfc_malloc module needs to be able to service the requests; 
            -- Each input packet takes about:
            --  write : 14 clocks
            --  rd    : 16 clocks
            --  other rd : 16 clocks
            --  free  : 4 clocks for every 8th packet;
            -- So we need at least 48 clocks per input packet.
            -- So generate a new packet every 64 clocks.
            -- Full case :
            --    In a complete 0.9 second frame, there are :
            --     (408 LFAA packets) * (384 channels) = 156672 packets per 0.902 seconds.
            --    At 64 clocks per packet => 10027008 clocks ( = 0.1 seconds sim time with 100 MHz simulation clock) (not the real clock, which is 450 MHz).
            -- Cut down case :
            --    Only use 8 channels.
            --    So in 0.9 seconds :
            --     408 * 8 = 3264 packets
            --     @64 clocks per packet = 208896 clocks ( = 2.1 ms sim time with 100 MHz simulation clock)
            if reset = '1' then
                packetTriggerCount <= "00000001";
                packetCountWrite <= (others => '0');
                channelWrite <= (others => '0');
                iwriteValid <= '0';
                packet_fsm <= idle;
            elsif resetDone = '1' then
                packetTriggerCount <= std_logic_vector(unsigned(packetTriggerCount) + 1);
                
                case packet_fsm is
                    when idle =>
                        if packetTriggerCount(5 downto 0) = "000000" then
                            packet_fsm <= send_packet;
                        end if;
                        iwriteValid <= '0';
                        
                    when send_packet =>
                        iwriteValid <= '1';
                        packet_fsm <= wait_done;
                    
                    when wait_done =>
                        if owriteValid = '1' then
                            iwriteValid <= '0';
                            if (unsigned(channelWrite) = (unsigned(totalChannelsIn) - 1)) then
                                channelWrite <= (others => '0');
                                packetCountWrite <= std_logic_vector(unsigned(packetCountWrite) + 1);
                            else
                                channelWrite <= std_logic_vector(unsigned(channelWrite) + 1);
                            end if;
                            -- Get a record of the pointer
                            ptrRecord(to_integer(unsigned(channelWrite)))(to_integer(unsigned(packetCountWrite))) <= to_integer(unsigned(writeAddress));
                            packet_fsm <= idle;
                        end if;
                        
                end case;
                
            end if;
            
            -- Generate read requests for output 0
            -- 
            if reset = '1' then
                -- packet count at which we trigger the next readout.
                -- +2 so that it comes a bit after all the data has gone in.
                rr0_trigger <= to_integer(unsigned(blocksPerFrame0)) + to_integer(unsigned(preload0(15 downto 11))) + 2; 
                rr1_trigger <= to_integer(unsigned(blocksPerFrame1)) + to_integer(unsigned(preload0(15 downto 11))) + 2;  -- start of the readout is aligned for both outputs, so we need to allow "preload0" frames for output 1, since preload0 >= preload1
                rr0_packetsPerFrame <= (to_integer(unsigned(blocksPerFrame0)) + to_integer(unsigned(preload0(15 downto 11)))) * to_integer(unsigned(totalChannels0));
                rr1_packetsPerFrame <= (to_integer(unsigned(blocksPerFrame1)) + to_integer(unsigned(preload1(15 downto 11)))) * to_integer(unsigned(totalChannels1));
                rd0TriggerCount <= 0;
                rd1TriggerCount <= 0;
                readReady0 <= '0';
                readReady1 <= '0';
                
                -- channel and packet count that we expect data for
                -- parameters as integers : preload0_int, preload1_int, blocksPerFrame0_int, blocksPerFrame1_int
                -- totalChannels0_int, totalChannels1_int
                rr0_channelCount <= 0;
                rr0_packet <= 0;      -- Packet count for the start of the frame, for all channels.
                rr0_packetCount <= 0; -- Packet count within this channel and frame.
                rr1_channelCount <= 0;
                rr1_packet <= preload0_int - preload1_int;
                rr1_packetCount <= 0;
                
            elsif resetDone = '1' then
                -----------------------------------------------------------------------------------------
                -- Reads from port 0
                --
                -- wait until packetCountWrite gets to rr0_trigger, then request a frames worth of data
                -- wait until enough data has gone in.
                if to_integer(unsigned(packetCountWrite)) = rr0_trigger then
                    -- Update the trigger to the next frame.
                    rr0_trigger <= rr0_trigger + to_integer(unsigned(blocksPerFrame0));
                    -- Generate requests for the frame.
                    -- Number of requests per frame is rr0_packetsPerFrame
                    gen_requests0 <= '1';
                else
                    gen_requests0 <= '0';
                end if;
                
                if rd0TriggerCount = readSpacing0 then
                    rd0TriggerCount <= 0;
                else
                    rd0TriggerCount <= rd0TriggerCount + 1;
                end if;                
                
                
                if gen_requests0 = '1' then
                    requests0Remaining <= rr0_packetsPerFrame;
                elsif rd0TriggerCount = 1 then  -- 1 is arbitrary; just needs to be once every time rd0TriggerCount wraps.
                    if requests0Remaining /= 0 then
                        requests0Remaining <= requests0Remaining - 1;
                    end if;
                end if;
                
                if requests0Remaining /= 0 and rd0TriggerCount = 1 then
                    readReady0 <= '1';
                elsif readValid0 = '1' then
                    readReady0 <= '0';
                end if;
                
                if readFree = '1' and readValid0 = '1' and readReady0 = '1' then
                    free0Del(0) <= '1';
                else
                    free0Del(0) <= '0';
                end if;
                free0Del(13 downto 1) <= free0Del(12 downto 0);
                free0 <= free0Del(13);
                
                -- Check the output is correct :
                --  - Is the pointer the same pointer that was provided for the write ?
                --  - Is the packet count correct ?
                --  - Is the channel correct ?
                if readValid0 = '1' and readReady0 = '1' then
                    -- Check the returned values are correct
                    assert (to_integer(unsigned(readAddress)) = ptrRecord(rr0_channel_int)(rr0_packetCount + rr0_packet)) 
                        report "Read pointer does not match pointer provided for writing to." severity failure;
                    
                    assert (to_integer(unsigned(readPacketCount)) = (rr0_packetCount + rr0_packet))
                        report "read packet count does not match expected value." severity failure;
                       
                    assert (to_integer(unsigned(readChannel)) = rr0_channel_int)
                        report "read channel does not match expected value." severity failure;
                    
                    -- Get the expected next channel and timestamp 
                    if (rr0_packetCount = (preload0_int + blocksPerFrame0_int - 1)) then
                        rr0_packetCount <= 0;
                        if (rr0_channelCount = (totalChannels0_int - 1)) then
                            rr0_channelCount <= 0;
                            rr0_packet <= rr0_packet + blocksPerFrame0_int;
                        else
                            rr0_channelCount <= rr0_channelCount + 1;
                        end if;
                    else
                        rr0_packetCount <= rr0_packetCount + 1;
                    end if;
                    
                end if;
                -----------------------------------------------------------------------
                -- Reads from port 1
                
                if to_integer(unsigned(packetCountWrite)) = rr1_trigger then
                    -- Update the trigger to the next frame.
                    rr1_trigger <= rr1_trigger + to_integer(unsigned(blocksPerFrame1));
                    -- Generate requests for the frame.
                    -- Number of requests per frame is rr0_packetsPerFrame
                    gen_requests1 <= '1';
                else
                    gen_requests1 <= '0';
                end if;
                
                if rd1TriggerCount = readSpacing1 then
                    rd1TriggerCount <= 0;
                else
                    rd1TriggerCount <= rd1TriggerCount + 1;
                end if;                
                
                if gen_requests1 = '1' then
                    requests1Remaining <= rr1_packetsPerFrame;
                elsif rd1TriggerCount = 1 then  -- 1 is arbitrary; just needs to be once every time rd0TriggerCount wraps.
                    if requests1Remaining /= 0 then
                        requests1Remaining <= requests1Remaining - 1;
                    end if;
                end if;
                
                if requests1Remaining /= 0 and rd1TriggerCount = 1 then
                    readReady1 <= '1';
                elsif readValid1 = '1' then
                    readReady1 <= '0';
                end if;
                
                if readFree = '1' and readValid1 = '1' and readReady1 = '1' then
                    free1Del(0) <= '1';
                else
                    free1Del(0) <= '0';
                end if;
                free1Del(69 downto 1) <= free1Del(68 downto 0);
                free1 <= free1Del(69);

                -- Check -
                if readValid1 = '1' and readReady1 = '1' then
                    -- Check the returned values are correct
                    assert (to_integer(unsigned(readAddress)) = ptrRecord(rr1_channel_int)(rr1_packetCount + rr1_packet)) 
                        report "Read pointer (" & integer'image(to_integer(unsigned(readAddress))) &  ") does not match pointer provided for writing to (" & integer'image(ptrRecord(rr1_channel_int)(rr1_packetCount + rr1_packet)) & ")." severity failure;
                    
                    assert (to_integer(unsigned(readPacketCount)) = (rr1_packetCount + rr1_packet))
                        report "read packet count does not match expected value." severity failure;
                       
                    assert (to_integer(unsigned(readChannel)) = rr1_channel_int)
                        report "read channel does not match expected value." severity failure;
                    
                    -- Get the expected next channel and timestamp 
                    if (rr1_packetCount = (preload1_int + blocksPerFrame1_int - 1)) then
                        rr1_packetCount <= 0;
                        if (rr1_channelCount = (totalChannels1_int - 1)) then
                            rr1_channelCount <= 0;
                            rr1_packet <= rr1_packet + blocksPerFrame1_int;
                        else
                            rr1_channelCount <= rr1_channelCount + 1;
                        end if;
                    else
                        rr1_packetCount <= rr1_packetCount + 1;
                    end if;
                    
                end if;

                
            end if;
                    
            -- The channel order memory
            readChannelOrderAddress0Del1 <= readChannelOrderAddress0;
            readChannelOrderAddress0Del2 <= readChannelOrderAddress0Del1;
            readChannelOrderAddress1Del1 <= readChannelOrderAddress1;
            readChannelOrderAddress1Del2 <= readChannelOrderAddress1Del1;
            if (testCase = 1) then
                -- Simple test case; output is the same as the input with 3 cycle latency.
                readChannelOrderData0 <= readChannelOrderAddress0Del2;
                readChannelOrderData1 <= readChannelOrderAddress1Del2;
                -- same thing for the checking
                rr0_channel_int <= rr0_channelCount;
                rr1_channel_int <= rr1_channelCount;
            elsif (testCase = 2) then
                -- Reverse order.
                readChannelOrderData0 <= std_logic_vector(383 - unsigned(readChannelOrderAddress0Del2));
                readChannelOrderData1 <= std_logic_vector(383 - unsigned(readChannelOrderAddress1Del2));
                -- Same thing for the checking
                rr0_channel_int <= 383 - rr0_channelCount;
                rr1_channel_int <= 383 - rr1_channelCount;
            else
                -- Reverse order for one but not the other.
                readChannelOrderData0 <= readChannelOrderAddress0Del2;
                readChannelOrderData1 <= std_logic_vector(383 - unsigned(readChannelOrderAddress1Del2));
                -- Same thing for the checking
                rr0_channel_int <= rr0_channelCount;
                rr1_channel_int <= 383 - rr1_channelCount;            
            end if;
            
            
            -- Track the number of times each memory block has been freed;
            -- 
            if ((readValid0 = '1' or readValid1 = '1') and readFree = '1') then
                addr_v := to_integer(unsigned(readAddress(23 downto 3))) - to_integer(unsigned(baseAddr(23 downto 3)));
                freeCount(addr_v) <= freeCount(addr_v) + 1;
            end if;
            if gen_requests0 = '1' then
                firstZero_v := 16384;
                minValue_v := 1000000;
                minIndex_v := 0;
                maxValue_v := 0;
                maxIndex_v := 0;
                for i in 0 to 16383 loop
                    if freeCount(i) = 0 then
                        if i < firstZero_v then
                            firstZero_v := i;
                        end if;
                    end if;
                    if freeCount(i) < minValue_v then
                        minValue_v := freeCount(i);
                        minIndex_v := i;
                    end if;
                    if freeCount(i) > maxValue_v then
                        maxValue_v := freeCount(i);
                        maxIndex_v := i;
                    end if;
                end loop;
                firstZero <= firstZero_v;
                minValue <= minValue_v;
                minIndex <= minIndex_v;
                maxValue <= maxValue_v;
                maxIndex <= maxIndex_v;
            end if;
        end if;
    end process;
    
    
    ct_vfc_malloc_inst : entity ct_vfc_lib.ct_vfc_malloc
    generic map (
        -- How much memory to allow for.
        -- True : 2 Gbytes have been allocated to the corner turn.
        -- False : 1 Gbyte is allocated to the corner turn.
        -- This defines the amount of space allocated for pointers.
        -- The actual amount of memory used and the base address of the memory is defined via MACE. 
        g_USE_2GBYTE => false
    ) Port map ( 
        -- 
        i_clk => clk, --  in std_logic;
        -- Interface to the data input side - writes to the HBM
        i_writePacketCount => packetCountWrite, -- in std_logic_vector(31 downto 0);  -- Packet count from the packet header
        i_writeChannel     => channelWrite,     -- in std_logic_vector(15 downto 0);  -- virtual channel from the packet header
        i_writeValid       => iwriteValid,      -- in std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
        o_writeAddress     => writeAddress,     -- out std_logic_vector(23 downto 0); -- Address to write this packet to, in units of 8192 bytes.
        o_writeOK          => writeOK,          -- out std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
        o_writeValid       => owriteValid,      -- out std_logic;                     -- i_packet_addr, i_packet_drop are valid. This concludes the transaction with the pointer management module.
        -- Interface to the data output side - Reads from the HBM
        -- If it is the last read of an address, then the address of block to be freed is put into a FIFO in this module, and read from the FIFO on i_free0/1
        -- This means no more than 3 of these addresses should be buffered by the module which reads the HBM.
        o_readAddress     => readAddress,     -- out std_logic_vector(23 downto 0);   -- HBM address to read from, in units of 8192 bytes. Points to the start of a 2048 sample packet.
        o_readPacketCount => readPacketCount, -- out std_logic_vector(31 downto 0);   -- packet count for this read address
        o_readChannel     => readChannel,     -- out std_logic_vector(15 downto 0);   -- Virtual channel at this address.
        o_readStart       => readStart,       -- out std_logic;                       -- Indicates this is the first read for this channel
        o_readOK          => readOK,          -- out std_logic;                       -- o_readAddress points to valid data
        o_readFree        => readFree,        -- out std_logic;                       -- Indicates readAddress can be freed once it has been read
        o_readValid0      => readValid0,      -- out std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady0      => readReady0,      -- in std_logic;                        -- port 0 read side can accept a new address to read from. Data is transfered when valid and ready are high.
        o_readValid1      => readValid1,      -- out std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady1      => readReady1,      -- in std_logic;                        -- port 1 read side can accept a new address to read from. Data is transfered when valid and ready are high.        
        i_free0           => free0,           -- in std_logic;                        -- pulse to indicate that the free operation can go ahead.
        i_free1           => free1,           -- in std_logic;
        -- Configuration from MACE
        -- Configuration parameters must be set prior to i_rst.
        -- Typical values for 0.3 second correlator corner turn :
        --  i_freeMax = 16384  = 1 Gbyte of memory
        --  i_baseAddr = 0x0   = Start at the start of the memory block.
        --  i_maxFutureCount = 400 (Units of LFAA blocks of 2048 samples = 2.2ms) (Minimum is 2 frames worth + preload, maximum is 63(pointers)*8(LFAA blocks/pointer) = 504, since that is the maximum length of the used list for a single channel (for the 1Gbyte case))
        --  i_scanStartPacketCount = 0 (Just depends on the input data)
        --  i_blocksPerFrame0 = 136 (=8x17, note 136*2.2ms = 300 ms)
        --  i_preload0 = 22 * 2048 = 45056; 
        --  i_totalChannels0 = 384
        --  i_blocksPerFrame1 = 16  (16*2.2ms = 35.2 ms)
        --  i_preload1 = 11*256 = 2816  (PST preload is 11*256 samples = 2816 samples)
        --  i_totalChannels1 = 384
        i_rst         => reset, --  in std_logic;                         -- Reset, frees all memory. Should be common to all instances of the corner turn. 
        o_busy        => busy,  -- out std_logic;                        -- Indicates the fsm is busy, e.g. writing default data to the free list after a reset.
        i_freeMax     => freeMax,  -- in std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
        i_baseAddr    => baseAddr, -- in std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
        i_maxFutureCount => maxFutureCount, -- in std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
        i_scanStartPacketCount => scanStartPacketCount, -- in std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
        i_blocksPerFrame0      => blocksPerFrame0(10 downto 0), -- in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
        i_preload0             => preload0,                     -- in std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
        i_totalChannels0       => totalChannels0,               -- in std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
        i_blocksPerFrame1      => blocksPerFrame1(10 downto 0), -- in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
        i_preload1             => preload1,             -- in std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
        i_totalChannels1       => totalChannels1,       -- in std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
        o_readChannelOrderAddress0 => readChannelOrderAddress0, -- out std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 0
        i_readChannelOrderData0    => readChannelOrderData0,    -- in  std_logic_vector(8 downto 0); -- virtual channel to read; assumes 3 clock latency from o_readChannelOrderAddress
        o_readChannelOrderAddress1 => readChannelOrderAddress1, -- out std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 1
        i_readChannelOrderData1    => readChannelOrderData1,    -- in  std_logic_vector(8 downto 0); -- virtual channel to read, assumes 3 clock latency for o_readChannelOrderAddress1
        -- Timing information 
        i_packetCountNotify => x"00000010", --  in std_logic_vector(31 downto 0); -- Set o_packetCountSeen when an incoming packet count (i_packetCountWrite) is this value or higher.
        o_packetCountSeen   => packetCountSeen, -- out std_logic;                    -- Pulse to indicate reception of a packet with packet count >= i_packetCountNotify
          -- don't use these; put timing in the output module.
          -- i_startFrame0       : in std_logic;                     -- Pulse to start sending data for output 0 (i.e. o_readAddress0 etc.)
          -- i_startFrame1       : in std_logic;                     -- Pulse to start sending data for output 1 (i.e. o_readAddress1 etc.)
        -- Monitoring
        o_freeSize    => freeSize,  --  out(15:0);      -- Number of entries in the free list;
        o_freeMin     => freeMin,   --  out(15:0); low water mark for o_freeSize; 
        o_inputLateCount => inputLateCount, --  out std_logic_vector(15 downto 0);   -- Count of input LFAA blocks which have been dropped as they are too late. Top bit is sticky.
        o_inputTooSoonCount => inputTooSoonCount, --  out std_logic_vector(15 downto 0); -- Count of input LFAA blocks which have been dropped as they are too far into the future. Top bit is sticky.
        o_duplicates     => duplicates, -- out std_logic_vector(7 downto 0);    -- count of duplicate input packets (i.e. input packets with the same timestamp and channel as a previous packet). Top bit is sticky.
        o_missingBlocks0 => missingBlocks0, -- out std_logic_vector(15 downto 0);   -- Count of blocks (1 block = 2048 time samples) on output 0 read with no data; top bit is sticky; low 15 bits wrap.
        o_missingBlocks1 => missingBlocks1, -- out std_logic_vector(15 downto 0);   -- Count of blocks on output 1 read with no data; top bit is sticky; low 15 bits wrap.
        o_preload1_gt_preload0 => preload1_gt_preload0, --  out std_logic;                 -- Error flag; i_preload0 must be greater than or equal to i_preload1. Detected when new values are loaded after a module reset (via i_rst).
        o_badFreeFIFOState  => badFreeFIFOState -- out std_logic                     -- Error flag; Tried to read the toBeFree fifo, but it was empty; should never happen.
    );
    
    
end Behavioral;
