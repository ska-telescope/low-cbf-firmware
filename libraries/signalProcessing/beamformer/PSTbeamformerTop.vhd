----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 23.11.2020 16:50:47
-- Module Name: PSTbeamformerTop - Behavioral
-- Description: 
--
-- Registers:
--   Each beam has 64Kbytes of address space.
--   This module supports up to 64 beams via the g_PST_BEAMS generic.
--   So 4Mbytes of address space is assumed here (64 * 64Kbytes = 4 MBytes). 4Mbytes = 22 address bits.
-- 
----------------------------------------------------------------------------------
library IEEE, axi4_lib, common_lib, bf_lib, xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;


entity PSTbeamformerTop is
    generic (
        g_DEBUG_ILA         : BOOLEAN := FALSE;
        g_PIPE_INSTANCE     : integer := 0;
        g_PST_BEAMS         : integer := 16
    );
    port(
        -- registers axi full interface
        i_MACE_clk : in std_logic;
        i_MACE_rst : in std_logic;
        i_axi_mosi : in  t_axi4_full_mosi;
        o_axi_miso : out t_axi4_full_miso;
        
        -- Beamformer data from the corner turn
        i_BF_clk : in std_logic;
        i_data   : in std_logic_vector(95 downto 0);  -- 3 consecutive fine channels delivered every clock.
        i_flagged : in std_logic_vector(2 downto 0);  -- aligns with i_data, indicates if any of the 3 fine channels are flagged as RFI.
        i_fine   : in std_logic_vector(7 downto 0);   -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        i_coarse : in std_logic_vector(9 downto 0);   -- index of the coarse channel.
        i_firstStation : in std_logic;                -- First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation  : in std_logic;
        i_timeStep     : in std_logic_vector(4 downto 0);   -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
        i_virtualChannel : in std_logic_vector(9 downto 0); -- virtual channel
        i_packetCount : in std_logic_vector(36 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        i_valid   : in std_logic;
        -- Other data from the corner turn
        i_jonesBuffer : in std_logic;
        i_phaseBuffer : in std_logic;
        i_beamsEnabled : in std_logic_vector(7 downto 0);  -- Number of beams which are enabled. 
        i_scale_exp_frac : in std_logic_vector(7 downto 0); -- scale factor as configured from ARGs.
        -- 64 bit bus out to the 100GE Packetiser
        o_BFdata           : out std_logic_vector(63 downto 0);
        o_BFpacketCount    : out std_logic_vector(36 downto 0);
        o_BFBeam           : out std_logic_vector(7 downto 0);
        o_BFFreqIndex      : out std_logic_vector(10 downto 0);
        o_BFvalid          : out std_logic;
        -- debug signals
        i_badPacket : in std_logic
    );
end PSTbeamformerTop;

architecture Behavioral of PSTbeamformerTop is

    -- create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_BF
    -- set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_BF} CONFIG.MEM_DEPTH {262144} CONFIG.READ_LATENCY {5}] [get_ips axi_bram_ctrl_BF]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201123_161551/vitisAccelCore.srcs/sources_1/ip/axi_bram_ctrl_BF/axi_bram_ctrl_BF.xci]
    component axi_bram_ctrl_BF
    port (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;

    COMPONENT axi_clock_converter_BF
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arregion : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        m_axi_aclk : IN STD_LOGIC;
        m_axi_aresetn : IN STD_LOGIC;
        m_axi_awaddr : OUT STD_LOGIC_VECTOR(21 DOWNTO 0);
        m_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid : OUT STD_LOGIC;
        m_axi_awready : IN STD_LOGIC;
        m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast : OUT STD_LOGIC;
        m_axi_wvalid : OUT STD_LOGIC;
        m_axi_wready : IN STD_LOGIC;
        m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid : IN STD_LOGIC;
        m_axi_bready : OUT STD_LOGIC;
        m_axi_araddr : OUT STD_LOGIC_VECTOR(21 DOWNTO 0);
        m_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid : OUT STD_LOGIC;
        m_axi_arready : IN STD_LOGIC;
        m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast : IN STD_LOGIC;
        m_axi_rvalid : IN STD_LOGIC;
        m_axi_rready : OUT STD_LOGIC);
    end component;
    
    COMPONENT ila_pst
    PORT (
   	    clk : IN STD_LOGIC;
   	    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;    
    
    signal BFmemRst   : std_logic;
    signal BFmemClk   : std_logic;
    signal BFmemEn    : std_logic;
    signal BFmemWrEn  : std_logic_vector(3 downto 0);
    signal BFmemAddr  : std_logic_vector(19 downto 0);
    signal BFmemDin   : std_logic_vector(31 downto 0);
    signal BFmemDout  : t_slv_32_arr(g_PST_BEAMS downto 0);
    
    signal MACE_rstn : std_logic;
    
    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;    
    signal awlock_slv : std_logic_vector(0 downto 0);
    signal arlock_slv : std_logic_vector(0 downto 0);
    
    signal axi_mosi_arlock : std_logic_vector(0 downto 0);
    signal axi_mosi_awlock : std_logic_vector(0 downto 0);
    signal highbits : std_logic_vector(1 downto 0) := "00";
    signal regAddr : t_slv_24_arr(g_PST_BEAMS downto 0);
    signal regWrData : t_slv_32_arr(g_PST_BEAMS downto 0);
    signal regWrEn : std_logic_vector(g_PST_BEAMS downto 0);
    signal jonesBuffer : std_logic_vector(g_PST_BEAMS downto 0);
    signal phaseBuffer : std_logic_vector(g_PST_BEAMS downto 0);
    signal beamsEnabled, scale_exp_frac : t_slv_8_arr(g_PST_BEAMS downto 0);
    
    signal PData : t_slv_64_arr(g_PST_BEAMS downto 0);
    signal PvirtualChannel : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal PpacketCount : t_slv_37_arr(g_PST_BEAMS downto 0);
    signal PBeam : t_slv_8_arr(g_PST_BEAMS downto 0);
    signal PFreqIndex : t_slv_11_arr(g_PST_BEAMS downto 0);
    signal Pvalid : std_logic_vector(g_PST_BEAMS downto 0);
    
    signal BFdata : t_slv_96_arr(g_PST_BEAMS downto 0);
    signal BFflagged : t_slv_3_arr(g_PST_BEAMS downto 0);
    signal BFfine : t_slv_8_arr(g_PST_BEAMS downto 0);
    signal BFCoarse : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal BFfirstStation : std_logic_vector(g_PST_BEAMS downto 0);
    signal BFlastStation : std_logic_vector(g_PST_BEAMS downto 0);
    signal BFvirtualChannel : t_slv_10_arr(g_PST_BEAMS downto 0);
    signal BFpacketCount : t_slv_37_arr(g_PST_BEAMS downto 0);
    signal BFvalid : std_logic_vector(g_PST_BEAMS downto 0);
    signal BFtimeStep : t_slv_5_arr(g_PST_BEAMS downto 0);
    signal MACE_rstn_BFclk : std_logic;
    signal rst_BFclk : std_logic;
    signal badPacket : std_logic_vector(g_PST_BEAMS downto 0);
    
begin

    MACE_rstn <= not i_MACE_rst;

    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => rst_BFclk,  -- 1-bit output
        dest_clk => i_BF_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',
        src_clk => i_MACE_clk,    -- 1-bit input: Source clock.
        src_pulse => i_MACE_rst,  -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            MACE_rstn_BFclk <= not rst_BFclk;
        end if;
    end process;
    
    awlock_slv(0) <= i_axi_mosi.awlock;
    arlock_slv(0) <= i_axi_mosi.arlock;


--    BF300_cc_ila : ila_0
--    port map (
--   	    clk => i_MACE_clk, --  IN STD_LOGIC;
--   	    probe0(21 downto 0) => i_axi_mosi.awaddr(21 downto 0),
--   	    probe0(22) => i_axi_mosi.awvalid,
--   	    probe0(23) => o_axi_miso.awready,
--   	    probe0(31 downto 24) => i_axi_mosi.wdata(7 downto 0),
--   	    probe0(32) => i_axi_mosi.wlast,
--   	    probe0(33) => i_axi_mosi.wvalid,
--   	    probe0(34) => o_axi_miso.wready,
--   	    probe0(56 downto 35) => i_axi_mosi.araddr(21 downto 0),
--   	    probe0(64 downto 57) => i_axi_mosi.arlen(7 downto 0),
--   	    probe0(65) => i_axi_mosi.arvalid,
--   	    probe0(66) => o_axi_miso.arready,
--   	    probe0(74 downto 67) => o_axi_miso.rdata(7 downto 0),
--   	    probe0(75) => o_axi_miso.rlast,
--   	    probe0(76) => o_axi_miso.rvalid,
--   	    probe0(77) => i_axi_mosi.rready,
--   	    probe0(191 downto 78) => (others => '0')
--    );

--    BF400_cc_ila : ila_0
--    port map (
--   	    clk => i_BF_clk, --  IN STD_LOGIC;
--   	    probe0(21 downto 0) => axi_mosi.awaddr(21 downto 0),
--   	    probe0(22) => axi_mosi.awvalid,
--   	    probe0(23) => axi_miso.awready,
--   	    probe0(31 downto 24) => axi_mosi.wdata(7 downto 0),
--   	    probe0(32) => axi_mosi.wlast,
--   	    probe0(33) => axi_mosi.wvalid,
--   	    probe0(34) => axi_miso.wready,
--   	    probe0(56 downto 35) => axi_mosi.araddr(21 downto 0),
--   	    probe0(64 downto 57) => axi_mosi.arlen(7 downto 0),
--   	    probe0(65) => axi_mosi.arvalid,
--   	    probe0(66) => axi_miso.arready,
--   	    probe0(74 downto 67) => axi_miso.rdata(7 downto 0),
--   	    probe0(75) => axi_miso.rlast,
--   	    probe0(76) => axi_miso.rvalid,
--   	    probe0(77) => axi_mosi.rready,
--   	    probe0(191 downto 78) => (others => '0')
--    );
    



    BF_cci : axi_clock_converter_BF
    port map (
        s_axi_aclk    => i_MACE_clk, -- IN STD_LOGIC;
        s_axi_aresetn => MACE_rstn, -- IN STD_LOGIC;
        s_axi_awaddr    => i_axi_mosi.awaddr(21 downto 0),
        s_axi_awlen     => i_axi_mosi.awlen,
        s_axi_awsize    => i_axi_mosi.awsize,
        s_axi_awburst   => i_axi_mosi.awburst,
        s_axi_awlock    => awlock_slv,
        s_axi_awcache   => i_axi_mosi.awcache,
        s_axi_awprot    => i_axi_mosi.awprot,
        s_axi_awregion => (others => '0'), -- in(3:0);
        s_axi_awqos    => (others => '0'), -- in(3:0);
        s_axi_awvalid   => i_axi_mosi.awvalid,
        s_axi_awready   => o_axi_miso.awready,        
        
        s_axi_wdata     => i_axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => i_axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => i_axi_mosi.wlast,
        s_axi_wvalid    => i_axi_mosi.wvalid,
        s_axi_wready    => o_axi_miso.wready,
        
        s_axi_bresp     => o_axi_miso.bresp,
        s_axi_bvalid    => o_axi_miso.bvalid,
        s_axi_bready    => i_axi_mosi.bready ,

        s_axi_araddr    => i_axi_mosi.araddr(21 downto 0),
        s_axi_arlen     => i_axi_mosi.arlen,
        s_axi_arsize    => i_axi_mosi.arsize,
        s_axi_arburst   => i_axi_mosi.arburst,
        s_axi_arlock    => arlock_slv,
        s_axi_arcache   => i_axi_mosi.arcache,
        s_axi_arprot    => i_axi_mosi.arprot,
        s_axi_arregion  => "0000", -- in(3:0),
        s_axi_arqos     => "0000", -- in(3:0),
        s_axi_arvalid   => i_axi_mosi.arvalid,
        s_axi_arready   => o_axi_miso.arready,
          
        s_axi_rdata     => o_axi_miso.rdata(31 downto 0),
        s_axi_rresp     => o_axi_miso.rresp,
        s_axi_rlast     => o_axi_miso.rlast,
        s_axi_rvalid    => o_axi_miso.rvalid,
        s_axi_rready    => i_axi_mosi.rready,
        -- master interface

        m_axi_aclk    => i_BF_clk, -- in std_logic;
        m_axi_aresetn => MACE_rstn_BFclk, -- in std_logic;
        m_axi_awaddr  => axi_mosi.awaddr(21 downto 0), -- out STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_awlen   => axi_mosi.awlen, -- out STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_awsize  => axi_mosi.awsize, -- out STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awburst => axi_mosi.awburst, -- out STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_awlock  => axi_mosi_awlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_awcache => axi_mosi.awcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awprot  => axi_mosi.awprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_awregion => axi_mosi.awregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awqos   => axi_mosi.awqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_awvalid => axi_mosi.awvalid, -- OUT STD_LOGIC;
        m_axi_awready => axi_miso.awready, -- IN STD_LOGIC;
        m_axi_wdata  => axi_mosi.wdata(31 downto 0), -- OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_wstrb  => axi_mosi.wstrb(3 downto 0), -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_wlast  => axi_mosi.wlast, -- OUT STD_LOGIC;
        m_axi_wvalid => axi_mosi.wvalid, -- OUT STD_LOGIC;
        m_axi_wready => axi_miso.wready, -- IN STD_LOGIC;
        m_axi_bresp  => axi_miso.bresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_bvalid => axi_miso.bvalid, -- IN STD_LOGIC;
        m_axi_bready => axi_mosi.bready, -- OUT STD_LOGIC;
        m_axi_araddr => axi_mosi.araddr(21 downto 0), -- OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        m_axi_arlen  => axi_mosi.arlen, -- OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_arsize => axi_mosi.arsize, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arburst => axi_mosi.arburst, -- OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_arlock  => axi_mosi_arlock, -- OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_arcache => axi_mosi.arcache, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arprot  => axi_mosi.arprot, -- OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_arregion => axi_mosi.arregion, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arqos => axi_mosi.arqos, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_arvalid => axi_mosi.arvalid, -- OUT STD_LOGIC;
        m_axi_arready => axi_miso.arready, -- IN STD_LOGIC;
        m_axi_rdata   => axi_miso.rdata(31 downto 0), -- IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_rresp   => axi_miso.rresp, -- IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_rlast   => axi_miso.rlast, -- IN STD_LOGIC;
        m_axi_rvalid  => axi_miso.rvalid, -- IN STD_LOGIC;
        m_axi_rready => axi_mosi.rready  -- OUT STD_LOGIC
    );

    axi_mosi.awlock <= axi_mosi_awlock(0);
    axi_mosi.arlock <= axi_mosi_arlock(0);
    
    -- Convert register interface from AXI full to address + data
    BF_ctrli : axi_bram_ctrl_BF
    port map (
        s_axi_aclk      => i_BF_clk,
        s_axi_aresetn   => MACE_rstn_BFclk, -- in std_logic;
        s_axi_awaddr    => axi_mosi.awaddr(19 downto 0),
        s_axi_awlen     => axi_mosi.awlen,
        s_axi_awsize    => axi_mosi.awsize,
        s_axi_awburst   => axi_mosi.awburst,
        s_axi_awlock    => axi_mosi.awlock ,
        s_axi_awcache   => axi_mosi.awcache,
        s_axi_awprot    => axi_mosi.awprot,
        s_axi_awvalid   => axi_mosi.awvalid,
        s_axi_awready   => axi_miso.awready,
        s_axi_wdata     => axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => axi_mosi.wlast,
        s_axi_wvalid    => axi_mosi.wvalid,
        s_axi_wready    => axi_miso.wready,
        s_axi_bresp     => axi_miso.bresp,
        s_axi_bvalid    => axi_miso.bvalid,
        s_axi_bready    => axi_mosi.bready ,
        s_axi_araddr    => axi_mosi.araddr(19 downto 0),
        s_axi_arlen     => axi_mosi.arlen,
        s_axi_arsize    => axi_mosi.arsize,
        s_axi_arburst   => axi_mosi.arburst,
        s_axi_arlock    => axi_mosi.arlock ,
        s_axi_arcache   => axi_mosi.arcache,
        s_axi_arprot    => axi_mosi.arprot,
        s_axi_arvalid   => axi_mosi.arvalid,
        s_axi_arready   => axi_miso.arready,
        s_axi_rdata     => axi_miso.rdata(31 downto 0),
        s_axi_rresp     => axi_miso.rresp,
        s_axi_rlast     => axi_miso.rlast,
        s_axi_rvalid    => axi_miso.rvalid,
        s_axi_rready    => axi_mosi.rready,
        bram_rst_a      => BFmemRst,   -- out std_logic;
        bram_clk_a      => BFmemClk,   -- out std_logic;
        bram_en_a       => BFmemEn,     -- out std_logic;
        bram_we_a       => BFmemWrEn,   -- out (3:0)
        bram_addr_a     => BFmemAddr,  -- out (19:0)
        bram_wrdata_a   => BFmemDin,   -- out (31:0)
        bram_rddata_a   => BFmemDout(0)   -- in (31:0)
    );
    
    -- Capture the high order bits of the write address, since the Xilinx axi to BRAM interface only supports 1 Mbyte of address space.
    -- This is a bit of a cheat; check in simulation that the address bits are transferred correctly. May need to add some latency to 
    -- account for the latency through the xilinx axi to bram block.
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            if (axi_mosi.awvalid = '1' and axi_miso.awready = '1') then
                highBits <= axi_mosi.awaddr(21 downto 20);
            end if;
        end if;
    end process;

    -- First stage of the daisy chained register interface
    regAddr(0) <= "00" & highBits & BFmemAddr;
    regWrData(0) <= BFmemDin;
    regWrEn(0) <= BFmemWrEn(0) and BFmemEn;  -- BFmemWrEn is 4 bits wide, one bit per byte; This assumes that all writes are 4 bytes wide. 
        
    -- First stage of the daisy chained data pipeline
    BFdata(0) <= i_data;
    BFflagged(0) <= i_flagged;
    BFfine(0) <= i_fine;
    BFCoarse(0) <= i_coarse;
    BFfirstStation(0) <= i_firstStation;
    BFlastStation(0) <= i_lastStation;
    BFtimeStep(0) <= i_timeStep;
    BFvirtualChannel(0) <= i_virtualChannel;
    BFpacketCount(0) <= i_packetCount;
    BFvalid(0) <= i_valid;

    jonesBuffer(0) <= i_jonesBuffer;
    phaseBuffer(0) <= i_phaseBuffer;
    beamsEnabled(0) <= i_beamsEnabled;
    scale_exp_frac(0) <= i_scale_exp_frac;

    -- First stage of the beamformed data pipeline 
    Pdata(0) <= (others => '0'); -- std_logic_vector(63 downto 0);
    PpacketCount(0) <= (others => '0'); -- in std_logic_vector(36 downto 0);
    PBeam(0) <= (others => '0'); -- in std_logic_vector(7 downto 0); 
    PFreqIndex(0) <= (others => '0'); 
    Pvalid(0) <= '0';
    
    badPacket(0) <= i_badPacket;
    
    -- Instantiate one module for each beam we are making
    beamGen : for i in 0 to (g_PST_BEAMS-1) generate
        
        bfinst: entity bf_lib.PSTbeamformer
        generic map (
            g_BEAM_NUMBER => i, -- : integer;
            -- Number of clock cycles to wait after a packet is ready to send before sending it on the BFData interface.
            -- This is to ensure there is are no clashes on the BFdata bus between different beamformers.
            g_PACKET_OFFSET => (i*800) --  integer
            -- 2 packets are prepared simultaneously, but we only send the second packet after all beamformer instances have sent the first packet. 
            -- So we have to wait a certain length of time before starting the second batch of packets.
            -- The second packet is sent (g_SECOND_PACKET_OFFSET + g_PACKET_OFFSET) clocks from when it is available. 
            --
            --  Second packet offset now calculated based on the number of beams enabled (i.e. using "i_beamsEnabled")
            --
            --g_SECOND_PACKET_OFFSET => (800*g_PST_BEAMS + 100)   
        )
        Port map(
            i_BF_clk  => i_BF_clk, --  in std_logic; -- 400MHz clock
            -- Data in to the beamformer
            i_data    => BFdata(i),  -- in std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock. Must have a six cycle latency relative to the other inputs ("i_fine", "i_station", etc)
            i_flagged => BFflagged(i), -- in std_logic_vector(2 downto 0);
            i_fine    => BFfine(i), --  in std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in i_data is (i_fine*3)
            i_coarse  => BFCoarse(i), -- in(9:0)
            i_firstStation => BFfirstStation(i),  -- in std_logic; First station in a burst.
            i_lastStation => BFlastStation(i),    -- in std_logic; Last station
            i_timeStep    => BFtimeStep(i),       -- in std_logic_vector(4 downto 0);   -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
            i_virtualChannel => BFvirtualChannel(i), -- in std_logic_vector(9 downto 0); -- virtual channel
            i_packetCount => BFpacketCount(i), -- in std_logic_vector(36 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
            i_valid   => BFvalid(i), -- in std_logic;
            -- Data out to the next beamformer (pipelined version of the data in) (2 pipeline stages in this module)
            o_data    => BFdata(i+1), -- out std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock; 
            o_flagged => BFflagged(i+1), -- out std_logic_vector(2 downto 0);
            o_fine    => BFfine(i+1), -- out std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
            o_coarse  => BFCoarse(i+1), -- out(9:0)
            o_firstStation => BFfirstStation(i+1), -- out std_logic; pipelined i_firstStation
            o_lastStation  => BFlastStation(i+1),  -- out std_logic; pipelined i_lastStation
            o_timeStep     => BFtimeStep(i+1),     -- out(4:0); pipelined i_timeStep
            o_virtualChannel => BFvirtualChannel(i+1), --  out std_logic_vector(9 downto 0); -- .
            o_packetCount => BFpacketCount(i+1), -- out std_logic_vector(36 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
            o_valid   => BFvalid(i+1), -- out std_logic;
            ------------------------------------------------------------------------
            -- Register interface (just address + data)
            i_regAddr => regAddr(i),   -- in(23:0); -- Byte address of the data (low two bits will always be "00")
            i_regData => regWrData(i), -- in(31:0);
            i_WrEn    => regWrEn(i),   -- in std_logic;
            -- Pipelined version of the register interface, for the next beamformer.
            o_regAddr => regAddr(i+1),   -- out std_logic_Vector(23 downto 0);
            o_regData => regWrData(i+1), -- out std_logic_Vector(31 downto 0);
            o_wrEn    => regWrEn(i+1),   -- out std_logic;
            -- register read data, 5 cycle latency from i_regAddr
            o_regReadData => BFmemDout(i), -- out(31:0);
            -- Miscellaneous register settings:
            i_jonesBuffer => jonesBuffer(i), -- in std_logic;  -- which of the two buffers to use for the Jones matrices
            i_phaseBuffer => phaseBuffer(i), -- in std_logic;  -- which of the two buffers to use for the phase
            i_beamsEnabled => beamsEnabled(i), -- in std_logic_vector(7 downto 0); -- Number of beams that are enabled. This beamformer outputs packets if g_BEAM_NUMBER < i_beamsEnabled
            i_scale_exp_frac => scale_exp_frac(i),
            -- pipelined outputs for buffer settings
            o_jonesBuffer => jonesBuffer(i+1), -- out std_logic;
            o_phaseBuffer => phaseBuffer(i+1), -- out std_logic;
            o_beamsEnabled => beamsEnabled(i+1), -- out std_logic_vector(7 downto 0);
            o_scale_exp_frac => scale_exp_frac(i+1),
            ---------------------------------------------------------------------
            -- Packets of data from the previous beamformer, to be passed on to the next beamformer.
            i_BFdata        => Pdata(i),        -- in(63:0);
            i_BFBeam        => PBeam(i),        -- in(7:0);  
            i_BFFreqIndex   => PFreqIndex(i),   -- in(10:0);
            i_BFvalid       => PValid(i),       -- in std_logic;
            -- Packets of data out to the 100G interface
            o_BFdata        => Pdata(i+1),        -- out(63:0);
            o_BFpacketCount => PpacketCount(i+1), -- out(36:0);
            o_BFFreqIndex   => PFreqIndex(i+1),   -- out(10:0);
            o_BFBeam        => PBeam(i+1),        -- out(7:0);  
            o_BFvalid       => Pvalid(i+1),       -- out std_logic
            -- debug stuff
            i_badPacket     => badPacket(i),
            o_badPacket     => badPacket(i+1)
        );
        
    end generate;
    
    o_BFdata  <= Pdata(g_PST_BEAMS);                    -- out (63:0);
    o_BFpacketCount <= PpacketCount(g_PST_BEAMS);       -- out (36:0);
    o_BFBeam <= PBeam(g_PST_BEAMS);                     -- out (7:0);
    o_BFFreqIndex <= PFreqIndex(g_PST_BEAMS);           -- out (10:0);
    o_BFvalid <= Pvalid(g_PST_BEAMS);                   -- out std_logic


debug_ila_gen : IF g_DEBUG_ILA GENERATE
    ila_BF_output: IF (g_PIPE_INSTANCE = 1) GENERATE
        
        ila_beamformer_output : ila_pst
            port map (
                clk                     => i_BF_clk, --  IN STD_LOGIC;
                
                probe0(63 downto 0)     => Pdata(2),
                probe0(71 downto 64)    => PBeam(2),
                probe0(82 downto 72)    => PFreqIndex(2),
                probe0(83)              => Pvalid(2),
                
                probe0(84)              => badPacket(2),
                probe0(89 downto 85)    => BFtimeStep(1),
                
                probe0(126 downto 90)   => PpacketCount(2),
                probe0(153 downto 127)  => ( others => '0') ,
                probe0(161 downto 154)  => PBeam(1),
                probe0(172 downto 162)  => PFreqIndex(1),
                probe0(173)             => Pvalid(1),
                
                probe0(181 downto 174)  => BFfine(1),
                probe0(191 downto 182)  => BFCoarse(1)
            );
        
        ila_beamformer_output_2 : ila_pst
            port map (
                clk                     => i_BF_clk, --  IN STD_LOGIC;
                
                probe0(63 downto 0)     => Pdata(2),
                probe0(71 downto 64)    => PBeam(2),
                probe0(82 downto 72)    => PFreqIndex(2),
                probe0(83)              => Pvalid(2),
                
                probe0(84)              => badPacket(2),
                probe0(89 downto 85)    => BFtimeStep(1),
                
                probe0(126 downto 90)   => PpacketCount(2),
                probe0(153 downto 127)  => ( others => '0') ,
                probe0(161 downto 154)  => PBeam(1),
                probe0(172 downto 162)  => PFreqIndex(1),
                probe0(173)             => Pvalid(1),
                
                probe0(181 downto 174)  => BFfine(1),
                probe0(191 downto 182)  => BFCoarse(1)
            );
        
    END GENERATE;

END GENERATE;    
--ila_beamformer_output : ila_pst
--    port map (
--   	    clk                     => i_BF_clk, --  IN STD_LOGIC;
   	    
--   	    probe0(63 downto 0)     => Pdata(2),
--   	    probe0(71 downto 64)    => PBeam(2),
--   	    probe0(82 downto 72)    => PFreqIndex(2),
--   	    probe0(83)              => Pvalid(2),
   	    
--   	    probe0(84)              => badPacket(2),
--   	    probe0(89 downto 85)    => BFtimeStep(1),
   	    
--   	    probe0(126 downto 90)   => PpacketCount(2),
--   	    probe0(153 downto 127)  => ( others => '0') ,
--   	    probe0(161 downto 154)  => PBeam(1),
--   	    probe0(172 downto 162)  => PFreqIndex(1),
--   	    probe0(173)             => Pvalid(1),
   	    
--   	    probe0(181 downto 174)  => BFfine(1),
--   	    probe0(191 downto 182)  => BFCoarse(1)
--    );
            
end Behavioral;


